//
//  GHButton.m
//  AT Hiker
//
//  Created by Ryan Linn on 10/21/17.
//  Copyright © 2017 Guthook's Hiking Guides. All rights reserved.
//

/*
#import "GHButton.h"

@interface GHButton ()
@property (nonatomic, weak) IBOutlet UIView *view;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIControl *control;
@property (nonatomic, weak) IBOutlet UIButton *badge;
@end


@implementation GHButton

-(id)initWithFrame:(CGRect)frame forTarget:(id)target action:(SEL)action withImage:(UIImage *)image andTitle:(NSString *)title {
  if (self = [super initWithFrame:frame]) {
    NSArray *subViews = [[NSBundle mainBundle] loadNibNamed:@"GHButton" owner:self options:nil];
    [self addSubview:subViews.firstObject];
    [self.subviews.firstObject setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    [self.control addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [self setImage:image];
    if (!title) {
      [self.titleLabel removeFromSuperview];
    } else {
      [self setTitleText:title];
    }
    [self initializeViews];
  }
  return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
  self = [super initWithCoder:aDecoder];
    if (self.subviews.count == 0) {
      GHButton *viewFromNib = [[NSBundle mainBundle] loadNibNamed:@"GHButton" owner:self options:nil].firstObject;
      self.view = viewFromNib.view;
      self.titleLabel = viewFromNib.titleLabel;
      self.control = viewFromNib.control;
      self.imageView = viewFromNib.imageView;
      [viewFromNib setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
      [viewFromNib setFrame:self.bounds];
      [self addSubview:viewFromNib];
      
      [self initializeViews];
    }
  return self;
}

-(void)initializeViews {
  self.view.layer.cornerRadius = 5.0;
  self.badge.layer.cornerRadius = 5.0;
  [self.badge setHidden:YES];
}

#pragma mark - Configuration

-(void)setImage:(UIImage *)image {
  [self.imageView setImage:image];
}

-(void)setTitleText:(NSString *)title {
  [self.titleLabel setText:title];
}

-(void)setBadgeValue:(int)val {
  [self.badge setTitle:[NSString stringWithFormat:@"%i", val]  forState:UIControlStateNormal];
  [self.badge setHidden:(val == 0)];
}

-(void)setBadgeAlert:(BOOL)alertBadge {
  [self.badge setTitle:@"!" forState:UIControlStateNormal];
  [self.badge setHidden:!alertBadge];
}

-(void)setBackgroundColor:(UIColor *)backgroundColor {
  [self.view setBackgroundColor:backgroundColor];
}

-(void)setAction:(SEL)action withTarget:(id)target {
  [self.control removeTarget:nil action:NULL forControlEvents:UIControlEventTouchUpInside];
  [self.control addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}
@end
*/
