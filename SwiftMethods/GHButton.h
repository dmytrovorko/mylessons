//
//  GHButton.h
//  AT Hiker
//
//  Created by Ryan Linn on 10/21/17.
//  Copyright © 2017 Guthook's Hiking Guides. All rights reserved.
//

/*
#import <UIKit/UIKit.h>

@interface GHButton : UIView

-(instancetype)initWithFrame:(CGRect)frame forTarget:(id)target action:(SEL)action withImage:(UIImage *)image andTitle:(NSString *)title;

-(void)setTitleText:(NSString *)title;
-(void)setImage:(UIImage *)image;
-(void)setBadgeValue:(int)val;
-(void)setBadgeAlert:(BOOL)alertBadge;
-(void)setAction:(SEL)action withTarget:(id)target;
@end
*/
