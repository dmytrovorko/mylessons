//
//  JsonToDataDataToJson.swift
//  SHAG_lessons
//
//  Created by Imac on 2/8/19.
//  Copyright © 2019 Vara. All rights reserved.
//

import Foundation



struct JSONHelper {
    
    static func dataToJSON(data: Data) -> JSON? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? JSON
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
    
    // Convert from JSON to nsdata
    static func jsonToNSData(json: JSON) -> Data?{
        do {
            return try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil;
    }
}
