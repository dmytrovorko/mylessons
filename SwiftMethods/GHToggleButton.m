//
//  GHToggleButton.m
//  AT Hiker
//
//  Created by Ryan Linn on 7/15/18.
//  Copyright © 2018 Guthook's Hiking Guides. All rights reserved.
//

/*
#import "GHToggleButton.h"

static CGFloat toggleButtonViewPadding = 5.0;

@interface GHToggleButton ()
//@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UIControl *controlBackground;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageView;

@property (strong, nonatomic) NSLayoutConstraint *rightImageRatioConstraint;
@property (strong, nonatomic) NSLayoutConstraint *leftImageRatioConstraint;
@property (strong, nonatomic) NSLayoutConstraint *titleWidthConstraint;
@property (weak) IBOutlet NSLayoutConstraint *leftImagePaddingConstraint;
@property (weak) IBOutlet NSLayoutConstraint *rightImagePaddingConstraint;
@end

@implementation GHToggleButton
@synthesize toggleState = _toggleState;
@synthesize highlightColor = _highlightColor;

-(instancetype)initWithCoder:(NSCoder *)aDecoder {
  if (self = [super initWithCoder:aDecoder]) {
    if (self.subviews.count == 0) {
      GHToggleButton *viewFromNib = [self buttonFromNib];
      self.controlBackground = viewFromNib.controlBackground;
      self.leftImageView = viewFromNib.leftImageView;
      self.titleLabel = viewFromNib.titleLabel;
      self.rightImageView = viewFromNib.rightImageView;
      self.rightImagePaddingConstraint = viewFromNib.rightImagePaddingConstraint;
      self.leftImagePaddingConstraint = viewFromNib.leftImagePaddingConstraint;
      [viewFromNib setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
      [viewFromNib setFrame:self.bounds];
      [self addSubview:viewFromNib];
      
      self.clipsToBounds = YES;
      [self.layer setCornerRadius:4.0];
    }
  }
  return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
  if (self = [super initWithFrame:frame]) {
    GHToggleButton *viewFromNib = [self buttonFromNib];
    [self addSubview:viewFromNib];
    [viewFromNib setFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    
    self.clipsToBounds = YES;
    [self.layer setCornerRadius:4.0];
  }
  return self;
}

-(GHToggleButton *)buttonFromNib {
  GHToggleButton *viewFromNib = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].firstObject;
  
  //viewFromNib.layer.cornerRadius = 4.0;
  if (!viewFromNib.highlightColor) {
    viewFromNib.highlightColor = [UIColor blueColor];
  }
  [viewFromNib setToggleState:NO];
  //[viewFromNib setRightImage:nil];
  //[viewFromNib setLeftImage:nil];
  
  return viewFromNib;
}

#pragma mark - Toggle State

-(void)setToggleState:(BOOL)toggleState {
  _toggleState = toggleState;
  
  UIColor *tintColor = (toggleState ? [UIColor whiteColor] : _highlightColor);
  [self.titleLabel setTextColor:tintColor];
  [self.leftImageView setTintColor:tintColor];
  [self.rightImageView setTintColor:tintColor];
  
  [self.controlBackground setBackgroundColor:(_toggleState ? _highlightColor : [UIColor clearColor])];

}

-(BOOL)toggleState {
  return _toggleState;
}

#pragma mark - Interface Builder Actions
-(IBAction)controlTouchUpInside:(id)sender {
  [self setToggleState:self.toggleState];
  if (self.actionBlock) {
    _actionBlock();
  }
}

-(IBAction)controlTouchDown:(id)sender {
  //touchDown sets control highlighted.
  UIColor *white = [UIColor whiteColor];
  UIColor *grey = [UIColor lightGrayColor];
  
  [self.titleLabel setTextColor:white];
  [self.leftImageView setTintColor:white];
  [self.rightImageView setTintColor:white];
  
  [self.controlBackground setBackgroundColor:grey];
  
  [self logFrames];
}

-(IBAction)controlTouchExited:(id)sender {
  [self setToggleState:self.toggleState];
}

#pragma mark - Property Setters
-(void)logFrames {
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
    NSLog(@"ToggleButton Frame: %@\nLeft Image: %@\nTitle Label: %@\nRight Image: %@", NSStringFromCGRect(self.frame), NSStringFromCGRect(self.leftImageView.frame), NSStringFromCGRect(self.titleLabel.frame), NSStringFromCGRect(self.rightImageView.frame));
  });
}

- (CGSize)adjustedFontSizeForLabel:(UILabel *)label {
  UIFont *maxFont = [UIFont systemFontOfSize:40.0];
  NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithAttributedString:label.attributedText];
  [text setAttributes:@{NSFontAttributeName:maxFont} range:NSMakeRange(0, text.length)];
  
  NSStringDrawingContext *context = [NSStringDrawingContext new];
  context.minimumScaleFactor = label.minimumScaleFactor;
  CGSize textSize = [text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, self.frame.size.height) options:NSStringDrawingUsesLineFragmentOrigin context:context].size;
  return textSize;
}


-(void)setTitle:(NSString *)title {
  if (_titleWidthConstraint) {
    _titleWidthConstraint.active = NO;
  }
  [self.titleLabel setText:title];
  
  //Since height constraint of the button must be set in IB, whenever a title is set the width of the label needs to be calculated by using the adjusted size of the text.
  CGFloat calculatedLabelWidth = 0.0;
  if (title.length > 0) {
    CGFloat labelFixedHeight = self.frame.size.height;
    CGSize textSize = [self adjustedFontSizeForLabel:self.titleLabel];
    CGFloat textSizeRatio = textSize.width / textSize.height;
    calculatedLabelWidth = textSizeRatio * labelFixedHeight;
  }
  _titleWidthConstraint = [self.titleLabel.widthAnchor constraintEqualToConstant:calculatedLabelWidth];
  _titleWidthConstraint.active = YES;
}

-(void)setLeftImage:(UIImage *)image {
  //Set padding to title label
  CGFloat paddingToLabel = (image == nil) ? 0.0 : toggleButtonViewPadding;
  [self.leftImagePaddingConstraint setConstant:paddingToLabel];
  
  //Set correct width
  if (_leftImageRatioConstraint) {
    [_leftImageRatioConstraint setActive:NO];
  }
  
  [self.leftImageView setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];

  NSLayoutConstraint *newConstraint;
  if (image) {
    CGFloat ratio = image.size.width / image.size.height;
    newConstraint = [self.leftImageView.widthAnchor constraintEqualToAnchor:self.leftImageView.heightAnchor multiplier:ratio];
  } else {
    newConstraint = [self.leftImageView.widthAnchor constraintEqualToConstant:0.0];
  }
  
  _leftImageRatioConstraint = newConstraint;
  newConstraint.active = YES;
}

-(void)setRightImage:(UIImage *)image {
  //Set padding to title label
  CGFloat paddingToLabel = (image == nil) ? 0.0 : toggleButtonViewPadding;
  [self.rightImagePaddingConstraint setConstant:paddingToLabel];
  
  //Set correct width
  if (_rightImageRatioConstraint) {
    [_rightImageRatioConstraint setActive:NO];
  }
  [self.rightImageView setImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];

  NSLayoutConstraint *newConstraint;
  if (image) {
    CGFloat ratio = image.size.width / image.size.height;
    newConstraint = [self.rightImageView.widthAnchor constraintEqualToAnchor:self.rightImageView.heightAnchor multiplier:ratio];
  } else {
    newConstraint = [self.rightImageView.widthAnchor constraintEqualToConstant:0.0];
  }
  
  _rightImageRatioConstraint = newConstraint;
  newConstraint.active = YES;
}

-(void)setHighlightColor:(UIColor *)highlightColor {
  _highlightColor = highlightColor;
  [self setToggleState:_toggleState];
}
@end
*/
