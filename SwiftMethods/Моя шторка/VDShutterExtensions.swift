//
//  VDPulleyExtensions.swift
//  TestCreateAnimation
//
//  Created by Imac on 3/28/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit



extension UIView {
    
    func layoutIfNeeded(animation: Bool, duration: TimeInterval = 0.3) {
        if animation {
            UIView.animate(withDuration: duration) { [weak self] in
                self?.layoutIfNeeded()
            }
        } else {
            layoutIfNeeded()
        }
    }
    
    func layoutIfNeededWithCurveEaseOut(duration: TimeInterval = 0.3) {
        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
            self?.layoutIfNeeded()
        })
    }
    
    func layoutIfNeededWithDamping(duration: TimeInterval = 0.3) {
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: .curveEaseOut, animations: { [weak self] in
            self?.layoutIfNeeded()
        } )
    }
}
