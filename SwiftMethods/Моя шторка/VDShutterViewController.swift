//
//  VDPullayViewController.swift
//  TestCreateAnimation
//
//  Created by Imac on 3/28/19.
//  Copyright © 2019 idev. All rights reserved.
//

import UIKit

enum ShutterPosition: Int, CaseIterable {
    case close
    case ajar
    case halfOpen
    case open
    
    mutating func next () {
        let allCases = ShutterPosition.allCases
        guard let currentIndex = allCases.firstIndex(of: self) else { return }
        guard let nextState = ShutterPosition.init(rawValue: currentIndex + 1) else { return }
        self = nextState
    }
    
    mutating func previous () {
        let allCases = ShutterPosition.allCases
        guard let currentIndex = allCases.firstIndex(of: self) else { return }
        guard let nextState = ShutterPosition.init(rawValue: currentIndex - 1) else { return }
        self = nextState
    }
    
    static var height: CGFloat = 0
}

struct ShutterState {
    var state: ShutterPosition
    
    var closeHeight: CGFloat = 0
    var ajarHeight: CGFloat = 0
    var halfOpenHeight: CGFloat = 0
    var openHeight: CGFloat = 0
    
    
    var contentHeight: CGFloat = 0
    
    var currentHeight: CGFloat = 0
    
    init(state: ShutterPosition) {
        self.state = state
    }
    
    mutating func next () {
        state.next()
    }
    
    mutating func previous () {
        state.previous()
    }
    
    func  nearestPosition (height: CGFloat) -> ShutterPosition {

        let distanceToPositions: [ShutterPosition : CGFloat] =
            [
              .close :      abs(height - closeHeight),
              .ajar :       abs(height - ajarHeight),
              .halfOpen :   abs(height - halfOpenHeight),
              .open :       abs(height - openHeight),
            ]
        
        let nearestPosition = distanceToPositions.min{ $0.value < $1.value}
        
        return nearestPosition?.key ?? .close
    }
    
}

enum PanDirection {
    case up
    case down
    case unknow
}

class VDShutterViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    
    var animationDuration: TimeInterval = 0.4

    var shutterState: ShutterState = ShutterState(state: .open)
    
    var shutterHeight: CGFloat = 0 {
        didSet {
            shutterState.currentHeight = shutterHeight
            topContenViewConstraint.constant = -shutterHeight
        }
    }
    
    
    // P R I V A T E   P R O P E R T I E S
    // MARK: - Private Properties
    
    fileprivate var topContenViewConstraint: NSLayoutConstraint!
    
    fileprivate lazy var heightConstraintContentVC: NSLayoutConstraint? = { contentView.constraints.filter{ $0.firstAttribute == .height && $0.firstItem === contentView}.first }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topContenViewConstraint = NSLayoutConstraint.init(item: contentView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)
        
        topContenViewConstraint.isActive = true
        
        
        initDefaultStateHeight()
        initGestureRecognizers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setStateShutter(state: shutterState.state, animation: false)
        
        view.layoutIfNeeded(animation: false)
    }
    
    
    
    // P R I V A T E   M E T H O D S
    // MARK: - Private Methods
    
    fileprivate func initDefaultStateHeight () {
        shutterState.contentHeight = view.bounds.height * 0.8
        heightConstraintContentVC?.constant = shutterState.contentHeight
        
        shutterState.closeHeight = 0
        shutterState.ajarHeight = 0.2 * shutterState.contentHeight
        shutterState.halfOpenHeight = 0.5 * shutterState.contentHeight
        shutterState.openHeight = shutterState.contentHeight
    }
    
    fileprivate func swipeShutter(direction: UISwipeGestureRecognizer.Direction) {
        switch direction {
        case .up:
            shutterState.next()
        case .down:
            shutterState.previous()
        default: break
        }
         setStateShutter(state: shutterState.state, animation: true)
    }
    
    fileprivate func setStateShutter(state: ShutterPosition, animation: Bool, withIntensity intensity: CGFloat = 0 ) {
        

        shutterState.state = state
        
        switch state {
        case .close:
            shutterHeight = shutterState.closeHeight
        case .ajar:
            shutterHeight = shutterState.ajarHeight
        case .halfOpen:
            shutterHeight = shutterState.halfOpenHeight
        case .open:
            shutterHeight = shutterState.openHeight
        }
        
        var correctedIntensity: CGFloat = 0
        if intensity < 0 { correctedIntensity = 0}
        else if intensity > 0.9 { correctedIntensity = 0.9 }
        else { correctedIntensity = intensity }
        
        let duration = animationDuration - animationDuration * Double(correctedIntensity)
        
        guard animation == true else {
            view.layoutIfNeeded(animation: false)
            return
            
        }
        
        if intensity >= 0.5 && state != .open { view.layoutIfNeededWithDamping(duration: duration) }
        else { view.layoutIfNeededWithCurveEaseOut(duration: duration) }

    }
    
    // finish animate pan gesture
    fileprivate let velocityCompare: CGFloat = 400
    
    fileprivate func finishGesture (velocity: CGFloat) {
        
        var direction = PanDirection.unknow
        
        if velocity > 0 { direction = .up }
        else if velocity < 0 { direction = .down}
        
        let intensity = abs(velocity) / velocityCompare
        
        
        var extremsState: [ShutterPosition] = []
        
        if shutterHeight == shutterState.closeHeight { extremsState = [.close]}
        else if shutterHeight == shutterState.ajarHeight { extremsState = [.ajar]}
        else if shutterHeight == shutterState.halfOpenHeight { extremsState = [.halfOpen]}
        else if shutterHeight == shutterState.openHeight { extremsState = [.open]}
        else if shutterHeight > shutterState.closeHeight && shutterHeight < shutterState.ajarHeight {
            extremsState = [.close, .ajar]
        } else if shutterHeight > shutterState.ajarHeight && shutterHeight < shutterState.halfOpenHeight {
            extremsState = [.ajar, .halfOpen]
        } else if shutterHeight > shutterState.halfOpenHeight && shutterHeight < shutterState.openHeight {
            extremsState = [.halfOpen, .open]
        } else if shutterHeight > shutterState.openHeight {
            extremsState = [.open]
        } else if shutterHeight < shutterState.closeHeight {
            extremsState = [.close]
        }

        if extremsState.count == 1, let state = extremsState.first {
            setStateShutter(state: state, animation: true, withIntensity: 1)
        } else if extremsState.count == 2 {
            let lowerState = extremsState[0]
            let upperState = extremsState[1]
            
            var finalState: ShutterPosition
            
            switch direction {
            case .up:
                finalState = upperState
            case .down:
                finalState = lowerState
            case .unknow:
                finalState = shutterState.nearestPosition(height: shutterHeight)
            }
            debugPrint("Intencity - \(intensity)")
            setStateShutter(state: finalState, animation: true, withIntensity: intensity)
        }
    }
    
    // UIGestureRecognize
    
    fileprivate func initGestureRecognizers () {
        
        let up = UISwipeGestureRecognizer(target : self, action : #selector(upSwipe(swipeGesture:)))
        up.direction = .up
        contentView.addGestureRecognizer(up)

        let down = UISwipeGestureRecognizer(target : self, action : #selector(downSwipe(swipeGesture:)))
        down.direction = .down
        contentView.addGestureRecognizer(down)
        
        
        
       let panGesture = UIPanGestureRecognizer(target: self, action: #selector(panHandler(panGesture:)) )
        contentView.addGestureRecognizer(panGesture)
    }
    
    @objc fileprivate func upSwipe(swipeGesture: UISwipeGestureRecognizer) {
        swipeShutter(direction: swipeGesture.direction)
    }
    
    @objc fileprivate func downSwipe(swipeGesture: UISwipeGestureRecognizer) {
        swipeShutter(direction: swipeGesture.direction)
    }
    
    
    // Pan Handler
    var lastTranslate: CGPoint = CGPoint.zero   // the last value
    var prevTranslate: CGPoint = CGPoint.zero  // the value before that one
    var lastTime: TimeInterval = 0
    var prevTime: TimeInterval = 0

    @objc fileprivate func panHandler(panGesture: UIPanGestureRecognizer) {
        
        let translation = panGesture.translation(in: self.view)

        if panGesture.state == .began
        {
            lastTime = Date.timeIntervalSinceReferenceDate
            lastTranslate = translation;
            prevTime = lastTime;
            prevTranslate = lastTranslate;
        }
        else if panGesture.state == .changed
        {
            prevTime = lastTime;
            prevTranslate = lastTranslate;
            lastTime = Date.timeIntervalSinceReferenceDate
            lastTranslate = translation;
            
            if shutterHeight  <= shutterState.openHeight {
                shutterHeight = fmin((shutterHeight - translation.y), shutterState.openHeight)
            }
        }
        else if panGesture.state == .ended
        {
            var swipeVelocity = CGPoint.zero
            
            let seconds: CGFloat = CGFloat(Date.timeIntervalSinceReferenceDate - prevTime)
            if seconds > 0
            {
                swipeVelocity = CGPoint(x:(translation.x - prevTranslate.x) / seconds, y: (translation.y - prevTranslate.y) / seconds);
            }
            
            let inertiaSeconds: CGFloat = 0.3;  // let's calculate where that flick would take us this far in the future
            let final = CGPoint(x: translation.x + swipeVelocity.x * inertiaSeconds,
                                y: translation.y + swipeVelocity.y * inertiaSeconds)
            
            debugPrint("\(final.y)")

            finishGesture(velocity: final.y)
        }
        
       panGesture.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
    }
    
}
