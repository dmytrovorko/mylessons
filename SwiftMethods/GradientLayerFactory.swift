//
//  GradientLayerFactory.swift
//  JoySpring
//
//  Created by Red Pill on 30.01.2019.
//  Copyright © 2019 emyoli.com. All rights reserved.
//


// L I N K
https://www.appcoda.com/cagradientlayer/

import UIKit

typealias AngleGradientPoints = (startPoint: CGPoint, endPopint: CGPoint)

enum ColorGradient {
  case diamond
  case silver
  case bronze
  case gold
  
  func getColorSet () -> [CGColor] {
    switch self {
    case .gold:
      return [#colorLiteral(red: 0.9960784314, green: 0.8941176471, blue: 0.4196078431, alpha: 1).cgColor,
              #colorLiteral(red: 1, green: 0.9333333333, blue: 0.6117647059, alpha: 1).cgColor,
              #colorLiteral(red: 1, green: 0.9607843137, blue: 0.7607843137, alpha: 1).cgColor,
              #colorLiteral(red: 0.937254902, green: 0.8235294118, blue: 0.2078431373, alpha: 1).cgColor,
              #colorLiteral(red: 0.8509803922, green: 0.6588235294, blue: 0.07450980392, alpha: 1).cgColor]
    case .diamond:
      return [#colorLiteral(red: 0.4980392157, green: 0.5529411765, blue: 0.5647058824, alpha: 1).cgColor,
              #colorLiteral(red: 0.8156862745, green: 0.9294117647, blue: 0.9568627451, alpha: 1).cgColor,
              #colorLiteral(red: 0.7058823529, green: 0.7490196078, blue: 0.7607843137, alpha: 1).cgColor,
              #colorLiteral(red: 0.3568627451, green: 0.4470588235, blue: 0.5137254902, alpha: 1).cgColor]
    case .silver:
      return [#colorLiteral(red: 0.9058823529, green: 0.9058823529, blue: 0.9058823529, alpha: 1).cgColor,
              #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.8901960784, alpha: 1).cgColor,
              #colorLiteral(red: 0.7764705882, green: 0.7882352941, blue: 0.8509803922, alpha: 1).cgColor,
              #colorLiteral(red: 0.3568627451, green: 0.3921568627, blue: 0.4039215686, alpha: 1).cgColor]
    case .bronze:
      return [#colorLiteral(red: 0.9333333333, green: 0.6901960784, blue: 0.5215686275, alpha: 1).cgColor,
              #colorLiteral(red: 0.9294117647, green: 0.737254902, blue: 0.5960784314, alpha: 1).cgColor,
              #colorLiteral(red: 0.9529411765, green: 0.7568627451, blue: 0.6117647059, alpha: 1).cgColor,
              #colorLiteral(red: 0.8509803922, green: 0.6470588235, blue: 0.4980392157, alpha: 1).cgColor,
              #colorLiteral(red: 0.6274509804, green: 0.4470588235, blue: 0.3254901961, alpha: 1).cgColor]
    }
  }
  
  func getLocations () -> [NSNumber] {
    switch self {
    case .gold:
      return  [0.0, 0.2099, 0.3204, 0.7182, 1]
    case .diamond:
      return [0.0074, 0.3341, 0.5736, 0.9273]
    case .bronze:
      return [0, 0.1713, 0.2818, 0.5083, 0.8398]
    case .silver:
      return [0.1602, 0.3149, 0.4917, 1]
    }
  }
  
  func getAngle () -> AngleGradientPoints {
    
    switch self {
    case .gold:
      return createAnglePoints(angle: 180.0)
    case .diamond:
      return createAnglePoints(angle: 178.74)
    case .bronze:
      return createAnglePoints(angle: 180.0)
    case .silver:
      return createAnglePoints(angle: 180.0)
      
    }
  }
  
  fileprivate func createAnglePoints(angle: Double) -> AngleGradientPoints {
    let xPoint: Double = angle / 360.0
    let aPoint = pow(sinf(Float(2.0 * Double.pi * ((xPoint + 0.75) / 2.0))), 2.0)
    let bPoint = pow(sinf(Float(2 * Double.pi * ((xPoint + 0.0) / 2))), 2)
    let cPoint = pow(sinf(Float(2 * Double.pi * ((xPoint + 0.25) / 2 ))), 2)
    let dPoint = pow(sinf(Float(2 * Double.pi * ((xPoint + 0.5) / 2))), 2)
    
    
    
    return (CGPoint.init(x: CGFloat(cPoint), y: CGFloat(dPoint) ), CGPoint.init(x: CGFloat(aPoint), y: CGFloat(bPoint)))
  }
}

struct  GradientLayerFactory {
  
  static func createGradientLayer(color: ColorGradient, rect: CGRect ) -> CAGradientLayer {
    let gradientLayerNew        = CAGradientLayer()
    gradientLayerNew.frame      = rect
    gradientLayerNew.colors     = color.getColorSet()
    gradientLayerNew.locations  = color.getLocations()
    gradientLayerNew.startPoint = color.getAngle().startPoint
    gradientLayerNew.endPoint   = color.getAngle().endPopint
    
    return gradientLayerNew
  }
  
  
}
