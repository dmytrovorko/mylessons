///*
// ПРоизовольный доступ к файлам.
// 
// int fseek (FILE * f, long shift, int origin); - устанавливает укзатель в заданную точку. 1 параметр - сам указатель, 2 параметр - смещение указателя в байтах, 3 параметр - точка отсчета смещения ( SEEK_SET - начало файла,
// SEEK_CUR - текущее положение указателя,
// SEEK_END - конец файла.)
// 
// long ftell(FILE *f); - возращает число байт от начала до текущего положения указателя
// */
//
//
//#import <Foundation/Foundation.h>
//#include <stdlib.h>
//#include <string.h>
//
//
//
//int main(int argc, const char * argv[]) {
//    
//    char ch;
//    FILE *f = fopen("TextFile.txt", "r");
//    
//    if (f == NULL)
//    {
//        printf ("Error open file\n");
//        return 0;
//    }
//    fseek (f, -1L, SEEK_END);
//    do{
//        ch = fgetc(f);
//        printf("%c", ch);
//    }
//    while (fseek(f, -2L, SEEK_CUR) == 0);
//    
//    fclose(f);
//    
//    return 0;
//}
//
