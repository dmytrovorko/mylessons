/*
 1) Fraction
****************************************************************************************************************************
 #import <Foundation/Foundation.h>
 
 @interface Fraction : NSObject
 
 @property (nonatomic) int num, den;
 
 -(instancetype) init;
 -(instancetype) initWithNum: (int)num andDen: (int)den;
 -(NSString *) description;
 -(Fraction*) addFraction: (Fraction*)fraction;
 
 
 +(instancetype) fraction;
 +(instancetype) fractionWithNum: (int)num andDen: (int)den;
 +(Fraction*) fraction: (Fraction*)fraction1 multiplyWith: (Fraction*) fraction2;
 @end
 
***************************************************************************************************************************
 #import "Fraction.h"
 
 @implementation Fraction
 
 +(instancetype) fraction {
 return [[Fraction alloc] init];
 }
 
 +(instancetype) fractionWithNum:(int)num andDen:(int)den {
 return [[Fraction alloc] initWithNum:num andDen:den];
 }
 
 -(void) setDen:(int)den{
 if (den == 0) {
 _den = 1;
 NSLog(@"Error! Divide by zero!");
 }
 else{
 _den = den;
 }
 }
 
 - (instancetype)init
 {
 self = [super init];
 if (self) {
 self.num = 0;
 self.den = 1;
 }
 return self;
 }
 
 -(instancetype) initWithNum:(int)num andDen:(int)den {
 self = [super init];
 if (self) {
 self.num = num;
 self.den = den;
 }
 return self;
 }
 
 -(NSString *) description {
 return [NSString stringWithFormat:@"%i/%i", self.num, self.den];
 }
 
 -(Fraction *) addFraction:(Fraction *)fraction {
 Fraction *res = [Fraction fraction];
 
 res.num = self.num * fraction.den + fraction.num * self.den;
 res.den = self.den * fraction.den;
 
 return res;
 }
 
 +(Fraction *) fraction:(Fraction *)fraction1 multiplyWith:(Fraction *)fraction2 {
 Fraction *res = [Fraction fraction];
 
 res.num = fraction1.num * fraction2.num;
 res.den = fraction1.den * fraction2.den;
 
 return res;
 }
 
 @end
 
 *******************************************************************************************************************
 #import <Foundation/Foundation.h>
 #import "Fraction.h"
 
 int main(int argc, const char * argv[]) {
 @autoreleasepool {
 
 Fraction    *f1 = [Fraction fractionWithNum:1 andDen:5],
 *f2 = [Fraction fractionWithNum:2 andDen:5],
 *f3 = [f1 addFraction:f2],
 *f4 = [Fraction fraction:f1 multiplyWith:f2];
 
 NSLog(@"%@ + %@ = %@", f1, f2, f3);
 NSLog(@"%@ * %@ = %@", f1, f2, f4);
 
 
 //Fraction *f = [[Fraction alloc] init];
 //Fraction *f = [[Fraction alloc] initWithNum:3 andDen:8];
 //Fraction *f = [Fraction fraction];
 //        Fraction *f = [Fraction fractionWithNum:2 andDen:5];
 //        NSLog(@"%@", f);
 
 //        f.num = 2; // [f setNum:2];
 //        f.den = 3; // [f setDen:3];
 
 //NSLog(@"%i/%i", f.num, f.den);
 //NSLog(@"%i/%i", [f num], [f den]);

}
return 0;
}
 **********************************************************************************************************************************

СОЗДАНИЕ id класса!!!!!
 
 @interface Array()
 {
 id __strong *p;
 }
 @end
 
 @implementation Array
 
 - (void) removeObjectAtIndex: (int) index{
 id __strong * buf = (id __strong *) calloc(--_size, sizeof(id));
 for (int i = 0; i < (self.size + 1); i++) {
 if (i < index)
 buf[i] = p[i];
 else if (i > index)
 buf [i - 1] = p[i];
 }
 if (p != nil)
 free(p);
 p = buf;
 }
 
 
 - (void) insertObject: (id) anObject atIndex: (int) index{
 id __strong *buf = (id __strong *) calloc (++_size, sizeof(id));
 
 for (int i = 0; i < _size; i++) {
 if (i < index){
 buf[i] = p[i];
 }
 else if (i > index)
 buf[i] = p[i-1];
 else if (i == index)
 buf[i] = anObject;
 }
 if (p != nil)
 free (p);
 p = buf;
 }
 
 - (instancetype)init
 {
 self = [super init];
 if (self) {
 p = nil;
 _size = 0;
 }
 return self;
 }
 
 +(instancetype)array
 {
 return [[Array alloc] init];
 }
 
 -(id)objectAtIndex:(int)index
 {
 if (index < 0 || index > self.size - 1) {
 return 0;
 }
 return p[index];
 }
 
 -(void)addObject:(id)object
 {
 id __strong *buf = (id __strong *)calloc(++_size, sizeof(id));
 for (int i = 0; i < _size - 1; i++) {
 buf[i] = p[i];
 }
 buf[_size-1] = object;
 if (p != nil) {
 free(p);
 }
 p = buf;
 }
 
 -(void)removeLastObject
 {
 id __strong *buf = (id __strong *)calloc(--_size, sizeof(id));
 for (int i = 0; i < _size; i++) {
 buf[i] = p[i];
 }
 if (p != nil) {
 free(p);
 }
 p = buf;
 }
 
 
 -(NSString *)description
 {
 NSMutableString *s = [NSMutableString stringWithFormat:@"a: "];
 
 for (int i = 0; i < self.size; i++) {
 [s appendFormat:@"\n%@ ", p[i]];
 }
 [s appendFormat:@"size: %i", self.size];
 return s;
 }
 
 -(void)dealloc{                            ///////////////принудительная очистка памяти, в тех случаях, где где не чистит система!
 NSLog(@"Deallocating memory...");
 if(self.size != 0){
 free(p);
 p = nil;
 _size = 0;
 }
 }

 
 
 
 
 
 
 
 
 
 
 
 
 */



