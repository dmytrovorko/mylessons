///*
// Структуры
// ADT - abstract data types
// Структура - это абстрактый (пользовательский) тип данных. Структура состоит из полей. Поле может иметь любой тип, в том числе быть другой структурой. До использования структуру неободимо объявить. Память будет выделятся только при создоании экземпляра структуры.
//
// Оператор typedef - позволяет задавать новые имена для существующих типов даных.
//
// typedef old_name new_name;
//
// например:
// typedef unsigned int size_t;
//
// Синтаксиз объявления структуры:
//
// typedf stract Имя {
//
// тип1 поле1;
// тип2 поле2;
// ...
//
//
// } Новое_имя_структуры;
//
// Экземпляр структуры занимает столько памяти, сколько в сумме весят все его поля. Однако компилятор может производить выравнивание полей с целью оптимизации, тогдк структуры будут занимать больше памяти.
// Для доступа к полям структуры используются оператор - "точка".
// Структуру целиком пожно проинецеализировать только при объявлении.
//
// Доступ к полям структур через указатель осуществляется с помощью оператора селектор '->'
//
// ОБЪЕДЕНЕНИЕ  - это разновидность структур, все поля которой выравнены таким образом, что начинаются с одного адресса. Экземпляр объеденения занимает в памяти объем равный размеру его самого большого поля. Одновременно в объеденении можно хранить значение только одного из полей. цель использования экономия памяти.
//
// Для экономии памяти в структурах могут использоваться битовые поля. Размер каждого поля указывается через двоеточие после его имени. Битовыми могут быть, только поля целых типов. Помять под экземпляр структуры выделяется блоками, равными размеру самого большого типа в структуре.
// Для выравнивания битовых полей используются безымянные битовые поля. Такое поле сдвигает все последующие вправо на свою величину. Нулевое безымянное битовое поле сдвигает последующее на начало следующего байта.
//
// */
//
//
//
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//
//typedef struct
//{
//    char first_name[30];
//    char last_name[30];
//    short age;
//    char profession[30];
//    unsigned long salary;
//} EMPLOYEE;
//
//enum MenuItems{
//    NameNotFound = -1,
//    AddName = 1,
//    RemoveName = 2,
//    SearchByNameLName = 3,
//    //SortByName = 3,
//    SortBy_ = 4,
//    PrintNames = 5,
//    Exit = 6
//};
//
//
//
////char* inputName(void);
////char** addName(char **names, int *namesCount);
////char** removeName(char **names, int *namesCount, int index);
////void printEmpl(char **names, const int namesCount);
////int searchByName(char **names, const int namesCount, const char *name);
////void sortByNames(char **names, const int namesCount, int(*compare)(char*, char*));
//int lessThan(char* first, char *second);
//int greaterThan(char* first, char *second);
//
//int searchByNameLN(EMPLOYEE **names, const int emplCount, const char *name, const char *lastName);
//void sort(EMPLOYEE **empl, const int emplCount, int(*compare)(EMPLOYEE*, EMPLOYEE*));
//EMPLOYEE** removeEmpl(EMPLOYEE **empl, int *emplCount, const int index);
//EMPLOYEE** addName(EMPLOYEE **names, int *namesCount);
//EMPLOYEE* inputEmpl(void);
//void printEmpl(EMPLOYEE **names, const int emplCount);
//
//int main(int argc, const char * argv[]) {
//
//    EMPLOYEE **employee = NULL;
//    int emplCount = 0;
//    enum MenuItems choice;
//
//
//    while (1) {
//        printf("\tMenu:\n1 - Add new employee\n2 - Remove employee\n3 - Search by name\n4 - Sort by (\n5 - Print names\n6 - Exit\n");
//        scanf("%i", &choice);
//
//        //system("clear");
//        switch (choice) {
//            case AddName:
//                employee = addName(employee, &emplCount);
//                break;
//
//            /*case SortByName:
//                printf("1) A-Z\t2) Z-A\n");
//                scanf("%i", &choice);
//                if(choice == 1)
//                    sort(names, namesCount, lessThan);
//                else
//                    sort(names, namesCount, greaterThan);
//                break;*/
//
//            case RemoveName:
//            {
//                //1) Remove name by index
//                int idx = 0;
//                printf("Input name index for remove: ");
//                scanf("%i", &idx);
//                employee = removeEmpl(employee, &emplCount, idx);
//
//                /*2) Remove name by name
//                char nameToSearch[80];
//                printf("Input name to remove: ");
//                scanf("%s", nameToSearch);
//                int idx = searchByName(names, namesCount, nameToSearch);
//                if (idx == NameNotFound) {
//                    printf("Name \'%s\' wasn't found:(\n", nameToSearch);
//                } else {
//                    names = removeName(names, &namesCount, idx);
//                    printf("Name \'%s\' was removed!\n", nameToSearch);
//                }*/
//                break;
//            }
//            case SearchByNameLName:
//            {
//                char nameToSearch[80], lastNameToSearch [80];
//                printf("Input name and last name to search (or '*' if not interested one off komponent):\n");
//                while(scanf("%s %s", nameToSearch, lastNameToSearch) == 1)
//                    if (!strcmp(nameToSearch, "*") && !strcmp(lastNameToSearch, "*")) printf("Not input data\n");
//                    else break;
//
//                int idx = searchByNameLN(employee, emplCount, nameToSearch, lastNameToSearch);
//                if (idx == NameNotFound) {
//                    printf("\'%s\'\'%s\' wasn't found:(\n", !strcmp(nameToSearch, "*") ? " " : nameToSearch, !strcmp(lastNameToSearch, "*") ? " " : nameToSearch);
//                } else {
//                    printf("\'%s\'\'%s\' was found in element #%i!\n", !strcmp(nameToSearch, "*") ? " " : nameToSearch, !strcmp(lastNameToSearch, "*") ? " " : lastNameToSearch, idx);
//                }
//                break;
//            }
//            case PrintNames:
//                printEmpl(employee, emplCount);
//                break;
//                //            case Exit:
//                //                for (int i = 0; i < namesCount; i++) {
//                //                    free(names[i]);
//                //                }
//                //                if(names) free(names);
//                //                names = NULL;
//                //                namesCount = 0;
//                //                return 0;
//            default:
//                break;
//        }
//    }
//
//    return 0;
//}
//
//
//
//int searchByNameLN(EMPLOYEE **names, const int emplCount, const char *name, const char *lastName) {
//
//    int choice;
//    if (*name != '*' && *lastName != '*') choice = 1;
//    else if (*name == '*') choice = 2;
//    else choice = 3;
//
//
//    switch(choice){
//        case 1:
//            for (int i = 0; i < emplCount; i++) {
//                if(!strcmp(names[i]->first_name, name) && !strcmp(names[i]->last_name, lastName) ) {
//                    return i;
//                }
//            }
//            break;
//        case 2:
//            for (int i = 0; i < emplCount; i++) {
//                if((!strcmp(names[i]->last_name, lastName))) {
//                    return i;
//                }
//            }
//            break;
//        case 3:
//            for (int i = 0; i < emplCount; i++) {
//                if(!strcmp(names[i]->first_name, name)){
//                    return i;
//                }
//            }
//            break;
//    }
//    return NameNotFound;
//}
//
//EMPLOYEE* inputEmpl(void) {
//    char name[30], lastName[30], profession[30] ;
//    int age, salary;
//    EMPLOYEE *empl = malloc(sizeof(EMPLOYEE));
//
//    printf("Input first name: ");
//    scanf("%s", name);
//    strcpy(empl->first_name, name);
//
//    printf("Input laste name: ");
//    scanf("%s", lastName);
//    strcpy(empl->last_name, lastName);
//
//    printf("Input Age: ");
//    scanf("%i", &age);
//    empl->age = age;
//
//    printf("Input Profession: ");
//    scanf("%s", profession);
//    strcpy(empl->profession, profession);
//
//    printf("Input Salary: ");
//    scanf("%i", &salary);
//    empl->salary = salary;
//
//
//    return empl;
//}
//
//EMPLOYEE** removeEmpl(EMPLOYEE **empl, int *emplCount, const int index) {
//    EMPLOYEE** temp = calloc(--*emplCount, sizeof(EMPLOYEE*));
//    //1)
//    for (int i = 0; i < index; i++) {
//        temp[i] = empl[i];
//    }
//    for (int i = index; i < *emplCount; i++) {
//        temp[i] = empl[i + 1];
//    }
//
//    free(empl[index]);
//    if(empl) free(empl);
//
//    return temp;
//}
//
//EMPLOYEE** addName(EMPLOYEE **employee, int *namesCount) {
//    EMPLOYEE** temp = calloc(++*namesCount, sizeof(EMPLOYEE*));
//    for (int i = 0; i < *namesCount - 1; i++) {
//        temp[i] = employee[i];
//    }
//    temp[*namesCount - 1] = inputEmpl();
//    if(employee) free(employee);
//
//    return temp;
//}
//
//void printEmpl(EMPLOYEE **empl, const int emplCount){
//
//    printf("%-5s%-15s %-15s %-15s %-15s %-15s\n", "#p/p", "First name", "Last name", "Age", "Profession", "Salary");
//
//    for (int i = 0; i < emplCount; i++)
//    {
//        printf("%-5i%-15s %-15s %-15i %-15s %-15lu\n", i, empl[i]->first_name, empl[i]->last_name, empl[i]->age, empl[i]->profession, empl[i]->salary);
//    }
//}
//
///*void sort(EMPLOYEE **empl, const int emplCount, int(*compare)(void*, void*), EMPLOYEE **choise) {
// for (int i = 0; i < emplCount; i++) {
// for (int j = emplCount - 1; j > i; j--) {
// if(compare(empl[j]->first_name, empl[j - 1]->last_name) == 1){
// EMPLOYEE *buf       = empl[j];
// empl[j]        = empl[j - 1];
// empl[j - 1]    = buf;
// }
// }
// }
// }*/
//
////int lessThan(char* first, char *second){
////    return strcmp(first, second) < 0 ? 1 : 0;
////
////}
////int greaterThan(char* first, char *second){
////    return strcmp(first, second) > 0 ? 1 : 0;
////}
//
//
//
//
//
//
//
