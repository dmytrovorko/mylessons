//
//  TableView Фичи.m
//  SHAG_lessons
//
//  Created by Дмитрий on 22.07.2018.
//  Copyright © 2018 Vara. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark - Растягивающейся заголовок (image)
// https://medium.com/if-let-swift-programming/how-to-create-a-stretchable-tableviewheader-in-ios-ee9ed049aba3
/*
import "DVTestViewController.h"

@interface DVTestViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) UIImageView * imageView;


@end

@implementation DVTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tableView.contentInset = UIEdgeInsetsMake(300, 0, 0, 0);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    
    CGRect imageViewRect = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 300);
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:imageViewRect];
    imageView.image = [UIImage imageNamed:@"baby.jpg"];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    
    self.imageView = imageView;
    [self.view addSubview: imageView];
    
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 30;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString * identifier = @"cell";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier: identifier ];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.text = @"FOO";
    }
    
    return cell;
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSLog(@"%@", NSStringFromCGRect(self.imageView.frame));
    CGFloat y = 300 - (scrollView.contentOffset.y + 300);
    CGFloat height = MIN(MAX(y, 60), 400);
    
    self.imageView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, height);
}
 */


