/*
 // Создание изменяемого массива
     NSLog(@"\r\n\r\nСоздание изменяемого массива\r\n");
     NSMutableArray * a = [NSMutableArray arrayWithObjects:@"One", @"Two", @"Three", nil];
     [a addObject:@"Four"];
     [a addObject:@"Five"];
     [a insertObject:@"Two" atIndex: 1];
     [a removeObjectAtIndex: 3];
     [a removeObject:@"Two"];
 
 // Сохранение массива в файл и чтение из файла
     NSLog(@"\r\n\r\nСохранение массива в файл и чтение из файла\r\n");
 
     if (@available(macOS 10.13, *)) {
     [a writeToURL:[NSURL URLWithString: @"file:////Users/ss/Library/Developer/Xcode/DerivedData/NSArray_Test-bjvhlfbowfkcatfppbnadeuptdgo/Build/Products/Debug/myfile.txt"] error:nil];
     } else {
     // Fallback on earlier versions
     [a writeToFile: @"myfile.txt" atomically:YES];
     }
     NSArray * two = [NSArray arrayWithContentsOfFile:@"myfile.txt"];
     NSLog(@"%@", two);
 ########################################################################


*/
