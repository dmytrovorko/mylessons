/*
//
//  DVCameraViewController.m
//  Photo Diary
//
//  Created by Дмитрий on 27.09.2018.
//  Copyright © 2018 com.DmitryVorko. All rights reserved.
// https://www.youtube.com/watch?v=6rXCVeLhp4c

#import "DVCameraViewController.h"

@import AVFoundation;

#import "DVPreviewViewController.h"
#import "DVAddEventViewController.h"
#import "DVNavigationButtons.h"

#import "CameraSessionView.h"



#import "DVUtils.h"


static void * SessionRunningContext = &SessionRunningContext;

typedef NS_ENUM(NSInteger, AVCamSetupResult) {
    AVCamSetupResultSuccess,
    AVCamSetupResultCameraNotAuthorized,
    AVCamSetupResultSessionConfigurationFailed
};



@interface DVCameraViewController () <AVCapturePhotoCaptureDelegate, CACameraSessionDelegate>

@property (nonatomic, strong) CameraSessionView *cameraView;

// controls for making photo
@property (weak, nonatomic) IBOutlet UIButton *  capturePhotoInGallery_Button;
@property (weak, nonatomic) IBOutlet UIButton * capturePhoto_Button;
@property (weak, nonatomic) IBOutlet UIButton * changeCamera_Button;
@property (weak, nonatomic) IBOutlet DVNavigationButtons * dismiss_Button;

// controls for saving photo
@property (weak, nonatomic) IBOutlet UIView *savePhoto_View;
@property (weak, nonatomic) IBOutlet UIButton *savePhoto_Button;
@property (weak, nonatomic) IBOutlet UIButton *cancelSavePhoto_Button;

// image preview
@property (weak, nonatomic) IBOutlet UIImageView * imagePreview_ImageView;


@property (strong, nonatomic) AVCaptureSession * session;
@property (strong, nonatomic) AVCaptureDeviceDiscoverySession * videoDeviceDiscoverySession;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer * previewLayer;
@property (strong, nonatomic) dispatch_queue_t sessionQueue;
@property (assign, nonatomic) AVCamSetupResult setupResult;
@property (strong, nonatomic) AVCaptureDeviceInput * videoDeviceInput;
@property (strong, nonatomic) AVCapturePhotoOutput * photoOutput;
@property (strong, nonatomic) NSMutableDictionary * inProgressPhotoCaptureDelegates;
@property (assign, nonatomic, getter=isSessionRunning) BOOL sessionRunning;
@property (strong, nonatomic) AVCaptureMovieFileOutput * movieFileOutput;
@property (strong, nonatomic) UIImagePickerController * picker;



@property (strong, nonatomic) UIImage                       * image;
@property (assign, nonatomic) CGRect                        rectForPreviewLayer;

@property (assign, nonatomic) CGSize                        iphoneScreenSize;


@end

#define OBSERVER_SESSION_KEY_PATH @"running"

@implementation DVCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initSession];
    
}

- (void) dealloc {
    [self removeObservers];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (CGSize) iphoneScreenSize {
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1136: {
                return CGSizeMake(320, 568);
            }
                break;
            case 1334: {
                return CGSizeMake(375, 667);
            }
                break;
            case 1920: {
                if ([UIScreen mainScreen].bounds.size.height == 667) {
                    return CGSizeMake(375, 667);
                }
                else {
                    return CGSizeMake(414, 736);
                }
            }
                break;
            case 2436:{
                return CGSizeMake(375, 812);
            }
                
                break;
            case 2688: {
                return CGSizeMake(414, 896);
            }
            case 1792: {
                return CGSizeMake(414, 896);
            }
            default:
                return CGSizeZero;
        }
    }
    return CGSizeZero;
}



- (void) initSession {
    
    self.sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    
    self.session = [[AVCaptureSession alloc] init];
    NSArray <AVCaptureDeviceType> * deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInDualCamera];
    self.videoDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes: deviceTypes
                                                                                              mediaType: AVMediaTypeVideo
                                                                                               position: AVCaptureDevicePositionUnspecified];
    
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession: self.session];
    
    self.previewLayer.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) * 0.1f,
                                         CGRectGetWidth(self.view.bounds),
                                         CGRectGetHeight(self.view.bounds) * 0.7);
    
    [self.previewLayer setBackgroundColor:[UIColor blackColor].CGColor ];
    self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer insertSublayer:self.previewLayer atIndex: 0];
    
    
    
    self.setupResult = AVCamSetupResultSuccess;
    
    switch ([AVCaptureDevice authorizationStatusForMediaType: AVMediaTypeVideo])
    {
        case AVAuthorizationStatusAuthorized:
        {
            // the user has previously granted access to the camera.
            break;
        }
        case AVAuthorizationStatusNotDetermined:
        {
            dispatch_suspend(self.sessionQueue);
            [AVCaptureDevice requestAccessForMediaType: AVMediaTypeVideo
                                     completionHandler:^(BOOL granted) {
                                         if ( !granted) {
                                             self.setupResult = AVCamSetupResultCameraNotAuthorized;
                                         }
                                         dispatch_resume(self.sessionQueue);
                                     }];
            break;
        }
        default:
        {
            self.setupResult = AVCamSetupResultCameraNotAuthorized;
            break;
        }
    }
    
    dispatch_async( self.sessionQueue, ^{
        [self configureSession];
    });
}


- (void) configureSession {
    if (self.setupResult != AVCamSetupResultSuccess) {
        return;
    }
    
    NSError * error = nil;
    
    [self.session beginConfiguration]; // begin configurattion session
    
    self.session.sessionPreset = AVCaptureSessionPresetPhoto;
    
    AVCaptureDevice * videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType: AVCaptureDeviceTypeBuiltInDualCamera
                                                                       mediaType: AVMediaTypeVideo
                                                                        position: AVCaptureDevicePositionBack];
    if (!videoDevice) {
        videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType: AVCaptureDeviceTypeBuiltInWideAngleCamera
                                                         mediaType: AVMediaTypeVideo
                                                          position: AVCaptureDevicePositionBack];
        if ( !videoDevice) {
            videoDevice = [AVCaptureDevice defaultDeviceWithDeviceType: AVCaptureDeviceTypeBuiltInWideAngleCamera
                                                             mediaType: AVMediaTypeVideo
                                                              position: AVCaptureDevicePositionFront];
        }
    }
    
    AVCaptureDeviceInput * videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice: videoDevice error: &error];
    
    if (!videoDeviceInput) {
        DVLog(@"Could not create video device input: %@", error.localizedDescription);
        self.setupResult = AVCamSetupResultSessionConfigurationFailed;
        [self.session commitConfiguration];
        return;
    }
    if ( [self.session canAddInput: videoDeviceInput] ) {
        [self.session addInput: videoDeviceInput];
        self.videoDeviceInput = videoDeviceInput;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIInterfaceOrientation statusBarOrientation = [UIApplication sharedApplication].statusBarOrientation;
            AVCaptureVideoOrientation initialVideoOrientation = AVCaptureVideoOrientationPortrait;
            if ( statusBarOrientation != UIInterfaceOrientationUnknown) {
                initialVideoOrientation = (AVCaptureVideoOrientation) statusBarOrientation;
            }
            self.previewLayer.connection.videoOrientation = initialVideoOrientation;
        });
    }
    else {
        DVLog(@"Could not add video device input to the session");
        self.setupResult = AVCamSetupResultSessionConfigurationFailed;
        [self.session commitConfiguration];
    }
    
    // Add photo output.
    AVCapturePhotoOutput * photoOutput = [[AVCapturePhotoOutput alloc] init];
    if ([self.session canAddOutput: photoOutput]) {
        [self.session addOutput: photoOutput];
        self.photoOutput = photoOutput;
        self.photoOutput.highResolutionCaptureEnabled = YES;
        self.inProgressPhotoCaptureDelegates = [NSMutableDictionary dictionary];
    }
    else {
        DVLog(@"Could not add photo output to the session");
        self.setupResult = AVCamSetupResultSessionConfigurationFailed;
        [self.session commitConfiguration];
        return;
    }
    
    //    self.baclgroundRecordingID = UIBackgroundTaskInvalid;
    [self.session commitConfiguration]; // end configurattion sessio
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    dispatch_async(self.sessionQueue, ^{
        switch (self.setupResult) {
            case AVCamSetupResultSuccess:
            {
                [self addObservers];
                
                [self.session startRunning];
                
                
                
                self.sessionRunning = self.session.isRunning;
                break;
            }
            case AVCamSetupResultCameraNotAuthorized:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * message = NSLocalizedString(@"App doesn't have permission to use the camera, please change privacy settings", @"Allert message when the user has denied access to the camera");
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Photo Diary"
                                                                                              message: message
                                                                                       preferredStyle: UIAlertControllerStyleAlert];
                    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", @"Alert OK button")
                                                                            style: UIAlertActionStyleCancel
                                                                          handler:nil];
                    [alertController addAction: cancelAction];
                    
                    UIAlertAction * settingAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"Settings", @"Alert button to open settings") style: UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL: [NSURL URLWithString: UIApplicationOpenSettingsURLString] options: @{} completionHandler:nil];
                    }];
                    [alertController addAction: settingAction];
                    [ self presentViewController: alertController animated: YES completion:nil];
                });
                break;
            }
            case AVCamSetupResultSessionConfigurationFailed:
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString * message = NSLocalizedString(@"Unable to capture media", @"Alert message when something goes wrong during capture session configuration");
                    UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Photo Diary"
                                                                                              message: message
                                                                                       preferredStyle: UIAlertControllerStyleAlert];
                    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle: NSLocalizedString(@"OK", @"Alert OK button")
                                                                            style: UIAlertActionStyleCancel
                                                                          handler:nil];
                    [alertController addAction: cancelAction];
                    [ self presentViewController: alertController animated: YES completion:nil];
                });
            }
                
                
            default:
                break;
        }
    });
}

#pragma mark - Private Methods

- (void) hideControllsForMakingPhoto: (BOOL) hide {
    if (hide) {
        self.capturePhotoInGallery_Button.hidden = YES;
        self.capturePhotoInGallery_Button.enabled = NO;
        self.capturePhoto_Button.hidden = YES;
        self.capturePhoto_Button.enabled = NO;
        self.changeCamera_Button.hidden = YES;
        self.changeCamera_Button.enabled = NO;
        self.dismiss_Button.hidden = YES;
        self.dismiss_Button.enabled = NO;
    }
    else {
        self.capturePhotoInGallery_Button.hidden = NO;
        self.capturePhotoInGallery_Button.enabled = YES;
        self.capturePhoto_Button.hidden = NO;
        self.capturePhoto_Button.enabled = YES;
        self.changeCamera_Button.hidden = NO;
        self.changeCamera_Button.enabled = YES;
        self.dismiss_Button.hidden = NO;
        self.dismiss_Button.enabled = YES;
    }
}

- (void) hideControllsForSavePhoto: (BOOL) hide {
    if (hide) {
        self.savePhoto_View.hidden = YES;
        self.savePhoto_Button.enabled = NO;
        self.cancelSavePhoto_Button.enabled = NO;
    }
    else {
        self.savePhoto_View.hidden = NO;
        self.savePhoto_Button.enabled = YES;
        self.cancelSavePhoto_Button.enabled = YES;
    }
}

#pragma mark - Observers

- (void) removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    [self.session removeObserver: self forKeyPath: OBSERVER_SESSION_KEY_PATH context: SessionRunningContext];
}

- (void) addObservers {
    [self.session addObserver: self forKeyPath: OBSERVER_SESSION_KEY_PATH options: NSKeyValueObservingOptionNew context: SessionRunningContext];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(subjectAreaDidChange:) name: AVCaptureDeviceSubjectAreaDidChangeNotification object: self.videoDeviceInput.device];
    [[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(sessionRuntimeError:) name: AVCaptureSessionRuntimeErrorNotification object: self.session];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(sessionWasInterrupted:) name: AVCaptureSessionWasInterruptedNotification object: self.session];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(sessionInterruptionEnded:) name:AVCaptureSessionInterruptionEndedNotification object: self.session];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == SessionRunningContext) {
        BOOL isSessionRunning = [change[NSKeyValueChangeNewKey] boolValue];
        
        dispatch_async( dispatch_get_main_queue(), ^{
            
        });
    }
    else
    {
        [super observeValueForKeyPath: keyPath ofObject: object change: change context: context];
    }
}

- (void) subjectAreaDidChange: (NSNotification *) notification {
    CGPoint devicePoint = CGPointMake(0.5, 0.5);
    //    [self focusWithMode]
}

- (void) sessionRuntimeError: (NSNotification *) notification {
    NSError * error = notification.userInfo[AVCaptureSessionErrorKey];
    DVLog(@"Capture session runtime error: %@", error.localizedDescription);
    
    if (error.code == AVErrorMediaServicesWereReset) {
        dispatch_async( self.sessionQueue, ^{
            if (self.isSessionRunning) {
                [self.session startRunning];
                self.sessionRunning = self.session.isRunning;
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //self.resumeButton.hidden = NO;
                });
            }
        });
    }
    else {
        //self.resumeButton.hidden = NO;
    }
}

- (void) sessionWasInterrupted: (NSNotification *) notification {
    BOOL showResumeButton = NO;
    AVCaptureSessionInterruptionReason reason = [notification.userInfo[AVCaptureSessionInterruptionReasonKey] integerValue];
    DVLog(@"Capture session was interrupted with reason %ld", (long)reason);
    
    if (reason == AVCaptureSessionInterruptionReasonAudioDeviceInUseByAnotherClient ||
        reason == AVCaptureSessionInterruptionReasonVideoDeviceInUseByAnotherClient) {
        showResumeButton = YES;
    }
    else if (reason == AVCaptureSessionInterruptionReasonVideoDeviceNotAvailableWithMultipleForegroundApps) {
        
    }
    
    if (showResumeButton) {
        
    }
}

- (void) sessionInterruptionEnded: (NSNotification *) notification {
    DVLog(@"Capture session interruption ended");
    
    
}

#pragma mark - Actions

- (IBAction)cancelSave:(UIButton *)sender {
    [self hideControllsForSavePhoto: YES];
    [self hideControllsForMakingPhoto: NO];
    self.imagePreview_ImageView.image = nil;
    
    __weak DVCameraViewController * weakSelf = self;
    
    dispatch_async( self.sessionQueue, ^{
        if (!weakSelf.isSessionRunning) {
            [weakSelf.session startRunning];
            weakSelf.sessionRunning = weakSelf.session.isRunning;
        }
    });
}
- (IBAction)savePhoto:(UIButton *)sender {
    DVAddEventViewController * addVC = (DVAddEventViewController *) self.presentingViewController;
    [addVC setPhoto: self.image];
    [self dismissViewControllerAnimated: YES completion: nil];
}
- (IBAction)changeCamera:(id)sender {
    dispatch_async(self.sessionQueue, ^{
        AVCaptureDevice * currentVideoDevice = self.videoDeviceInput.device;
        AVCaptureDevicePosition currentPosition = currentVideoDevice.position;
        
        AVCaptureDevicePosition prefferdPosition;
        AVCaptureDeviceType prefferedDeviceType;
        
        switch (currentPosition)
        {
            case AVCaptureDevicePositionUnspecified:
            case AVCaptureDevicePositionFront:
                prefferdPosition = AVCaptureDevicePositionBack;
                prefferedDeviceType = AVCaptureDeviceTypeBuiltInDualCamera;
                break;
            case AVCaptureDevicePositionBack:
                prefferdPosition = AVCaptureDevicePositionFront;
                prefferedDeviceType = AVCaptureDeviceTypeBuiltInWideAngleCamera;
                break;
        }
        
        NSArray <AVCaptureDevice *> * devices = self.videoDeviceDiscoverySession.devices;
        AVCaptureDevice * newVideoDevice  = nil;
        
        for (AVCaptureDevice * device in devices) {
            if ( device.position == prefferdPosition && [device.deviceType isEqualToString: prefferedDeviceType]) {
                newVideoDevice = device;
                break;
            }
        }
        
        if ( !newVideoDevice ) {
            for (AVCaptureDevice * device in devices) {
                if ( device.position == prefferdPosition) {
                    newVideoDevice = device;
                    break;
                }
            }
        }
        
        if ( newVideoDevice ) {
            AVCaptureDeviceInput * videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:newVideoDevice error:nil];
            [self.session beginConfiguration];
            
            [self.session removeInput: self.videoDeviceInput];
            
            if ( [self.session canAddInput: videoDeviceInput]) {
                [[NSNotificationCenter defaultCenter] removeObserver: self
                                                                name: AVCaptureDeviceSubjectAreaDidChangeNotification
                                                              object: currentVideoDevice];
                [[NSNotificationCenter defaultCenter] addObserver: self
                                                         selector: @selector(subjectAreaDidChange:)
                                                             name: AVCaptureDeviceSubjectAreaDidChangeNotification
                                                           object: newVideoDevice];
                [self.session addInput: videoDeviceInput];
                self.videoDeviceInput = videoDeviceInput;
            }
            else {
                [self.session addInput: self.videoDeviceInput];
            }
            AVCaptureConnection * movieFileOutputConnection = [self.movieFileOutput connectionWithMediaType: AVMediaTypeVideo];
            
            if (movieFileOutputConnection.isVideoStabilizationSupported) {
                movieFileOutputConnection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationModeAuto;
            }
            
            [self.session commitConfiguration];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            });
        }
        
    });
}

- (IBAction)capturePhoto:(id)sender {
    AVCaptureVideoOrientation videoPreviewLayerVideoOrientation = self.previewLayer.connection.videoOrientation;
    
    dispatch_async( self.sessionQueue, ^{
        AVCaptureConnection * photoOutputConnection = [self.photoOutput connectionWithMediaType: AVMediaTypeVideo];
        photoOutputConnection.videoOrientation = videoPreviewLayerVideoOrientation;
        
        AVCapturePhotoSettings * photoSettings = [AVCapturePhotoSettings photoSettings];
        photoSettings.flashMode = AVCaptureFlashModeAuto;
        photoSettings.highResolutionPhotoEnabled = YES;
        if ( photoSettings.availablePreviewPhotoPixelFormatTypes.count > 0) {
            photoSettings.previewPhotoFormat = @{(NSString *) kCVPixelBufferPixelFormatTypeKey : photoSettings.availablePreviewPhotoPixelFormatTypes.firstObject};
        }
        [self.photoOutput capturePhotoWithSettings: photoSettings delegate: self];
    });
}


- (IBAction)addImageFromLibrary: (UIButton *)sender {
    
    dispatch_async(self.sessionQueue, ^{
        if (self.isSessionRunning) {
            [self.session stopRunning];
            self.sessionRunning = self.session.isRunning;
        }
    });
    
    
    
    
    
    
    _cameraView = [[CameraSessionView alloc] initWithFrame:self.view.frame];
    _cameraView.delegate = self;
    [self.view addSubview:_cameraView];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}



- (IBAction)dismissButton:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}



#pragma mark - <AVCapturePhotoCaptureDelegate>

- (void)captureOutput:(AVCapturePhotoOutput *)output didFinishProcessingPhoto:(AVCapturePhoto *)photo error:(NSError *)error
{
    if (error) {
        NSLog(@"error : %@", error.localizedDescription);
    }
    __weak DVCameraViewController * weakSelf = self;
    
    dispatch_async( weakSelf.sessionQueue, ^{
        
        if (weakSelf.isSessionRunning) {
            [weakSelf.session stopRunning];
            weakSelf.sessionRunning = weakSelf.session.isRunning;
        }
        
        if (photo) {
            NSData * data = [photo fileDataRepresentation];
            UIImage * image = [UIImage imageWithData: data];
            self.image = image;
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration: 0.2f animations:^{
                    weakSelf.imagePreview_ImageView.contentMode = UIViewContentModeScaleAspectFill;
                    weakSelf.imagePreview_ImageView.image = image;
                    [weakSelf hideControllsForMakingPhoto: YES];
                    [weakSelf hideControllsForSavePhoto: NO];
                }];
            });
        }
    });
    
    
}















@end
*/
