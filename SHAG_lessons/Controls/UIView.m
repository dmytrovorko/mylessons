//
//  NavigationBar.m
//  SHAG_lessons
//
//  Created by Дмитрий on 13.08.2018.
//  Copyright © 2018 Vara. All rights reserved.
//

#import <Foundation/Foundation.h>


#pragma mark - круглые углы на выбранных углах
/*
// labelHeader - left round corners
self.headerLabel.layer.cornerRadius = 5.f;
self.headerLabel.clipsToBounds = YES;
self.headerLabel.layer.maskedCorners =  kCALayerMinXMaxYCorner | kCALayerMinXMinYCorner;
*/


#pragma mark - используется для инициализации
/*
-(id)initWithCoder:(NSCoder*)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
    }
    
    return self;
}
 */

#pragma mark - Drawing and Bezier Path
/*
CGContextSetFillColorWithColor(context, fillRectangles.CGColor);
UIBezierPath * bezierPath =  [UIBezierPath bezierPathWithRoundedRect: rectForLeftRectangle
                                                   byRoundingCorners: UIRectCornerTopLeft | UIRectCornerBottomLeft
                                                         cornerRadii:CGSizeMake(CORNER_RADIUS, CORNER_RADIUS)];
[bezierPath fill];

bezierPath =  [UIBezierPath bezierPathWithRoundedRect: rectForCenterRectangle cornerRadius: 0];
[bezierPath fill];

bezierPath =  [UIBezierPath bezierPathWithRoundedRect: rectForRightRectangle
                                    byRoundingCorners: UIRectCornerTopRight | UIRectCornerBottomRight
                                          cornerRadii:CGSizeMake(CORNER_RADIUS, CORNER_RADIUS)];
[bezierPath fill];
*/

/*
- (void)drawRect:(CGRect)rect {
    self.layer.cornerRadius = CORNER_RADIUS;
    self.layer.masksToBounds = YES;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    NSInteger choise = 1;
    if (choise == 0) {  // drawing rectangle
        // drawing rectangle
        CGFloat strokeDistance = 25;
        CGPoint centerPoint = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2 );
        CGPoint lowerLeftCorner = CGPointMake(centerPoint.x - strokeDistance, centerPoint.y + strokeDistance);
        CGPoint lowerRightCorner = CGPointMake(centerPoint.x + strokeDistance, centerPoint.y + strokeDistance);
        CGPoint upperRightCorner = CGPointMake(centerPoint.x + strokeDistance, centerPoint.y - strokeDistance * 2);
        CGPoint upperLeftCorner = CGPointMake(centerPoint.x - strokeDistance, centerPoint.y - strokeDistance * 2);
        
        CGContextSetLineCap(context, kCGLineCapSquare);
        CGContextSetLineWidth(context, 8.0);
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
        
        CGContextMoveToPoint(context, lowerLeftCorner.x, lowerLeftCorner.y);
        CGContextAddLineToPoint(context, upperLeftCorner.x, upperLeftCorner.y);
        CGContextAddLineToPoint(context, upperRightCorner.x, upperRightCorner.y);
        CGContextAddLineToPoint(context, lowerRightCorner.x, lowerRightCorner.y);
        CGContextAddLineToPoint(context, lowerLeftCorner.x, lowerLeftCorner.y);
    }
    else if (choise == 1) { // draw circle
        CGPoint centerPoint = CGPointMake(CGRectGetWidth(self.bounds) / 2, CGRectGetHeight(self.bounds) / 2 );
        
        CGContextAddArc(context, centerPoint.x , centerPoint.y, 70,  (0 * M_PI / 180), (360 * M_PI / 180), 0);
        
        CGContextSetLineCap(context, kCGLineCapSquare);
        CGContextSetLineWidth(context, 2.0);
        CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
    }
    
    CGContextStrokePath(context);
    
}
 */
