
import Foundation

// MARK: - ConstantsVariables

// MARK: __Lets

/*
 // константы не могут изменять значения
 // справа сразу можно видеть результат и полученные значения
 
 let const1 = 5 // по-умолчанию тип становится Int (исходя из значения)
 type(of: const1)
 let const2:Int //  указываем тип явно (т.к. не указано значение)
 let const3:Int = 5 // полная запись
 //const3 = 5;// нельзя присвоить новое значение в константу
 
 //print(const2)
 
 // по-умолчанию тип становится Double
 let constDouble = 5.0
 //let constInt: Int = 20.1 // ошибка компиляции
 
 // нажмите Alt и левой кнопкой мыши, чтобы узнать тип переменной и получить информацию
 
 //let c11 // ошибка, нужно указывать тип и/или значение, т.к. компилятор не может определить
 
 let с3:Double=4 // без пробелов
 //let с4:Double =4 // ошибка
 
 
 let c6: Int = 51// символ ; не обязателен, если нет других операторов в этой же строке
 let c7 = 3; let c8 = 1// ; обязательно, т.к. несколько операторов в одной строке
 let c9 = 1 as Double// другая запись типа
 
 // "верблюжья" нотация именования (в java все константы указываются большими буквами и через _)
 let someConstName: Int
 */


// MARK: __Var


/*
 // переменные могут изменять значения
 
 var v1 = 6
 v1 += 3 // v1 = v1 + 3
 
 //v1++ // инкремент нельзя использовать, как в Java
 v1 += 1
 
 var v2 = v1 * 1000 // присвоение результата
 
 print(v1, v2)  // print - глобальная функция для печати
 
 // в playground можно не писать print, т.к. информация будет выводиться в любом случае
 v1
 
 
 //var v3 // ошибка, не может определить тип
 var v4: Double = 14 // если бы не указали тип Double - то компилятор определил бы как Int
 //var v5: Int = v4// ошибка: разные типы
 
 var v5 = Int(v4) // конвертация типа
 
 //v4+v5 // ошибка  binary operator '+' cannot be applied to operands of type 'Double' and 'Int'
 v4+Double(v5) //  нужно сконвертировать в один тип
 
 
 // объявления в одну строку
 var one,two,three: Int
 var four = 4, five = 5, six = 6
 
 
 // используйте комбинация var + Tab или let + Tab для быстрого создания переменных и констант (вместо Tab можно использовать Enter)
 */




// MARK: - Types

// MARK: __Int

/*
 // Int
 // в 64-разрядных системах Int означает Int64, в 32-разрядных системах Int означает Int32
 // UInt может хранить только положительные числа, Int - любые
 
 let v1: Int = 100
 //let v2: Int8 = 200 // нельзя присвоить 200
 let v3: Int16 = 500
 let v4: Int32 = 600
 let v5: Int64 = 900
 
 // по-умолчанию не присваивается значение (в данном случае 0), в Java автоматически присваивается значение 0 (если внутри класса), если ничего не указано
 var v6: Int;
 // print(v6) // ошибка - использование переменной до инициализации (для решения нужно использовать тип Optional - об этом в следующих уроках)
 
 
 // types
 var vt1 = 5
 var vt2 = vt1
 vt2 = 7 // значение vt1 не меняется, т.к. value type
 vt1
 
 */

// MARK: __String

/*
 // String - текстовый тип, набор символов
 // Character - один символ
 
 // в Java String является ссылочным типом, в Swift - value type
 
 let s1 = "first word"
 let i1 = 100
 let s2 = "my text is \"\(s1)\" and number = \(i1)" // использование кавычек внутри текста, подстановка значений
 
 let c1 = "a" // по-умолчанию присваивается тип String
 type(of: c1)
 
 let c2: Character = "a"
 type(of: c2)
 
 //let c3: Character = "ab"// нельзя
 
 let s3 = String(1+3)// конвертация (сначал посчитает 1+3, затем сконвертирует в String)
 
 //let s4 = 'test' // одинарные кавычки нельзя использовать
 
 
 
 // сравнение строк
 var newStr = "test str"
 var newStr2 = "test str"
 
 // используем == для сравнения String значений (т.к. value type), = для присвоения (в Java для String нужно использовать equals, т.к. там это объектный тип)
 
 newStr == newStr2 // сравнение
 newStr = newStr2 // присвоение
 
 
 var st1 = "👨‍👩‍👦‍👦" // emoji можно вставлять из меню Edit
 var st2 = st1
 st2 = "3" // значение st1 не меняется, т.к. value type
 st1
 
 // многострочное значение (через тройные кавычки)
 let strLines = """
 In addition to familiar types, Swift introduces advanced types not found in Objective-C, such as tuples.
 Tuples enable you to create and pass around groupings of values.
 You can use a tuple to return multiple values from a function as a single compound value.
 """
 
 // символ юникод
 let unicodeSymbol = "\u{3030}"
 */


// MARK: __Substring
/*
 /* часто используемый тип Substring при извлечении одной строки и другой
 String - занимает новые области памяти
 Substring - использует ту же память, которая была зарезервирована под String, из которого выделяем Substring
 
 */
 
 let text1 = "swift 1 2 3"
 let text2 = text1.dropLast(3)
 
 text2 // занимает часть памяти text1
 */

/*
 
 /*
 - отличие Float от Double: Double - 64 битное, более точное число с плавающей точкой, больше чисел после запятой, Float: 32 бита
 
 - рекомендуется всегда использовать тип Double
 - Double и Float могут быть как положительными, так и отрицательными
 
 */
 
 
 //MARK: __Double
 let d1 = 13.4 // по-умолчанию присваивается тип Double
 type(of: d1)
 
 
 //v1-d1 // ошибка компиляции, типы не совпадают
 //Double(v1)-d1 // нужно сконвертировать v1
 
 let f1: Float = 15.1 // чтобы объявить Float - нужно явно прописать этот тип
 type(of: f1)
 */

/*
 
 
 //MARK: __BOOL
 // тип Bool часто используется в условиях, циклах и пр.
 // может принимать одно из значений: true или false
 
 
 let b1:Bool = true
 
 // Bool присваивается автоматически
 let b2 = false
 
 
 // по-умолчанию не присваивается значение false
 let b3:Bool
 //print(b3) // ошибка
 */

/*
 
 // MARK: __convert
 // конвертация
 
 var str1 = "5"
 var int1 = 10
 
 //print(str1+int1)// ошибка, разные типы
 
 let newVal = Int(str1)
 
 
 print(str1 + String(int1))
 */

// MARK: __typealias

/*
 // создание своего алиаса на любой существующий тип (часто используется для определенных классов, кортежей, упрощает чтение кода)
 // часто будете использовать в своих проектах
 
 typealias MyString = String; // нотация именования - название своего типа в typealias всегда начинается с большой буквы
 
 
 let newStr: MyString = "test str";
 type(of: newStr)
 
 
 // кортежи далее будем разбирать более подробно, сейчас пока для примера работы с typealias
 typealias PersonTuple = (firstName: String, secondName: String, age: Int, car: Bool)
 
 var personIvan: PersonTuple = ("Ivan", "Ivanov", 20, false)
 var personOleg: PersonTuple = ("Oleg", "Olegov", 42, true)
 */


// MARK: - Tuples

// MARK: __Base
/*

// типы значений в кортеже могут быть любыми

let t1 = ("test", 1) // если указан let (конcтанта), то нельзя присвоить новое значения для элементов кортежа
let t2 = (2, 3, 4)
var t3 = (true, "test2", 7)

// метки для значений, чтобы получать значение по имени
let t4 = (a: 1, b: 2, c: 3)
t4.b

// допустимы любые типы в кортеже
let t5 = (text: "resume", count: 51, people: ["Имя1", "Имя2"])

let (x, y, z) = (1, 2, 3)// массовое объявление переменных (если типы переменных разные - рекомендуется использовать объявление отдельных переменных)
print(x)
print(y)
print(z)


// получение доступа к значениям кортежа
print("\(t1.0), \(t1.1)") // доступ по индексу (индексация с нуля)
print(t5.text) // по метке


// изменение значения (также по индексу или имени)

//t1.0 = "value" // ошибка, т.к. константа

t3.1 = "new value"
t3.2 = 6

print(t3)



// желательно всегда давать имена переменных кортежей (чтобы не было путаницы)
var tp = (a:"А", b: "Б", c:"Ц", d: "Д", e:"Е", f: "Ф")
tp.5 // трудно сразу понять, к чему обращаемся
tp.e // обращение по имени понятней, чем по индексу, особенно если много переменных в кортеже
*/

// MARK: __typealias

/*
// если в коде часто используется большой кортеж - его можно заменить на свой тип, чтобы код было легче читать
// привыкайте использовать typealias в своих проектах

typealias PersonTuple = (firstName: String, secondName: String, age: Int, car: Bool)



var personIvan: PersonTuple = ("Tom", "Jerry", 20, false)
var personOleg: PersonTuple = ("Piter", "Smith", 42, true)


// для сравнения, без использования typealias (труднее читать код)
var personIvan2: (firstName: String, secondName: String, age: Int, car: Bool) = ("Tom", "Jerry", 20, false)
var personOleg2: (firstName: String, secondName: String, age: Int, car: Bool) = ("Piter", "Smith", 42, true)



typealias CarTuple = (name: String, model: String)


// вложенные кортежи
var multiPerson: (PersonTuple, CarTuple)

// вариант без typealias для сравнения (код получается больше)
var multiPerson2: ((firstName: String, secondName: String, age: Int, car: Bool), (name: String, model: String))

multiPerson = (("Tom", "Jerry", 20, false),("test name", "test model"))

multiPerson.1.name
*/

// MARK: __Var

/*
 // разложение кортежа по переменным
 
 let t = (2, 3, 4)
 
 // ранее кортеж t не имел своих меток
 let (a, b, c) = t
 print(a)
 print(b)
 print(c)
 
 let (o, p, _) = t // можно раскладывать не все значения, а только необходимые
 print(o)
 print(p)

*/

// MARK: __Inner

/*
 // кортежи внутри кортежа
 
 // получение значений последовательно по индексу
 var inner1: (Int, (Int, Int)) = (5, (5, 25))
 
 print(inner1.1.1)
 
 
 // получение значений сначала по имени, затем по индексу
 var inner2  = (5, innerTuple: (5, "tuple value"))
 
 print(inner2.innerTuple.1)
*/

// MARK: __ByVal

/*
 // доступ по значению (не по ссылке)
 
 var tuple1 = ("v1", "v2")
 
 var tuple2 = tuple1
 tuple2.0 = "newV1"
 
 print(tuple1.0)
 print(tuple2.0)
 

*/

// MARK: - Conditions

// MARK: __if



 // стандартное применение if
 
/*
let letterA = "А"

if letterA == "A"{// скобки обязательны
    print("true") // почему не выполняется условие ?? следите за раскладкой клавиатуры (язык)
}else if letterA == "B"{
    print("else1")
}else if letterA == "С"{
    print("else2")
}else {
    print("nothing")
}


// символ "!" - отрицание (как и в Java)
if (letterA != "A"){
    print("not a")
}


// тернарный (тройной) условный оператор - сокращенная запись if

let letterB = "B"

letterB == "B" ? print("true B") : print("not b") // тернарный оператор нужно использовать аккуратно, т.к. усложняет чтение кода

// можно использовать в выражениях
let letters = letterA + (!letterB.isEmpty ? letterB : "")
print(letters)
*/



// MARK: __Switch


/*
 // switch - в отличие от Java - не нужно в каждом case писать break для выхода из switch
 // если будет найден нужный case - выход из switch производится автоматически
 
 let text: String = "test55"
 
 
 // нельзя оставлять пустым тело case и default

switch text {
    
case "test": // фигурные скобки не нужны
    print("1")
    
case "test2", "test3": // несколько значений case
    print("2")
    
    // case "test4": // нельзя оставлять пустым
    
default: // default обязателен
    print("true default 1")
    
}



switch text {
    
// не рекомендуется использовать именно так, показано для примера
case _: // подходит для любых значений text - поэтому все нижние case и default не будут выполняться
    print("i'm default")
    
case "test":
    print("1")
    
case "test2":
    print("2")
    
default:
    print("true default 2")
}


*/

// MARK: __where



 /*
 var a = 11
 
 // условие в case
 
 switch a {
 case _ where a % 3 == 2: // условие внутри блока case, в java это не поддерживается, но можно реализовать (не так элегантно)
 print(a)
 
 default:
 print("")
 
 }
 */

// MARK: __fallthrough


/*
// fallthrough - если нужно поведение switch как в Java
// даже если будет найден нужный case - все равно выполняется следующий (если специально не выходить из case)



var a = 2

switch a {
case 1:
    print("a=1")
    fallthrough
case 2:
    print("a=2")
    fallthrough
case 3:
    print("a=3")
    
default:
    print("default")
    
}
*/

// MARK: __SwitchRange

// условие с range (вхождение в интервал значений)

/*
var a = 17


switch a {
case 1..<10:
    print("<10")
    
case 10..<20: // будет выполнен этот case как первое подходящее условие
    print(">10")
    
case 20..<30:
    print(">20")
    
default:
    print("")
    
}
*/

// MARK: __Tuple

/*
// switch + tuple, "pattern matching"

let letters = (a: "А", b: "Б", c: "Ц", d: "Д", e: "Е")

switch letters {
case (_, _, _, _, _): // по смыслу заменяет блок default, поэтому нижние case никогда не выполнятся
    print("любые значения")
case ("А", _, _, _, _):
    print("найдена А")
case (_, _, "Ц", _, "Е"):
    print("найдены Ц и Е")
default:        // 5
    print("default")
}
*/


// MARK: - Loops

// MARK: __Ranges

// range - диапазон значений определенного типа (часто используется в условиях, циклах, коллекциях и пр.)


/*
// Closed Range
let first10digits = 0...10 // от 0 до 10 включительно

// Half-Open Range
let first9digits = 0..<10 // не включает 10

// let range = 0>..10 // так нельзя и не имеет смысла

// One-Sided Range
let less10 = ...10 // все значения меньше 10 (часто применяется для получения элементов коллекции по индексу)

let bigger10 = 10... // все значения больше 10

type(of: first10digits)
*/

// MARK: __For


// можно использовать continue (перейти к следующей итерации) и break (выход из цикла)

/*
let first10digits = 1...10

for number in first10digits {
    print(number);
}


// использование в циклах

var sum = 0;

for i in 1...10{
    print(i, terminator: "")// печать без перехода на новую строку
    
    if i == 8{
        break
    }
}

print("")
print("---")

for _ in 1..<10{ // переменная не нужна
    print("no var")
}

print("")
print("---")


// реверсия

for number in first10digits.reversed() {
    if number == 4{ // 4 не печатать
        continue
    }
    print(number)
}
*/

// MARK: __Label

// переход к нужной метке

/*

for1: for i in 1...10{
    for2: for j in 1...5{
        
        
        if i % 3 == 0{
            break for1 // если не указывать метку - выход будет производиться только из внутреннего цикла
        }
        
        print(i, j)
    }
}

print("----")


for1: for i in 1...10{
    for2: for j in 1...5{
        
        if i % 3 == 0{
            continue for1 // если не указывать метку - continue будет производиться только для внутреннего цикла
        }
        
        print(i, j)
    }
}

print("----")


for1: for i in 1...10{
    for2: for j in 1...5{
        
        if j % 3 == 0{
            continue for2
        }
        
        print(i, j)
    }
}
*/

// MARK: __Where


/*
// цикл с условием
for i in 1..<10 where i % 3 == 0{
    print("\(i) - ok")
}


// для сравнения - цикл с условием, если не использовать where - код становится более громоздким
for i in 1..<10{
    if (i % 3 == 0) {
        continue // перейти на следующий шаг цикла
    }
    print(i)
}
*/

// MARK: __Inner


/*
// вложенные циклы, как в Java и других C языках

var sum = 0

for number1 in 1...10 {
    sum += number1
    for number2 in 1...10 {
        sum += number2
    }
}

print(sum)
*/


// MARK: __While


// while

/*
var i = 0

while i<10{ // если условие сразу false - ни один шаг цикла не будет выполнен
    // i++ // так нельзя, как в Java, т.к. операторы инкременты и декремента удалены с версии Swift 3
    i += 1
    
    if i == 5 {
        continue // переход на следующий шаг
    }
    
    print(i)
}


print("-----")

// Repeat-While

var i2 = 0

repeat { // как минимум одна итерация цикла будет выполнена, т.к. проверка условия идет в конце
    
    i2 -= 1
    print(i2)
    
} while (i2>0)
*/


// MARK: __Stride

/*
/* для тех, кто привык к стандартным циклам Java
 
 for(int x = 10; x < 20; x = x + 2) {
 
 }
 
 функция stride ("шагать")
 
 */


for i in stride(from: 10, to: 20, by: 2) { // 20 не включается
    print(i)
}
print()

for i in stride(from: 10, through: 20, by: 2) { // 20 включается
    print(i)
}

print()

// обратно
for i in stride(from: 20, through: 10, by: -3) {
    print(i)
}
*/


// MARK: - Optionals

// MARK: __Base

/*
// пустой контейнер типа Int (по-умолчанию присваивается значение nil)
var intOptional:Int? // знак ? означает "упаковку" в контейнер (в данном случае Int)


// можно объявлять так (но это избыточно):
// var intOptional:Optional<Int>

// при попытке использовать Optional переменную - компилятор укажет не ошибку
//intOptional += 1

// контейнер типа Int со значением 10
var intOptionalWithValue: Int? = 10

// печатает контейнер, а не само значение
print(intOptionalWithValue)

// для работы со значением - нужно его извлечь из контейнера

// force unwrapping - явное, принудительное извлечение значения (в данном случае тип Int со значением 10)
print(intOptionalWithValue!)

// ошибки
//print(intOptional!) // извлечение значения из пустого контейнера

//print(intOptional+5) // нельзя использовать Optional напрямую, надо всегда сначала извлекать значение

//intOptional = 11 // 11 автоматически попадает в контейнер
//print(intOptional!+5) // пустое значение при извлечении (не проверяем на nil перед использованием) - такую запись можно использовать, если ранее присвоили значение




// implicit unwrapping (неявное извлечение) - если точно известно, что есть значение, можно извлекать автоматически
var str: String! // везде, где будет использоваться эта переменная - будет автоматическое извлечение
//str.count // ошибка - попытались автоматически извлечь значение, а там nil

str = "test value"

str = str + " new" // автоматическое извлечение и добавление текста





//var i: Int = nil // нельзя присваивать nil, т.к. тип не Optional

//var i = 0
//i = nil
*/

// MARK: __Optional Binding

/*

 //optional chaining - опциональная цепочка
 
 

var str: String? // создаем переменную типа String, по-умолчанию присваивается nil

// если str не nil, тогда посчитается кол-во символов
var count = str?.characters.count // здесь ? означает: если значение str не пусто - выполнить дальнейшие действия, иначе присвоить nil (count имеет тип Optional)


//var count = str!.characters.count  // ошибка, в отличие от варианта с ? - код дальше не выполняется (не пытается получить characters)

// если не использовать optional chaining - получается больше кода:
if let str = str{
    count = str.characters.count
}
// в Java нужно было бы отдельно прописать условие, if str != null {count = str.length}




str = "test"

// переменная countOptional также будет иметь тип Optional (а не Int)
var countOptional = str?.characters.count
print(countOptional!) // поэтому нужно обязательно извлекать значение из контейнера


str = nil


//countOptional = str?.characters.count
//print(countOptional!) // ошибка
*/

// MARK: __NilCoalescing

/*
var str: String?

var str2 = str ?? "default value" // значение по-умолчанию должно быть того же типа, что и Optional контейнер (в данном случае String)

print(str2)
*/

// MARK: __Tuple


/*
// force unwrapping

// опциональный кортеж (контейнер над самим кортежем) - требуется извлечение всего кортежа
var optTuple: (Int, str: String)? = (6, "test")
print(optTuple!.0)
print(optTuple!.str)


// опциональные значения кортежа - требуется извлечение каждого значения
var optTupleValues: (Int?, String?) = (1, "test2")
print(optTupleValues.0!)
print(optTupleValues.1!)


// опциональный кортеж и его значения - - требуется извлечение всего кортежа и каждого значения
var optTupleWithValues: (Int?, String?)? = (5, "test3")
print(optTupleWithValues!.0!)
print(optTupleWithValues!.1!)


// implicit unwrapping


var optTuple2: (Int, str: String)! = (11, "str")
print(optTuple2.0)
print(optTuple2.str)


// нельзя так делать, только для верхнего уровня кортежа
//var optTupleValues2: (Int!, String!) = (1, "str2")
//print(optTupleValues2.0)
//print(optTupleValues2.1)
*/


// MARK: __Convert


/*
var strValue = "5"
var intValue = 10



// при конвертации возвращается Optional
let intVal = Int(strValue)
type(of: intVal)

let doubleVal = Double(strValue)
type(of: doubleVal)

// после конвертации нельзя сразу начать работать со значением, нужно извлечь его из контейнера Optional
//print(intVal + 10) // ошибка

print(intVal! + 10) // force unwrapping

let strConvert = String(5)
type(of: strConvert)



// при конвертации возвращается String
let convertedFromInt = String(intValue)
print(convertedFromInt)
type(of: convertedFromInt)
*/

// MARK: __Switch


/*
// switch вместе с optional

let str: String? = "text"

switch (str) {
case .none:
    print("no text")
case .some (let txt): // извлечение значения из Optional
    print("text is: \(txt)")
}
*/

// MARK: __Inner


/*
// контейнер внутри контейнера
let constVal: String????? = "test"

print(constVal)
print(constVal!!!!!)
*/

// MARK: __BindingChaining

/*
// optional binding + optional chaining

var str:String? = "test"

if let count = str?.characters.count{
    print(count)
}else{
    print("count is nil")
}


// то же самое без optional chaining


if str != nil {
    let count = str!.characters.count
    print(count)
}else{
    print("count is nil")
}
*/
       
// MARK: - Functions

// MARK: __Base

/*
 функция - блок кода для выполнения определенной задачи
 
 желательно, чтобы функция выполняла только свою работу (но дополнительно может вызывать другие функции)
 
 - параметры (внешние - метка при вызове, внутренние - используются внутри функции, хотя по факту это одни и те же переменные)
 
 
 по сравнению с Java - функции в Swift имеют больше возможностей
 
 */

/*

// объявление функции без параметров и типа возвращаемого значения
func printReady(){
    print("ready")
}

// вызов функции
printReady()




// функция с параметрами
func sendParam(str: String, index: Int){// параметры являются константами по-умолчанию, поэтому в них не могут быть присвоены другие значения
    // str="new value" // ошибка (если хотите создать копию значения параметра - создайте новую переменную через var)
    print("\(str),\(index)")
}

// при вызове нужно обязательно указывать имена параметров
sendParam(str: "test", index: 12)


// функция с возвратом результата (может возвращать любой тип, в том числе и другую функцию)
func sum(int1: Int, int2: Int) -> Int{
    return int1+int2;
}


sum(int1: 2, int2: 5)



// другие записи функций, которые не возвращают результат (пустой кортеж: public typealias Void = ())
func returnVoid() -> Void{
    print("void1")
}

func returnVoid2() -> (){
    print("void2")
}

func returnVoid3(){
    print("void3")
}


// рекомендуется использовать  Void  вместо (), повышается чтение кода, когда рядом много других скобок

returnVoid()
returnVoid2()
*/

// MARK: __DefaultParamValue


/*
// можно выставить значения по-умолчанию для параметров

func useDefaultValues(str: String = "default value", index: Int = 5){
    print("\(str), \(index)")
}

// варианты вызова функции

useDefaultValues() // будут использоваться значения параметров по-умолчанию

useDefaultValues(str: "new text") // заменяем значение для str

useDefaultValues(str: "new text 2", index: 15) // заменяем значения для str и index
*/

// MARK: __InOut
/*
 /*
 
 функция изменяет значение внешней переменной (параметр метода хранит ссылку на переменную, а не копию значения)
 в этом случае параметр функции - не константа, может изменяться
 
 */
 
 func inOutFunc(index: inout Int){
 index += 1;
 }
 
 var indexVar = 7
 
 inOutFunc(index: &indexVar)
 
 print(indexVar)
*/


// MARK: __Label
/*

// label - метки нужны при вызове функции, чтобы понимать, за что отвечает параметр
// метка - это не имя переменной, а ее более подробное описание
// понятия "external parameter name" и "internal parameter name"


func convertText(textForConversion text: String, index: Int){ // если не указываете метку - она автоматически создается от имени параметра
    print("\(text),\(index)")
}

convertText(textForConversion: "my new text", index: 1) // при вызове метода - обязательно нужно указывать имена внешних параметров - их метки


// "отключение" меток, можно отключать для всех или для некоторых параметров
func omitLabels(_ p1: Int, _ p2:Int, str: String){ // не забывайте ставить пробел после "_"
    print("\(p1),\(p2)")
}

omitLabels(5, 7, str: "test")// при вызове - не нужно указывать первые 2 метки параметров (более привычный вариант для Java программистов)
*/



// MARK: __Tuples

// функция с возратом кортежа (tuple)

/*
func sum(int1:Int, int2:Int) -> (Int, String) {
    return (int1 + int2, "success")
}

sum(int1: 2, int2: 5)

var tupleResult = sum(int1: 12, int2: 24)

tupleResult.0 // сумма
tupleResult.1 // доп. описание



// функция с возратом кортежа (tuple) и параметром типа кортежа
func funcTuple(tupleParam p: (Int, Int)) -> (Int, String) {
    return (p.0 + p.1, "success")
}

print(funcTuple(tupleParam: (2, 5)))// для кортежа опущены метки параметров




// добавлены имена параметров возвращаемого кортежа (для удобства)
func sum2(tupleParam p: (Int, Int)) -> (sum: Int, result: String) {
    return (p.0 + p.1, "success")
}


var sumTuple = sum2(tupleParam: (2, 5))
sumTuple.result
sumTuple.sum
*/


// MARK: __FuncAsVar

/*
// функция - это тип в Swift (может присваиваться в переменную, константу, параметр функции) - в Java напрямую это не поддерживается (через Refleсtion)
// многие Java программисты не могут привыкнуть к этим возможностям

//  first-class function - функция первого класса - можно присваивать в переменную, передавать как параметр, возвращать как значение



func addStrValue(_ str:String) -> String{
    return str + " added value"
}

// в эту переменную можно присвоить функцию
var addStrMethod = addStrValue // говорят, что присваивается указатель на функцию

addStrMethod("new") // вызываем метод с помощью переменной


// в эту переменную можно присвоить метод только с подходяшими параметрами и возвращаемым значением
// такие переменные имеют "функциональный тип" (параметры + возвращаемые значения)

func sum(_ int1:Int, _ int2:Int) -> Int{
    return int1 + int2;
}

var sumMethod: (Int, Int) -> Int = sum

sumMethod(1,2)

//sumMethod = addStrMethod // оишбка, нельзя присвоить, т.к. параметры и возвращаемый тип не совпадают




// другие способы присвоения

func printString(str: String){
    print("var \(str)")
}

func printFixedStr(){
    print("fixed str")
}


var emptyType:() -> ()  = printFixedStr // в переменную emptyType можно присвоить только те функции, которые не принимают параметры и не возвращают значение
// var emptyType:() -> Void = printFixedStr

//var emptyType = printFixedStr  // вариант без указания типа переменной (тип автоматически будет браться из присвоенной функции) - меньше кода, но сложнее читать сам код

emptyType() // вызов функции через переменную


var printVar: (String) -> Void  = printString  // если написать printString() - ошибка, т.к. это попытка вызова функции
printVar("str")
*/


// MARK: __FuncAsReturn
/*

// функция, как тип возвращаемого значения другой функции

func plus(p1: Int, p2: Int) -> Int {
    return p1 + p2
}

func minus(p1: Int, p2: Int) -> Int {
    return p1 - p2
}


func returnFuncType(oper: Bool) -> (Int, Int) -> Int { // возвращает функцию в зависимости от параметра
    if oper{
        return plus
    }else{
        return minus
    }
}

returnFuncType(oper: true)(5,2) // вызов состоит из 2-х частей: сначала возвращается сама функция, затем она вызывается с параметрами



/* не путайте когда возвращается:
 - результат работы функции
 - сама функция (ссылка на функцию)
 */
*/


// MARK: __FuncAsParam

/*
// функция как параметр другой функции


func sum(p1: Int, p2: Int) -> Int{
    return p1+p2;
}

func extract(p1: Int, p2: Int) -> Int{
    return p1-p2;
}


func funcParam(funcType: (Int, Int) -> Int, _ a:Int, _ b:Int){ // можно смотреть историю вызова функции через Value History
    print(funcType(a,b))
}


funcParam(funcType: sum, 2,5)
funcParam(funcType: extract, 5,1)
*/

// MARK: __Optional

/*
// использование optional для функций

func testParamOptional(str:String?){
    
    if let str = str{
        print(str)
    }
    
}

testParamOptional(str: nil) // если убрать ?, т.е. testNil(str:String), то будет ошибка компиляции (нельзя передавать nil в параметр String)


// optional в возвращ. типе
func testReturnOptional(str:String) -> Int?{
    return str.characters.count;
}

print(testReturnOptional(str: "asdasd")!)


// optional в параметре и возвращ. типе
func testAllOptional(str: String?) -> Int?{
    return str?.characters.count;
}

print(testAllOptional(str: "testq111")!)


// implicit unwrapping
func testImplicitOptParam(str: String!) -> Int{
    return str.characters.count;
}



var a:String? = "asda";

//testReturnOptional(str: a)
testImplicitOptParam(str: a)
//testImplicitOptParam(str: nil)
*/


// MARK: __NestedFunc

/*
// Функции можно объявлять внутри другой функции (Nested functions) – в Java так нельзя (можно использовать внутренние классы с методами)

// можно использовать для организации кода: разбить одну большую функцию на несколько (при условии, что внутренние функции больше нигде не нужны)


func returnFuncType(oper: Bool) -> (Int, Int) -> Int {
    
    var result: String
    
    // внутренние функции имеют доступ к переменным внешней функции
    func plus(p1: Int, p2: Int) -> Int {
        return p1 + p2
    }
    
    func minus(p1: Int, p2: Int) -> Int {
        return p1 - p2
    }
    
    if oper{
        return plus
    }else{
        return minus
    }
}


returnFuncType(oper: true)(5,2)
*/


// MARK: __Overload


/*
/* overloading - перегрузка функции (метода) - название совпадает, но параметры и тип возвращаемого значения могут отличаться
 
 В Java перегрузка метода отличается - имя и тип совпадают, но разные параметры
 
 */

func plus(p1: Int, p2: Int) -> Int {
    return p1 + p2
}

func plus(p1: Double, p2: Double) -> Double {
    return p1 + p2
}

func plus(){
    print("void plus")
}

// через Ctrl + Пробел будут показаны все перегруженные методы


plus(p1: 20.1, p2: 11.2)

plus(p1: 2, p2: 5)


// функция print - много перегрузок
*/


// MARK: __TypeAlias



/*
// ----- использование typealias ------

typealias MethodType = (String, Bool,  Int) -> (Int, String, Bool, String)

func newMethod(_ str:String, _ b:Bool,  _ i:Int) -> (Int, String, Bool, String){
    print("\(str), \(b), \(i)")
    return (1,"test", true, "test2"); // для примера, роли здесь не играет
}


// если переменных много - упрощается чтение кода при указании типа
var printVar1: MethodType = newMethod
var printVar2: MethodType = newMethod
var printVar3: MethodType = newMethod

// вызов функции через переменную, которая на нее ссылается
printVar2("test", true, 1)
printVar2("test", false, 2)
printVar2("test", true, 3)




// пример с функцией как параметр другой функции
func sum(_ i1: Int, _ i2: Int) -> Int {
    return i1 + i2
}

typealias SumType = (Int, Int) -> Int

var sumMethod:SumType  = sum

func secondFunc(_ sumMethod: SumType, _ a: Int, _ b: Int) {
    print("\(sumMethod(a, b))")
}

secondFunc(sum, 10, 20)


// не путайтесь
var methodVar: ((Int) -> Int) -> ((Int) -> Int) // функция с параметром Int и возвращаемым типом Int - в качестве параметра и возвращаемого типа
*/


// MARK: - Closure

// MARK: __Base


/*
// замыкание - блок кода, на который можно ссылаться и использовать в любом месте
// как и функции, можно передавать в параметры, присваивать в переменную, возвращать как тип и пр.


// разные способы объявления
let testClosure1:() -> () = {
    print("test") // при объявлении - замыкание не выполняется
}

let testClosure2:() -> Void = {
    print("test") // при объявлении - замыкание не выполняется
}

let testClosure3 = {
    print("test") // при объявлении - замыкание не выполняется
}

testClosure1()
testClosure2()
testClosure3() // замыкание выполняется только в момент примения


// замыкание с параметром
var printNumber: (Int) -> () = { (number) in
    print(number)
}

// нет меток параметров как у функций
printNumber(5)



// замыкание с параметрами и возвращаемым значением
var printTextAndNumber: (Int, String) -> (String) = { (number, text) in
    return "\(text): \(number)"
}

printTextAndNumber(5, "test text ")


// замыкание для подсчета суммы
var sum1: (Int, Int) -> Int = { (number1, number2) in
    return number1 + number2
}

// параметры без скобок
var sum2: (Int, Int) -> Int = { number1, number2 in
    return number1 + number2
}


// сокращенные записи (если тело замыкания простое, например арифметическая операция, т.е. сразу выполняется return)

// без указания параметров и in
var sum3: (Int, Int) -> Int = { return $0 + $1 }

// без return
var sum4: (Int, Int) -> Int = { $0 + $1 }

sum1(17, 2)
sum2(17, 2)
sum3(17, 2)
sum4(17, 2)

// оишбка - нельзя изменить, т.к. константа
//var testConst: (String) -> Void = { $0.append(" new text")}

// если мы хотим работать с переменной, а не константой
var testConst: (String) -> String = {
    var str = $0
    str.append(" - new text")
    return str
}

testConst("base text")
*/


// MARK: __Base2

/*
// пример из документациии

var customersInLine = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]
print(customersInLine.count)


let customerProvider = { customersInLine.remove(at: 0) }

print(customersInLine.count)

print("Now serving \(customerProvider())!")

print(customersInLine.count)


// только когда вызвали замыкание - произошло удаление элемента
*/

// MARK: __ClosureParam

/*
// использование параметров функции для их передачи в замыкание

typealias TwoDigitsFunction = (Int, Int) -> Int

func testCalc(_ number1:Int, _ number2:Int, _ calcFunc:TwoDigitsFunction) -> Int // сюда можно передавать как функцию, так и замыкание - главное, чтобы совпадали параметры и типы
{
    return calcFunc(number1,number2) * 100
}

// inline реализация замыкания - не нужно создавать отдельную переменную для присвоения замыкания
var sum = testCalc(4,2, { $0 + $1 })
var minus = testCalc(2,5, { $0 - $1 })
var multiply = testCalc(4,8, { $0 * $1 })
var divide = testCalc(11,3, { $0 / $1 })

sum
minus
multiply
divide



//testCalc(4,2, { $0 + $1 })
//testCalc(2,5, { $0 - $1 })
//testCalc(4,8, { $0 * $1 })
//testCalc(11,3, { $0 / $1 })


// если не использовать замыкания, то нужно реализовывать отдельные функции для данных матем. операций (появится избыточность кода)
*/


// MARK: __TrailingClosure

/*
// специальнай синтаксис (более удобный для чтения) для передачи замыканий (при условии что замыкание - это последний параметр функции


typealias TwoDigitsFunction = (Int, Int) -> Int

func testCalc(_ i1:Int, _ i2:Int, _ calcFunc:TwoDigitsFunction) -> Int {
    return calcFunc(i1,i2) * 100
}

// trailing closure - реализация после скобок с параметрами - более удобно для чтения
var sum = testCalc(4,2) { $0 + $1 }
var minus = testCalc(2,5) { $0 - $1 }
var multiply = testCalc(4,8) { $0 * $1 }
var divide = testCalc(11,3) { $0 / $1 }


sum
minus
multiply
divide


// если функция принимает только замыкание, круглые скобки можно опустить

func printStr(printClosure: () -> Void ) {
    printClosure()
}

printStr{ print("some") } // нет круглых скобок для printStr
*/


// MARK: __Autoclosure

/*
/* @autoclosure
 
 Упрощает синтаксис – не нужно указывать фигурные скобки
 Автоматически конвертирует выражение в замыкание
 
 */


var str = "text"

func checkStr(textCheckClosure: () -> Bool) {
    if textCheckClosure() {
        print("ok")
    }
}


// разные виды вызова замыкания
checkStr(textCheckClosure: { return str.contains("t")})
checkStr(textCheckClosure: { str.contains("t")}) // без return
checkStr() {str.contains("t")} // trailing closure
checkStr {str.contains("t")} // trailing closure

// @autoclosure - атрибут (аналог аннотации в Java) - доп. информация для компилятора

func checkStr2(textCheckClosure: @autoclosure () -> Bool) {
    if textCheckClosure() {
        print("ok")
    }
}

checkStr2(textCheckClosure: str.contains("t")) // нет фигурных скобок, как будто передаем параметры функции
// str.contains("t") автоматически был сконвертирован в замыкание
*/


// MARK: __CaptureVariables


/*
// замыкание «захватывает переменные» из контекста - capture variables


// замыкание имеет доступ к переменным контекста, где вызывается
// контекстом может быть любой блок, класс, функция и пр. - ограниченная область, где запускается замыкание
var a = 10
var b = 15

let closure1 = { print(a, b) } // не принимает параметров и не возвращает значение

closure1() // печатает значения переменных (может к ним обращаться - "захватывает")

a = 12
b = 41

closure1() // после повторного вызова - замыкание уже показывает измененные значения переменных
// замыкание создает ссылки (reference type) на переменные a и b


// функция также имеет доступ к переменным контекста, как и замыкание (функция - это по сути то же самое,   именованное замыкание)
func testCapture(){
    print("captured from func:")
    print(a,b)
    print()
}

testCapture()
*/


/*
// структуру будем проходить дальше, здесь - для демонстрации
// в данном случае контекст замыкания sum - это границы структуры Calc

struct Calc {
    var a: Int
    var b: Int
    
    // замыкание захватывает переменные a и b и может с ними работать
    var sum: Int {
        return a + b
    }
}


let calc = Calc(a: 2, b: 1) // создаем объект Calc
print(calc.sum) // вызываем замыкание
*/


// пример из документации

/*
func makeIncrementer(forIncrement amount: Int) -> () -> Int {
    var runningTotal = 0
    
    // функия "держит" ссылку на runningTotal
    func incrementer() -> Int { // эта функция возвращается как результат работы функции makeIncrementer
        runningTotal += amount
        return runningTotal
    }
    return incrementer
}

let incrementByTen = makeIncrementer(forIncrement: 10)

// при каждом вызове runningTotal увеличивается на 10
incrementByTen()
incrementByTen()
incrementByTen()
*/

// MARK: __CaptureList


/*
var a = 10
var b = 15

// чтобы замыкание создавало не ссылки на переменные a и b, а копии (value type) - указываем их
let closure1 = { [a, b] in print(a, b) } // [a, b] - это и есть captured list


closure1()

a = 12
b = 41

closure1() // после повторного вызова - выводит те же значения, что и в первый раз




// при создании нового замыкания - он возьмет уже измененные значения
let closure2 = { [varA = a, varB = b] in print(varA, varB) } // можно задавать другие имена для capture list

closure2()
closure2
*/


// MARK: - Guard

// MARK: __Base

/*
// Используется для раннего выхода из блока кода
// Часто используется для проверки значений, валидации
// Решает проблему "Pyramid of Doom"



// guard чаще всего используется внутри функции, чтобы проверить значения параметров

func testGuard(_ firstName:String, _ lastName:String, _ address: String, _ age:Int){
    
    // все условия должны быть выполнены
    if firstName != "" {
        if lastName != "" {
            if address != "" {
                if age > 0 {
                    print("проверка прошла успешно")
                }else{
                    print("age < 0")
                    return
                }
            }else{
                print("address is empty")
                return
            }
        }else{
            print("lastname is empty")
            return
        }
    }else{
        print("firstname is empty")
        return
    }
    
    
    
    // применение guard (улучшается чтение кода)
    
    guard firstName != "" else {
        print("firstname is empty")
        return  // обязательно должен быть return, break, continue или throw
    }
    
    guard lastName != "" else {
        print("lastname is empty")
        return
    }
    
    guard address != "" else {
        print("address is empty")
        return
    }
    
    guard age > 0 else {
        print("age < 0")
        return
    }
    
    
    
    // сюда попадаем, если выполнились все условия
    print("проверка прошла успешно")
    
    
    // можно проверять все значения одновременно
    guard firstName != "", lastName != "", address != "", age > 0 else {
        print("одно из условий не выполнилось") // если не важно какое именно
        
        return
    }
    
}


testGuard("firstName", "lastName", "", 35)
*/



// MARK: __NoUse

/*
// если не нужен ранний выход -  guard использовать не получится


func testIf(_ firstName:String, _ lastName:String, _ address: String, _ age:Int){
    
    
    // здесь проверяются последовательно все условия
    
    if firstName != "" {
        
    }else{
        print("firstname is empty")
    }
    
    if lastName != "" {
        
    }else{
        print("lastname is empty")
    }
    
    if address != "" {
        
    }else{
        print("address is empty")
    }
    
    if age > 0 {
        
    }else{
        print("age < 0")
    }
    
}

func testGuard(_ firstName:String, _ lastName:String, _ address: String, _ age:Int){
    
    
    // логика работы кода меняется, т.к. при первой же неудачной проверке произойдет выход из функции
    guard firstName != "" else {
        print("firstname is empty")
        return
    }
    
    guard lastName != "" else {
        print("lastname is empty")
        return
    }
    
    guard address != "" else {
        print("address is empty")
        return
    }
    
    guard age > 0 else {
        print("age < 0")
        return
    }
    
    
    print("проверка прошла успешно")
    
}


testIf("", "lastName", "", 35)
print("")
testGuard("", "lastName", "", 35)
*/


// MARK: __Optional

/*
// optional binding + guard


func guardOptional1(count:Int?){
    
    // optional binding (проверка на nil)
    guard let count = count else {
        return
    }
    
    print("1-е условие выполнилось, count=\(count)")
    
}


func guardOptional2(count:Int?){
    
    
    
    // одновременно optional binding + условие
    guard let count = count, count > 5 else {
        return
    }
    
    print("2-е условие выполнилось, count=\(count)")
    
    
}


func guardOptional3(count:Int?, name:String?){
    
    // несколько optional binding + условие
    guard let count = count, let name = name, count > 10 else {
        return
    }
    
    
    
    print("3-е условие выполнилось, count=\(count), name=\(name)")
    
}

func guardOptional4(count:Int?){
    
    
    guard var count = count else { // если нужно дальше изменять значение переменной - используем guard var
        return
    }
    
    print("4-е условие выполнилось, count=\(count)")
    
    
    count += 10
}


guardOptional1(count: 4)
guardOptional2(count: 6)
guardOptional3(count: 19, name: "Piter")
guardOptional4(count: 2)

*/


// MARK: - Arrays

// MARK: __Base


/*
let numbers:[Int] = [3, 6, 1, 10] // массив типа Int
//names.append(5) // нельзя добавить новое значение, т.к. массив immutable (не изменяемый) из-за let

let numbers2:Array<Int> = [3, 6, 1, 10] // другой способ объявления (Generics будем проходить дальше)

let numbers3:Array<Int> = [3, 6, 1, 10,]  // если оставите запятую - ничего страшного - помогает в ряде случаев, когда работаем динамически с массивом (в Java нельзя оставлять)

//let emptyArray = [] // нельзя, нужно указать хотя бы тип, т.к. компилятор не может его определить

let intArray = [5,1] // по значениям массива компилятор автоматически определяет тип Int


// инициализация начальными значениями
var intArray2 = [Int](repeating: 4, count: 5)
intArray2.append(3) // можно добавлять новые элементы (длина массива не фиксированная, аналог List в Java)
intArray2 += [2, 1, 8] // другой способ добавления элементов



var newArray = [Int]()// инициализация пустого массива (создание объектов будет проходить в будущих уроках)

var newArray2:[Int]
//print(newArray2) // нельзя использовать, т.к. массив не проинициализирован

var emptyArray:[Int] = []// пустой массив

// чтобы добавлять любые типы - используем тип Any или AnyObject (будем проходить дальше), такой подход не рекомендуется в большинстве случаев
let mixedTypesArray:[Any] = [1, "test", 4]


var names = ["Ivan", "Oleg", "Tim", "David", "Anna"]
names.count // количество элементов
names.isEmpty // частый оператор - есть ли элементы в массиве

names[3] = "New David" // индексация с нуля
names.insert("Alex", at: 3)
names.remove(at: 2) // возвращает удаленный объект

names // если в правой части не помещается длинный массив - нажмите прямоугольник для раскрытия
names[1...4]
names[..<3]

//names[6] // ошибка, значение за пределами индекса

// готовые методы
names.min()
names.max()

names.contains("Anna")

intArray2.max()

// возвращает Optional, поэтому нужно проверить перез извлечением
print(names.first)

if let name = names.first{
    print(name)
}
*/

// MARK: __Range

/*
// использование диапазона для получения и обновления значений
var names = ["Ivan", "Oleg", "Tim", "David", "Anna"]

names[1...4]
names[..<3]

// обновление диапазона значений по индексам
names[1...3] = ["test1","test2","test3"]

names

// удаление диапазона значений по индексам
names.removeSubrange(0...2)
*/

// MARK: __ByValue


/*
var numbers = [3, 6, 1, 10]

var numbers2 = numbers // копируются значения из массива numbers в numbers2 (не ссылка на массив, как в Java)


numbers2
numbers

numbers2.remove(at: 2) // на другом массиве не сказывается удаление
//numbers.remove(at: 1)

numbers2
numbers
*/

// MARK: __Loop


/*
var names = ["Ivan", "Oleg", "Tim", "David", "Anna"]

for name in names { // цикл по элементам массива
    print(name)
}

print("-")

for (index, value) in names.enumerated() { // кортеж из индексов и значений
    print(index, value)
}


print("-")

for name in names.reversed() { // в обратном порядке (при объявлении)
    print(name)
}
*/


// MARK: __Variadic


/*
// variadic parameters

func printAll(values: String...) { // параметр превращается в массив
    
    type(of: values)
    
    for text in values {
        print(text)
    }
    
}


printAll(values: "one", "2", "go", "text")


// variadic может быть только для 1 параметра на функцию (в Swift - в любом месте, в Java можно только как последний параметр)
*/


// MARK: __Closure

/*

// использование замыкания для сортировки

var names = ["Ivan", "Oleg", "Tim", "David", "Anna"]

// sorted возвращает копию массива (оригинал не затрагивает)
let sortedNames = names.sorted(by: { (a, b) -> Bool in
    return a < b
})

let sortedNames2 = names.sorted(by: { $0 < $1 })

let sortedNames3 = names.sorted(by: < )

names
sortedNames
sortedNames2
sortedNames3

names.sorted { (a, b) -> Bool in
    return a < b
}


names.sorted {
    return $0 < $1
}


names.sorted {$0 < $1}


names.sorted(by: <)



// метод sort - сортирует исходный массив

names.sort()
names
*/


// MARK: __Dimensions


/*
// можно создавать массивы любой вложенности (мерности)

var twoDimArray = [ [2, 5], [1, 7], [15, 90], [15, 39] ]

//var twoDimArray: [[Int]] = [ [2, 5], [1, 7], [15, 90], [15, 39] ]



// обращение к элементам
twoDimArray[0][0]
twoDimArray[3][1]

// изменение

twoDimArray[3][1] = 0 //  изменяем одно значение
twoDimArray[1] = [3, 6] // можно изменять оба значения

twoDimArray


for n1 in 0 ..< twoDimArray.count { // < чтобы не выйти за границы массива, т.к. индексация с нуля
    for n2 in 0 ..< twoDimArray[n1].count {
        print(twoDimArray[n1][n2])
    }
}


var fiveDimArray:[[[[[Int]]]]] // сколько скобок, столько мерностей
*/

// MARK: __Optionals

/*

// элементы опциональные
let numbers: [Int?] = [21, 51, nil, 12]
print(numbers)

for number in numbers {
    print(number)
    
    //    if let number = number{
    //        print(number)
    //    }
    
}


// сам массив опционален
var numbers2: [Int]? = [21, 51, 12]
print(numbers2)

numbers2?.append(4) // XCode сам дописывает знак вопроса при попытке вызова append, т.к. это optional тип

numbers2?.count

numbers2
*/


// MARK: __Split

/*
var text = "Swift is friendly to new programmers. It’s an industrial-quality programming language that’s as expressive and enjoyable as a scripting language"

var t1 = text.split(separator: " ")//  возвращает разделенный текст в виде массива слов
t1

// можно использовать вариант с замыканием
var t2 = text.split{$0 == " "}
t2
*/


// MARK: - Set

// MARK: __Base

/*
// set - математическое множество


var intSet: Set = [124, 5, 62, 12] // коллекция целых значений (если убрать ": Set", то получится Array)


// создание объектов будем проходить дальше
var set2: Set<Int> = [1, 5, 62, 12]

// создание пустого множества
var set3 = Set<Int>()

// можно инициализировать из массива
let set4 = Set([1, 5, 62, 12])

// множество текстовых значений
let strSet: Set = ["Ivan", "Oleg", "Tim", "David", "Anna"]

// порядок вывода не гарантируется
for str in strSet {
    print("\(str)")
}

print()

// вывод в отсортированном виде
for str in strSet.sorted() {
    print("\(str)")
}


intSet.insert(7) // не указываем индекс, т.к. его нет в Set
intSet.remove(5)
intSet.count

intSet.contains(12)

print(intSet)



// математические операции над множествами
var intSet2: Set = [521, 15, 12, 42]

intSet.intersection(intSet2) // найти пересечение элементов
*/

// MARK: __Array


/*
var strSet: Set = ["Ivan", "Oleg", "Tim", "David", "Anna"]

// конвертируем Set в Array, теперь у всех элементов есть индекс
var arrayNames = Array(strSet)

type(of: arrayNames)

for (index, name) in arrayNames.enumerated() {
    print(index, name)
}

arrayNames.remove(at: 1)

arrayNames


// конвертация из массва в Set (индексы снова недоступны)
var strSet2 = Set(arrayNames)

type(of: strSet2)
*/


// MARK: - Dictionary

// MARK: __Base

/*
// Dictionary - словарь - "ключ-значение"

// в этом варианте - похож на массив, т.к. ключом является Int
let strDict: [Int:String] = [1: "Ivan", 2: "Oleg", 3: "Tim", 4: "David", 5: "Anna"]

// пустой словарь
let emptyDict: [Int:String] = [:]

// тип можно не указывать, если есть значения при объявл
let docNumbers = ["Ivan": 1234123, "Oleg": 65310, "Tim": 89159]

for (key, value) in docNumbers {
    print(key, value)
}


// обновление
var person = ["name": "TestName", "address": "test street"]

person["name"] = "Ivan" // заменяет значение по ключу (также добавляет ключ-значение, если нет такого ключа)

var oldValue = person.updateValue("new address", forKey: "address") // возвращает замененный объект (значение)

person

// получение
person["asdasd"] // нет такого ключа, поэтому возвращается nil

person["name"]

person


// удаление
person.removeValue(forKey: "address")


person
*/


// MARK: __Array

/*
// конвертация в массив

let docNumbers = ["Ivan": 1234123, "Oleg": 65310, "Tim": 89159]


var arrayKeys = Array(docNumbers.keys)
//var arrayKeys = [String](docNumbers.keys) // другая запись
type(of: arrayKeys) // тип сохраняется (какой был у ключа)

//var arrayValues = Array(docNumbers.values)
var arrayValues = [Int](docNumbers.values) // другая запись
type(of: arrayValues)
*/


// MARK: - CollectionsMethods

// MARK: __Map

/*
// map - проводит операцию с каждым элементом коллекции

// для массива (можно также использовать Set или Dictionary)
let sourceNumbers = [1, 29, 17, 128]

let numbersResult1 = sourceNumbers.map {$0 + 1} // trailing closure
//let numbersResult1 = sourceNumbers.map {(n1:Int) -> Int in return n1 + 1} // полная запись
numbersResult1


// реализация без метода map (занимает намного больше кода)
var numbersResult2: [Int] = []
for value in sourceNumbers {
    numbersResult2.append(value + 1) // копируем в новый массив новые значения
}
numbersResult2



// для словарей есть специальный метод mapValues, который сразу работает со значениями без получения ключей

let person = ["name": "Ivan", "address": "test street"]

let newPerson = person.mapValues { $0 + " v.2"}

newPerson
*/

// MARK: __Filter

/*
// filter - возвращает отфильтрованную коллекцию

let sourceNumbers = [1, 29, 17, 128]

var filteredArray = sourceNumbers.filter{String($0).contains("1")} // только те цифры, которые имеют цифру 1
sourceNumbers // исходный массив остался без изменений
filteredArray

var filteredArray2 = sourceNumbers.filter{$0 > 10} // только те цифры, которые больше 10

filteredArray2
*/


// MARK: __Reduce


/*
// reduce - объединение всех элементов коллекции в одно значение
//var arraySum = sourceNumbers.reduce(0){} //trailing closure

let numbers = [1, 2, 3, 4]
let numberSum = numbers.reduce(0) {$0 + $1} // $0 - текущий результат, $1 - следующий элемент

numberSum



// методы map, reduce, filter пришли из функционального программирования, в Java похожие методы доступны для лямбда выражений
*/

// MARK: __ForEach


/*
// foreach - выполнить какой-либо оператор, например вызвать метод у каждого элемента коллекции

var str = ["1", "2", "", "4", ""]

// часто используют для вызова какого-либо метода у коллекции объектов
str.forEach(){
    print($0.isEmpty)
}
*/

// MARK: __Chain

/*

// можно вызывать методы друг за другом

let sourceNumbers = [12, 43, 1, 29, 17, 128, 129, 430]


// умножить все цифры на 2 и оставить только больше 200

var result = sourceNumbers.map {$0 * 2}.filter {$0 > 100}
result
*/


// MARK: __Convert

/*
//  конвертация типов

var numbers: [Int] = [15, 98, 32, 4, 50]

type(of: numbers)

var str = numbers.map
{
    String($0)
}

type(of: str)

print(str)
*/

// MARK: __Lazy


/*
let numbers = [1, 29, 17, 128]

let result = numbers.lazy.map {$0 + 1} // выполнится только при первом обращении к элементам (для оптимизации)


for number in result {
    print(number)
}
*/

// MARK: - Struct

// MARK: __Base


/*

//  ранее мы уже работали со структурами (это почти все системные типы в Swift)


// создаете экземпляр нужной структуры (класса, перечисления) - и работаете с ним

var str: String = "test" // экземпляр структуры String

var setNumbers: Set = [1, 4, 6] // экземпляр структуры Set

var dict:Dictionary = ["name":"Piter", "address":"test street"] // экземпляр структуры Dictionary

var array = [Int]() // экземпляр структуры Array тип Int


// структуры -  value type

var str2 = str
str2 = "new test"
str // осталось прежнее значение
*/

// MARK: __CreateStruct


/*
struct Team{
    
    // свойства структуры
    
    var name:String // название команды
    
    var players:[String] // набор игроков
    
    func printPlayers(){ // метод структуры
        for player in players {
            print(player)
        }
    }
    
}

// создание экземпляра структуры (не нужно ключевое слово "new" как было в Java )
let team1 = Team(name: "Arsenal", players: ["John", "Adam"]) // используется инициализатор (конструктор), подробнее - в след. уроке

var team2 = Team(name: "Inter", players: ["Tim", "Ivan"])

var team3 = Team(name: "Real madrid", players: ["Jerry", "Tom"])

//team1.name = "other name" // нельзя, т.к. team1 присвоили в константу, даже если var name (характерно только для структур)

team2.name = "Milan" // можно, т.к. var team2

team2.printPlayers()


// value type

var team4 = team3
team4.name = "new team"
team3.name
*/

// MARK: __StoredProprties

/*
/* stored properties - хранят значения
 
 все необязательные свойства можно указать как Optional (могут принимать nil)
 
 */

struct Team{
    
    var name:String = "noname" // можно задавать значения по-умолчанию
    
    // 11 - антипаттерн "magic number" - вместо числа лучше использовать константу
    var players = [String](repeating: "", count: 11) // массив из 11 значений ""
    
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
    
}
*/

// MARK: __ComputedProperties
/*
// computed properties - вычисляемые (вычисленные) значения
// НЕ может содержать значение по-умолчанию

struct Team{
    
    var name:String = "noname"
    
    let maxPlayersCount = 11 // чтобы избежать "magin number" - создадим переменную (максимальное число игроков в команде)
    
    //    var players = [String](repeating: "", count: maxPlayersCount) // нельзя использовать переменную maxPlayersCount
    
    var players = [String](repeating: "", count: 11)
    
    // Read-Only Computed Property - вычисляет значение (можно на основе других переменных)
    var count:Int { // нет параметров, т.к. это не метод (функция)
        var count = 0
        
        for player in players {
            if (player != ""){
                count += 1
            }
        }
        return count //  количество не пустых значений
    }
    
    var desc: String
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
}

var team1 = Team(name: "Real madrid", players: ["Jerry", "Tom", ""], desc: "доп. описание")

team1.count
team1.desc

team1.players.append("Bob")

team1.count

*/





// MARK: __Static

/*
/* static - элемент не зависит от других переменных и одинаков для всех экземпляров
 
 в Swift называют "type property"
 
 */

struct Team{
    
    static let maxPlayersCount = 11 // константа - максимальное число игроков в команде, одинаково для всех экземпляров структуры
    
    var name:String = "noname"
    
    var players = [String](repeating: "", count: maxPlayersCount) // теперь можно использовать переменную playersCount
    
    var count:Int {
        var count = 0
        
        for player in players {
            if (player != ""){
                count += 1
            }
        }
        return count
    }
    
    var desc: String
    
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
}


var team1 = Team(name: "Inter", players: ["Adam", "Smith", "Tom"], desc: "доп. описание")
team1.count

//team1.maxPlayersCount // нельзя так вызывать static переменную (в Java можно было)

Team.maxPlayersCount // вызов через название структуры

team1.desc
*/






// MARK: __GetSet

/*  Геттеры – сеттеры – это computed property, поэтому только вычисляют значения, а не хранят как stored property (в отличие от Java, где get-set это просто доступ к переменным объекта)
 
 свойства с get-set не могут присваивать значение по-умолчанию при объявлении
 
 можно использовать генерацию "vargetset" для быстрого создания кода
 
 get/set можно использовать и вне структуры для любой переменной
 
 */

/*
struct Team{
    
    var name:String = "noname"
    
    static let maxPlayersCount = 11
    
    var players = [String](repeating: "", count: maxPlayersCount)
    
    var count:Int {
        
        //  по сути это просто getter (без setter)
        
        var count = 0
        
        for player in players {
            if (player != ""){
                count += 1
            }
        }
        return count
        
    }
    
    
    
    var salary = 20_000 // минимальная зп для каждого игрока
    
    // переменная, которая вычисляет свое значение (а не хранит как stored property)
    var budget:Int{
        
        // get должен быть обязательно
        get {
            return salary * count
        }
        
        // set может отсутствовать (тогда свойство будет read-only)
        // если нет set - слово get можно не добавлять
        set{ // готовая переменная newValue
            self.salary = Int(newValue / count) // зп рассчитывается как (общий бюджет/кол-во игроков)
            
            // не вызывайте свой переменную - программа зациклится
            //            self.budget = newValue
        }
    }
    
    
    // если set не нужен - свойство budget+get выглядело бы более сокращенно
    
    //    var budget:Int{
    //            return salary * count
    //    }
    
    
    var desc: String = "desc"
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
}

var team1 = Team()

team1.players[0] = "player1"
team1.players[1] = "player2"
team1.budget = 100_000

team1.salary // возвращает зп каждого игрока в зависимости от бюджета

team1.budget // возвращает бюджет исходя из зп и кол-ва игроков
*/







// MARK: __Lazy

/*
// вычисления, которые нужно производить только по требованию (если создается новый объект, долго выполняется операция и пр.)
// работает только с var и только с stored property



struct Team{
    
    var name:String = "noname"
    
    static let maxPlayersCount = 11
    
    var players = [String](repeating: "", count: maxPlayersCount)
    
    var count:Int {
        
        var count = 0
        
        for player in players {
            if (player != ""){
                count += 1
            }
        }
        return count
        
    }
    
    var salary = 20_000 // минимальная зп для каждого игрока
    
    // переменная, которая вычисляет свое значение (а не хранит как stored property)
    var budget:Int{
        get {
            return salary * count
        }
        
        set{ // готовая переменная newValue
            self.salary = Int(newValue / count) // зп рассчитывается как (общий бюджет/кол-во игроков)
        }
    }
    
    
    var desc: String = "desc"
    
    lazy var longOperationVar = 10 // обычно здесь выполняется создание другого объекта с долгой операцией
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
}

var team1 = Team()
team1 // здесь еще не заполнена переменная longOperationVar

team1.longOperationVar // при первом обращении - выполняется присвоение значения в переменную

team1 // здесь уже заполнена переменная longOperationVar, т.к. ранее к ней уже обратились
*/


// MARK: __Observer
 /*
 
 observer - наблюдатель - срабатывает при изменении значения переменной
 
 отличия от get-set
 - применяется для stored property
 - не вычисляет значение, а проверяет все ли верно
 - может содержать значение по-умолчанию (computed property не может)
 - можно использовать и вне структуры для любой переменной
 
 
 */


/*
struct Team{
    
    var name:String = "noname"
    
    static let maxPlayersCount = 11
    
    var players = [String](repeating: "", count: maxPlayersCount)
    
    var count:Int {
        
        var count = 0
        
        for player in players {
            if (player != ""){
                count += 1
            }
        }
        return count
        
    }
    
    // наблюдатели отрабатывают только тогда, когда меняем значение уже после создания объекта (не во время инициализации)
    var salary = 20_000 { // задаем значение по-умолчанию
        
        // можно оставлять только один set
        
        willSet { // отработает перед изменением значения
            print("newValue = \(newValue)")
        }
        
        didSet{ // отработает после изменения значения
            print("oldValue = \(oldValue)")
        }
        
        //        willSet (newSalary) { // можно задавать свое имя переменной
        //            print("newValue = \(newSalary)")
        //        }
        
        //        didSet (oldSalary){ // отработает после изменения значения
        //            print("oldValue = \(oldSalary)")
        //        }
        
    }
    
    // переменная, которая вычисляет свое значение (а не хранит как stored property)
    var budget:Int{
        get {
            return salary * count
        }
        
        set{ // готовая переменная newValue
            self.salary = Int(newValue / count) // зп рассчитывается как (общий бюджет/кол-во игроков)
        }
    }
    
    
    var desc: String = "desc"
    
    lazy var longOperationVar = 10 // обычно здесь выполняется создание другого объекта с долгой операцией
    
    func printPlayers(){
        for player in players {
            print(player)
        }
    }
    
}

var team1 = Team()
team1.salary = 30_000
*/



// MARK: __GetSetObserver


/*
// get/set и observers можно использовать и вне структуры для любой переменной

var number = 11{
    willSet {
        print("\(newValue), \(number)")
    }
    didSet{
        print("\(oldValue), \(number)")
    }
}

number = 20
*/


// MARK: - Init

// MARK: __Base
/*
// инициализаторы, которые создаются компилятором

struct Team{
    
    static let playersCount = 11 // не исп. в init
    
    // stored properties - их может заполнять init
    var name:String = "noname"
    
    var players = [String?](repeating: nil, count: playersCount)
    
    var desc: String = "desc"
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    // computed properties
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
}

/*
 По-умолчанию компилятор создает 2 инициализатора:
 - пустой - если все свойства имеют значение по-умолчанию (или свойств нет)
 - со всем свойствами, даже заполненными
 */


// default initializer
var team1 = Team() // пустой init доступен только при условии, что все свойства имеют значения по-умолчанию
team1.count


// memberwise initializer
var team2 = Team(name: "Real madrid", players: ["Bob", "Tom"], desc: "доп. описание", salary: 100)
team2.count
team2.budget = 200_000
team2.salary


// вспомните, как мы создавали пустой массив
var emptyArray = [Int]()
*/


// MARK: __NoDefault

/*
 если хотя бы 1 свойство не заполнено - init без параметров недоступен
 
 */

/*
struct Team{
    
    static let playersCount = 11
    
    // пустое свойство (не инициализировано), не путать с nil
    var name:String // важное отличие от Java - по-умолчанию не присваивается nil
    
    var players = [String?](repeating: nil, count: playersCount)
    
    var desc: String = "desc"
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
}



var team = Team() // недоступен, компилятор сгенерит только 1 конструктор - со всеми параметрами

*/



// MARK: __OptionalProperties
 // для Optional свойства, объявленных как var - присваивается значение по-умолчанию nil (для let нет)

/*
struct Team{
    
    static let playersCount = 11
    
    // задается по-умолчанию значение nil
    var name:String? //  извлекать значение нужно будет вручную
    
    var players = [String?](repeating: nil, count: playersCount)
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String! // значение автоматически распакуется (извлечется)
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
}



var team = Team() // доступен, т.к. optional значения по-умолчанию стали равны nil - в итоге все свойства получают значения по-умолчанию

team.name
team.desc

type(of: team.desc)
type(of: team.name)

// также доступен полный init (со всеми параметрами)
var team2 = Team(name: "Real madrid", players: ["Bob", "Tom"], desc: "доп. описание", salary: 100)

team2.desc
team2.name

team2.name?.append(" ()")
team2.name
*/





// MARK: __OwnInit

/*
/* собственные init (обычно следуют в начале структуры/класса после всех свойств)
 
 при создании своих init - автоматические уже не создаются (их приходится создавать вручную)
 
 */


struct Team{
    
    static let maxPlayersCount = 11
    
    var players = [String?](repeating: nil, count: maxPlayersCount)
    
    var desc: String
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    var name:String // убрал optional для демонстрации
    
    
    // компилятор не даст создать такие init, т.к. "видит", что не все обязательные свойства заполняются
    
    //    init(){
    //
    //    }
    //
    //    init(name: String) {
    //        self.name = name
    //    }
    
    
    init(){
        print("init()")
        self.init(name: "def name", desc: "def desc") // вызов одного init из другого
        
        //      self.init(name: "def name") // два раза подряд self.init вызывать нельзя
        
    }
    
    
    init(name:String){
        print("init(String)")
        self.init(name: name, desc: "def desc")
    }
    
    // этот init - дает возможность создавать объект, т.к. все пустые свойства заполняются
    init(name: String, desc: String) {
        print("init(String,String)")
        self.name = name // self - аналог this (ссылка на текущий экземпляр)
        self.desc = desc
    }
    
    
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    
    
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    
}


var team = Team(name: "test team")

var team1 = Team(name: "test team", desc: "test desc")
team1.desc
team1.name

var team2 = Team()
team2.desc
team2.name

*/




// MARK: __Label
/*
 
 можно скрывать метки для init (как делали для функции)
 
 */


/*
struct Team{
    
    var name:String
    
    static let maxPlayersCount = 11
    var players = [String?](repeating: nil, count: maxPlayersCount)
    
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String
    
    init(){
        self.init("def value", "def desc")
    }
    
    init(_ name: String, _ desc: String) {
        self.name = name
        self.desc = desc
    }
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    
}


var team = Team("test team", "test desc") // не нужно названия параметров
*/




// MARK: __Const

 /*
 
 Константы – можно заполнять через init (после этого значение поменять нельзя) – многие новички думают, что константу нужно заполнять сразу при объявлении
 
 */


/*
struct Team{
    
    let name:String // представим, что название команды не меняется
    
    static let maxPlayersCount = 11
    var players = [String?](repeating: nil, count: maxPlayersCount)
    
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String
    
    init(){
        self.init("def value", "def desc")
    }
    
    init(_ name: String, _ desc: String) {
        self.name = name
        self.desc = desc
    }
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    
}



var team = Team("test team", "test desc") // константа name инициализируется один раз при создании объекта
//team.name = "team2" // нельзя изменить, т.к. константа
*/




// MARK: __Failable



/*
struct Team{
    
    var name:String = "noname"
    
    static let maxPlayersCount = 11
    var players = [String?](repeating: nil, count: maxPlayersCount)
    
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String = "desc"
    
    init(){}
    
    init?(name: String, desc: String) {
        
        if name.count<2{ // пример условия
            return nil
        }
        
        self.name = name
        self.desc = desc
    }
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    
}



//var team = Team(name: "t", desc: "test desc") // возвращает nil, т.к. имя < 2


var team = Team(name: "new team", desc: "test desc")

// optional chaining
team?.count

// optional binding
if var team = team{
    team.players.append("new player")
    team.count
}
*/


// MARK: - Methods

// MARK: __Base

/*
// методы - это функции внутри типа


struct Team{
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[String?]! // обычно объявление force unwrapping переменных означает, что они обязательно будут инициализироваться в init
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String = "desc"
    
    init(maxPlayersCount:Int){
        self.maxPlayersCount = maxPlayersCount
        players = [String?](repeating: nil, count: self.maxPlayersCount)
    }
    
    init(name: String, desc: String, maxPlayersCount:Int) {
        self.init(maxPlayersCount: maxPlayersCount)
        self.name = name
        self.desc = desc
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    // instance method
    func printPlayers(){
        
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    
}


var team = Team(maxPlayersCount: 20)
team.players[0] = "new player" // для оращения к коллекции - нам нужно обращаться к переменной players
team.count
*/







// MARK: __Mutating

/*
// если метод изменяет свойства объекта - нужно указывать mutating


struct Team{
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[String?]!
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String = "desc"
    
    init(maxPlayersCount:Int){
        self.maxPlayersCount = maxPlayersCount
        players = [String?]() // не создаются пустые значения
    }
    
    init(name: String, desc: String, maxPlayersCount:Int) {
        self.init(maxPlayersCount: maxPlayersCount)
        self.name = name
        self.desc = desc
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    // метод (не нужен mutating, т.к. метод не изменяет свойства)
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    // без mutating компилятор выдаст ошибку
    mutating func addPlayer(player:String){
        
        // если достигли максимальное число - больше не добавляем игроков
        if (players.count == maxPlayersCount){
            print("команда уже полная, не могу добавить \(player)")
            return
        }
        
        players.append(player)
        // self.players.append(player)// можно было так, но избыточно
    }
    
    //  можно присваивать новый объект
    mutating func release(){
        self = Team(maxPlayersCount: maxPlayersCount) // обнуляет команду
    }
    
    
}


var team1 = Team(maxPlayersCount: 5)
team1.addPlayer(player: "player1")
team1.addPlayer(player: "player2")
team1.addPlayer(player: "player3")
team1.addPlayer(player: "player4")
team1.addPlayer(player: "player5")
team1.addPlayer(player: "player6") // не даст добавить, т.к. достигли макс. количества игроков
team1.count

team1.printPlayers()

team1.release()

team1.count
*/



// MARK: __Static\

/*
// называется type method - по аналогии с type property - статичный элемент

struct Team{
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[String?]!
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String = "desc"
    
    init(maxPlayersCount:Int){
        self.maxPlayersCount = maxPlayersCount
        players = [String?]() // не создаются пустые значения
    }
    
    init(name: String, desc: String, maxPlayersCount:Int) {
        self.init(maxPlayersCount: maxPlayersCount)
        self.name = name
        self.desc = desc
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    // метод
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
    }
    
    // без mutating компилятор выдаст ошибку
    mutating func addPlayer(player:String){
        
        // если достигли максимальное число - больше не добавляем игроков
        if (players.count == maxPlayersCount){
            print("команда уже полная, не могу добавить \(player)")
            return
        }
        
        players.append(player)
        //self.players.append(player)// можно было так, но избыточно
    }
    
    //  можно присваивать новый объект
    mutating func release(){
        self = Team(maxPlayersCount: maxPlayersCount) // обнуляет команду
    }
    
    // статичный метод не зависит от свойств объекта (работает только с переданными параметрами)
    static func checkString(text:String) -> Bool {
        //        print(name) // свойства не могут использовать в статичном блоке
        return !text.contains("-") // проверяет, чтобы текст не содержал "-"
    }
    
    
}


var team1 = Team(maxPlayersCount: 5)
team1.addPlayer(player: "player1")
team1.addPlayer(player: "player2")
team1.addPlayer(player: "player3")
team1.addPlayer(player: "player4")
team1.addPlayer(player: "player5")
team1.addPlayer(player: "player6") // не даст добавить, т.к. достигли макс. количества игроков
team1.count

team1.printPlayers()

team1.release()

team1.count


Team.checkString(text: "player-")
Team.checkString(text: "player")
*/




// MARK: __Subscript

/* subscript - используется для получения элемента из коллекции (например, по индексу)
 
 можно реализовать получение элемента по любым параметрам для своего типа
 
 в нашем случае - Team содержит набор игроков, которых мы можем получать по индексу (или в любым другим параметрам)
 
 */

/*

struct Team{
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[String?]!
    
    var count:Int {
        return players.filter { !($0 ?? "").isEmpty}.count
    }
    
    var desc: String = "desc"
    
    init(maxPlayersCount:Int){
        self.maxPlayersCount = maxPlayersCount
        players = [String?]() // не создаются пустые значения
    }
    
    init(name: String, desc: String, maxPlayersCount:Int) {
        self.init(maxPlayersCount: maxPlayersCount)
        self.name = name
        self.desc = desc
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    // метод
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player)
            }
        }
        
        print() // для отступа от других печатей
    }
    
    
    // можно использовать любые параметры, в данном случае будем исп. index:Int
    subscript (index: Int) -> String?{
        
        // получение игрока по индексу
        get{
            return players[index]
        }
        
        // обновление игрока по индексу (если под этим индексом кто-то есть)
        set{
            if players[index] != nil{
                players[index] = newValue
            }
            
        }
    }
    
    mutating func addPlayer(player:String){
        
        if (players.count == maxPlayersCount){
            print("команда уже полная, не могу добавить \(player)")
            return
        }
        
        players.append(player)
    }
    
    
    mutating func release(){
        self = Team(maxPlayersCount: maxPlayersCount)
    }
    
    
    static func checkString(text:String) -> Bool {
        return !text.contains("-") // проверяет, чтобы текст не содержал "-"
    }
    
    
}

var team1 = Team(maxPlayersCount: 5)
team1.salary = 30_000
team1.addPlayer(player: "player1")
team1.addPlayer(player: "player2")
team1.addPlayer(player: "player3")

team1.printPlayers()


// можно как обновлять игроков через subscript
team1[0] = "Player1 new"
team1[2] = "Player3 new"

team1.printPlayers()

// так и получать
team1[1]




team1.printPlayers()
*/

// MARK: - Class

// MARK: __Base

// можно заменить struct на class - и получится класс

/*
class Team{
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[Player?]!
    
    var count:Int {
        return players.filter { $0 != nil }.count
    }
    
    var desc: String = "desc"
    
    // designated inits
    
    init(maxPlayersCount:Int){
        self.maxPlayersCount = maxPlayersCount
        players = [Player?]()
    }
    
    init(){
        //        self.init(maxPlayersCount:100) designated init не может вызывать через self
        self.maxPlayersCount = 100 // поэтому можем просто присвоить значение
        
    }
    
    
    convenience init(name: String, desc: String, maxPlayersCount:Int) {
        
        // обязательно должен быть вызван в первую очередь, только потом остальной код
        self.init(maxPlayersCount: maxPlayersCount) // обязательно должен вызывать какой-либо designated init или другой convenience init (в итоге все равно должен быть вызван хотя бы один designated init)
        self.name = name
        self.desc = desc
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            
        }
    }
    
    
    var budget:Int{
        get {
            return salary * count
        }
        
        set{
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    subscript (index: Int) -> Player?{
        
        get{
            if index > maxPlayersCount{
                print ("вышли за предел массива")
                return nil
            }else{
                return players[index]
            }
        }
        
        
        set{
            if players[index] != nil{
                players[index] = newValue
            }
            
        }
    }
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player.name, player.age)
            }
        }
    }
    
    // mutating не требуется, чтобы изменять свойства
    func addPlayer(player:Player){
        
        if count >= maxPlayersCount{
            print("команда уже полная, не могу добавить \(player.name)")
            return
        }
        
        if player.age < 20{
            print("нельзя добавлять \(player.name) < 20 лет")
            return
        }
        
        players.append(player)
    }
    
    
    // self - immutable, поэтому функция не будет работать
    //    func release(){
    //        self = Team(maxPlayersCount: maxPlayersCount)
    //    }
    
    
    static func checkString(text:String) -> Bool {
        return !text.contains("-")
    }
    
    
}

// Класс не создает автоматически memberwise initializer
// если параметры не принимают значения по-умолчанию - не создается default initializer

class Player{
    
    var name:String
    var age:UInt
    
    // в случае создания своих init - default init не создается
    init(name:String, age:UInt){
        self.name = name
        self.age = age
    }
    
    convenience init(name:String){
        //        age = 9 // нельзя, сначала self.init
        self.init(name: name, age: 0) // вызов другого init - initializer delegation (чтобы не дублировать код)
        
        // любой следующий код должен находить здесь, а не перед self.init
    }
}


var p1 = Player(name: "player1", age:24)
var p2 = Player(name: "player2", age:21)
var p3 = Player(name: "player3", age:19)
var p4 = Player(name: "player4", age:25)
var p5 = Player(name: "player5", age:28)
var p6 = Player(name: "player6", age:22)
var p7 = Player(name: "player7", age:31)

// reference type
var p8 = p2
p8.name = "super name"

var team = Team(maxPlayersCount: 5)
team.addPlayer(player: p1)
team.addPlayer(player: p2)
team.addPlayer(player: p3)
team.addPlayer(player: p4) // не добавит
team.addPlayer(player: p5)
team.addPlayer(player: p6)
team.addPlayer(player: p7) // не добавит

team.printPlayers()


// проверка ссылок
var tmp1 = p2
var tmp2 = p2
tmp1 === tmp2 // указывают на один объект

tmp1 = Player(name: "tmp", age:24)
tmp2 = Player(name: "tmp", age:24)
tmp1 === tmp2 // указывают на разные объекты (хоть их значения и равны)

// tmp1 == tmp2 // нельзя использовать
*/

// MARK: - Inheritance



// MARK: __Base


/*
class Team{
    
    var coach:Coach
    
    var name:String = "noname"
    
    let maxPlayersCount: Int
    
    var players:[Player?]!
    
    var count:Int {
        return players.filter { $0 != nil }.count
    }
    
    var desc: String = "desc"
    
    init(maxPlayersCount:Int, coach: Coach){
        self.maxPlayersCount = maxPlayersCount
        self.coach = coach
        players = [Player?]()
    }
    
    init(coach: Coach){
        self.maxPlayersCount = 100
        self.coach = coach
    }
    
    convenience init(name: String, desc: String, maxPlayersCount:Int, coach: Coach) {
        self.init(maxPlayersCount: maxPlayersCount, coach: coach)
        
        // любой код должен идти после self.init
        self.name = name
        self.desc = desc
        self.coach = coach
    }
    
    
    
    var salary = 0 {
        didSet {
            if count == 0 {
                salary = 0
            }
            print("parent salary didset")
        }
    }
    
    
    var budget:Int{
        get {
            print("parent budget get")
            return salary * count
        }
        
        set{
            print("parent budget set")
            
            if count == 0{
                self.salary = 0
            }else{
                self.salary = Int(newValue / count)
            }
        }
    }
    
    
    subscript (index: Int) -> Player?{
        
        get{
            if index > maxPlayersCount{
                print ("вышли за предел массива")
                return nil
            }else{
                return players[index]
            }
        }
        
        
        set{
            if let player = newValue, players[index] != nil{
                if checkAge(player: player) {
                    players[index] = player
                }
            }
            
        }
    }
    
    
    func printPlayers(){
        for player in players {
            if let player = player{
                print(player.name, player.age)
            }
        }
    }
    
    
    func addPlayer(player:Player){
        
        if count >= maxPlayersCount{
            print("команда уже полная, не могу добавить \(player.name)")
            return
        }
        
        if (checkAge(player: player)){
            players.append(player)
        }
    }
    
    
    
    static func checkString(text:String) -> Bool {
        return !text.contains("-")
    }
    
    func checkAge(player:Player) -> Bool{
        
        // можно использовать guard вместо if
        if player.age < 20{
            print("нельзя добавлять \(player.name) < 20 лет")
            return false
        }
        
        if player.age > coach.age{
            print("игрок \(player.name) (\(player.age)) не может быть старше тренера (\(coach.age))")
            return false
        }
        
        return true
        
    }
    
    
}


class Coach{
    var name:String
    var age:UInt
    var experience:UInt
    
    init(name:String, age:UInt, experience:UInt){
        self.name = name
        self.age = age
        self.experience = experience
    }
    
    convenience init(name:String, age:UInt){
        self.init(name: name, age: age, experience:0)
    }
    
}


class Player{
    var name:String!
    var age:UInt!
    
    required init(name:String, age:UInt){
        self.name = name
        self.age = age
    }
    
    convenience init(name:String){
        self.init(name: name, age: 0)
    }
}



// футбольный игрок - расширяет базовый класс игрока
class FootballPlayer : Player{
    
    var number:Int
    
    func kick(){
        print("kick")
    }
    
    
    required init(name: String, age: UInt) { // override указывать необязательно, т.к.  required
        number = 0 // должен идти перед super.init, т.к. согласно "2-phase init" сначала класс должен заполнять свои свойства
        super.init(name: name, age: age) // после заполнения своих свойств - можем вызывать родителя
    }
    
}

// футбольная команда - расширяет базовую команду
class FootballTeam : Team{
    
    // override + ctrl+пробел - показывает все, что можно переопределить
    
    
    
    // нельзя объявлять такую же переменную, как и в родительском классе - в Java можно, будет перекрытие (hide) переменной
    //        var desc: String = "asd"
    
    
    // override для subscript
    override subscript (index: Int) -> Player?{
        
        get{
            // можно использовать super для вызова родительского subscript
            return super[super.count-1-index] // инвертируем получение игрока (просто для примера)
        }
        
        set{
            super[index] = newValue // используем родительский subscript (не будет зацикливания)
        }
    }
    
    
    // override для stored property
    override var desc: String{
        
        // нужно обязательно переопределять и get и set, иначе ошибка компиляции
        get{
            return super.desc + " (copyright)" // возвращает родительское измененное значение
        }
        
        set{
            super.desc = newValue // просто присваиваем новое значение
        }
    }
    
    
    
    // override для computed property
    override var budget:Int{
        
        // бюджет и зп у всех увеличена в 2 раза
        get {
            print("override budget get")
            return super.budget * 2 // можно взять родительскую реализацию и умножить на 2
        }
        
        // если у родителя есть set - в дочернем классе его также обязательно нужно переопределить (иначе ошибка компиляции)
        set{
            super.budget = newValue
        }
        
    }
    
    
    
    // override для observers - на самом деле не переопределяет, а дополняет, т.е. будут вызываться все родительские willSet/didSet для этого свойства
    override var salary:Int  {
        
        // даже если в родительском классе нет willSet - в дочернем его можно добавить
        willSet{
            //            print("override salary willset")
        }
        
        // переопределяем didSet - будут вызываться все от родительских к дочерним
        didSet{
            print("override salary didset")
        }
    }
    
    // override для init
    override init(maxPlayersCount: Int, coach: Coach) {
        // если в текущем классе есть незаполненные свойства - их нужно заполнять перед вызовом super.init
        super.init(maxPlayersCount: maxPlayersCount, coach: coach)
        // сначала должен вызываться super.init, потом остальной код
        
    }
    
    
    
    
    // override для метода (печатает границы)
    override final func printPlayers() { // можно указать final, чтобы следующие дочерние классы не могли переопределить этот метод
        print("-----")
        super.printPlayers() // можно вызывать родительскую реализацию метода
        print("-----")
    }
    
    // новый метод для футбольной команды - расширение функционала
    func takePhoto(){
        print("take photo")
    }
    
}




var p1 = FootballPlayer(name: "player1", age:24)
var p2 = FootballPlayer(name: "player2", age:21)
var p3 = FootballPlayer(name: "player3", age:19)
var p4 = FootballPlayer(name: "player4", age:25)
var p5 = FootballPlayer(name: "player5", age:28)
var p6 = FootballPlayer(name: "player6", age:48)
var p7 = FootballPlayer(name: "player7", age:17)

var coach = Coach(name: "coach name", age: 45)


var team = FootballTeam(maxPlayersCount: 5, coach: coach)


team.addPlayer(player: p1)
team.addPlayer(player: p2)
team.addPlayer(player: p3) // младше 20
team.addPlayer(player: p4)
team.addPlayer(player: p5)
team.addPlayer(player: p6) // старше тренера
team.addPlayer(player: p7)


team.salary = 100
team.budget


team[1] = p4 // заменяем игрока через переопределенный subscript

team.printPlayers()

team[0]?.name

////team.budget = 20
//team.budget
//team.desc = "фывфыв "
//team.desc
*/




// MARK: __Init1

/*
 
 Класс Team пока опустим, чтобы не усложнять
 Используем только Player и дочерние классы
 
 */

/*

// в обоих классах создается автоматически default init (который является designated init)

class Player{
    var name:String = "nonname"
    var age:UInt = 0
    
    
    // напоминание: memberwise init автоматически не создается в классе (в отличие от структуры)
    
    
}


class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
}

var p = FootballPlayer() // сначала вызовется дочерний default init (без параметров), затем родительский
p.name
*/





// MARK: __Init2

/*

// создаем init вручную
class Player{
    var name:String
    var age:UInt
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"defname", age:0)
    }
    
    
}

// создается автоматически default init (который является designated init), который вызывает родительский convenience init(), созданный вручную
class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
}

var p = FootballPlayer() // автоматически будет вызван родительский convenience init
p.name
*/



// MARK: __Init3


/*
// создаем init вручную
class Player{
    var name:String
    var age:UInt
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"defname", age:0)
    }
    
    convenience init(name:String) {
        self.init(name: name, age:0)
    }
    
    convenience init(age:UInt) {
        self.init(name: "noname", age:age)
    }
    
    
}

// создается автоматически default init, который вызывает родительский designated init(), созданный вручную
class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
    init(){
        super.init(name: "test", age: 4)
    }
    
    convenience init(number:Int) {
        //        super.init(name:"noname") // не может вызывать какой-либо init родительского класса
        //        super.init(name:"noname", age: 10)
        self.init() // может вызывать только self.init
        
        self.number = number
    }
    
}

var p = FootballPlayer() // автоматически будет вызван родительский convenience init
p.name

*/



// MARK: __Init4

/*
// если в дочернем классе не реализован ни один designated initializer – он наследует все родительские designated initializer и convenience initializer


class Player{
    var name:String
    var age:UInt
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"defname", age:0)
    }
    
    convenience init(name:String) {
        self.init(name: name, age:0)
    }
    
    convenience init(age:UInt) {
        self.init(name: "noname", age:age)
    }
    
    
}


class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
    
}


var p = FootballPlayer(name: "test name", age: 24) // все 4 init родительского класса доступны для FootballPlayer
p.name
*/


// MARK: __Init5

/*
// если дочерний класс переопределяет все родительские designated initializer – дочерний класс автоматически наследует все convenience initializers из родительского класса



class Player{
    var name:String
    var age:UInt
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"defname", age:0)
    }
    
    convenience init(name:String) {
        self.init(name: name, age:0)
    }
    
    convenience init(age:UInt) {
        self.init(name: "noname", age:age)
    }
    
    
}


class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
    // переопределяет единственный designated init из родительского класса - поэтому наследуются все convenience init
    override init(name: String, age: UInt) {
        super.init(name:name, age:age)
    }
    
}

var p = FootballPlayer(name: "test name", age: 24) // все 4 init родительского класса доступны для FootballPlayer
p.name
*/



// MARK: __Init6

/*
// если дочерний класс переопределяет все родительские designated initializer – дочерний класс автоматически наследует все convenience initializers из родительского класса

class Player{
    var name:String
    var age:UInt
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    convenience init() {
        self.init(name:"defname", age:0)
    }
    
    convenience init(name:String) {
        self.init(name: name, age:0)
    }
    
    convenience init(age:UInt) {
        self.init(name: "noname", age:age)
    }
    
    
}


class FootballPlayer : Player{
    
    var number:Int = 0
    
    func kick(){
        print("kick")
    }
    
    init(number:Int, name: String, age: UInt){
        self.number = number
        super.init(name:name, age: age)
    }
    
    // convenience init переопределяет единственный designated init
    override convenience init(name: String, age: UInt) {
        self.init(name:"noname", age:5)
    }
    
}


// доступно 5 различных init
var p = FootballPlayer()
p.name

*/

// MARK: - Protocol



// MARK: __Properties
 
/*

// свойства в протоколе


protocol Person{
    
    // нельзя указывать просто переменные, нужно computed properties
    // нельзя задавать значения по-умолчанию для переменных
    
    var name:String {get set} // для классов - get и set обязательны
    var age:UInt {get} // get - обязателен, set - по желанию
    // var age:UInt // так нельзя объявлять
    
    //    let name:String // нельзя объявлять константу в протоколе
    
    
}

// класс должен выполнять протокол Person (соответствовать протоколу)
class Player : Person{
    
    
    // для переменной age НУЖНО реализовать как get, так и set
    
    // можно объявить так (чтобы автоматически реализовать и get и set)
    var name = ""
    
    //    let name = "" // нельзя использовать let, т.к. свойство read-write
    
    
    // если значение вычисляется из других переменных - можно объявить так (обязательно должен быть и get и set)
    
    //    var text = "text"
    //    var name: String {
    //        get{
    //            return self.text
    //        }
    //
    //        set{
    //            self.text = newValue + " (c)"
    //        }
    //    }
    
    
    // для переменной age МОЖНО реализовать как get, так и set
    //    var age = 0 // если не укзазать UInt - компилятор автоматически объявит как Int и будет несоответствие протоколу
    
    //    var age:UInt = 0
    
    
    let age:UInt = 0 // можно использовать let, т.к. свойство в протоколе указано как read-only
    
    
}

// создавать экземпляр протокола нельзя
//var person  = Person()


var p = Player()
p.name = "test"

print(p.name)
*/



// MARK: __Init

/*

// init в протоколе

protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
    
    // init
    init() // чтобы объект можно было создавать без указания параметров
    init(name:String, age: UInt) // фигурные скобки отсутствуют, т.к. нет реализации
    
    
}

class Player : Person{
    
    var name = "" // реализует переменную name из протокола
    var age:UInt = 0 // реализует переменную age из протокола
    
    // можно через Ctrl + пробел реализовывать init из протокола (генерация кода)
    
    // все дочерние классы должны также реализовать эти init
    
    required init() {} // required - обязательно нужно указывать, иначе - ошибка компиляции
    
    
    required init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
    
}

// дочерний класс обязан реализовать init
class FootballPlayer : Player{
    
    var number:UInt
    
    required init() {
        self.number = 0
        super.init()
    }
    
    
    required init(name: String, age: UInt) {
        //self.init() // одновременно нельзя выполнять и super и self
        
        self.number = 0
        super.init(name: name, age: age)
        
    }
    
    // доп. init
    convenience init(number:UInt) {
        self.init()
        self.number = number
    }
    
}



var p1 = Player()
p1.name = "test"
p1.age
p1.name

var p2 = Player(name: "test2", age: 15)
p2.age
p2.name

var p3 = FootballPlayer()
p3.age
p3.name
*/




// MARK: __Inherit

/*
 
// наследование протоколов
// принцип как в Java - от общего к частному
// протокол может наследоваться от любого количества протоколов


// человек имеет возраст и имя
protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
    init()
    init(name:String, age: UInt)
    
    
}

// игрок имеет игровой номер
protocol Player : Person{
    
    var number:String {get set}
    
    init(number:UInt)
    
}

// футболист получает зп
protocol FootballPlayer : Player{
    
    var salary:UInt {get set}
    
    init(salary:UInt)
    
}


// класс, который соответствует протоколу FootballPlayer - должен реализовать все свойства и init от 3 протоколов

*/

// MARK: __Methods

/*

// методы протоколов


protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
    init()
    init(name:String, age: UInt)
    
    
}


protocol Player : Person{
    
    var number:String {get set}
    
    init(number:UInt)
    
}


protocol FootballPlayer : Player{
    
    var salary:UInt {get set}
    
    init(salary:UInt)
    
    func kickTheBall() // функция без параметров и типов
    
    // mutating (для возможности изменения свойства) нужен только для value types (struct, enum), для класса при реализации указывать не нужно
    mutating func raiseSalary(quantity:UInt) -> UInt // изменяет зп - умножается на quantity
    
    
}
*/





// MARK: __Types1


//  если тип переменной является протоколом - это отражается на свойствах (read-only или read-write)

/*

protocol Person{
    var name:String {get set}
    var age:UInt {get}
}


class Player : Person{
    var name = ""
    var age:UInt = 0
}



var p1: Person = Player() // тип переменной - тип протокола
p1.name = "test"
//p1.age = 5 // нельзя присвоить, т.к. в протоколе это свойство указано как read-only


var p2: Player = Player() // тип переменной - тип класса
p2.name = "test"
p2.age = 15 // можно присвоить, т.к. в классе это свойство указано как read-write

*/




// MARK: __Types2

// протоколы как типы в других протоколах


/*

protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
    init()
    init(name:String, age: UInt)
    
    
}


protocol Player : Person{
    
    var number:String {get set}
    
    init(number:UInt)
    
}


protocol FootballPlayer : Player{
    
    var salary:UInt {get set}
    
    init(salary:UInt)
    
    func kickTheBall() -> Void // функция без параметров и типов
    
    // mutatin нужен для value types (struct, enum), для класса при реализации указывать не нужно
    mutating func raiseSalary(quantity:UInt) -> UInt // изменяет зп - умножается на quantity
    
}


protocol Team{
    
    var players:[Player?] {get set} // в коллекции может храниться любой тип-реализация протокола Player (полиморфизм)
    
    func addPlayer(player:Player) -> Void // в функцию можно передавать любую реализацию протокола Player
}


// таким образом создается абстрактный уровень приложения и достигается универсальность кода (основы ООП)
*/

// MARK: - Extension






// MARK: __Properties
/*
// extension - расширение функционала существуюшего класса (struct, enum)
// часто используется, когда нет доступа к изменению исходного кода



// добавление свойств (пример из документации)
extension Double {
    var km: Double { return self * 1_000.0 }
    var m: Double { return self }
    var cm: Double { return self / 100.0 }
    var mm: Double { return self / 1_000.0 }
    var ft: Double { return self / 3.28084 }
}


let marathon = 42.km + 195.m
*/





// MARK: __Class




/*
protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
}


protocol Player : Person{
    
    var number:UInt {get set}
    
    var salary:UInt {get set}
    
    mutating func raiseSalary(quantity:UInt) -> UInt
    
    func move()
    
}


protocol FootballPlayer : Player{
    
    func kickTheBall() // пнуть мяч
    
}


protocol Coach : Person{
    
    func training()  // тренировать
}


protocol Team{
    
    var maximumPlayersCount: UInt {get}
    
    var players:[UInt:Player?] {get set}
    
    var coach:Coach {get set}
    
    var name:String {get set}
    
    func printPlayers()
}


class BasePerson : Person { // базовый класс для всех людей
    
    var name = ""
    var age:UInt = 0
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
}



class FootballCoach : BasePerson, Coach { // футбольный тренер
    
    override init(name: String, age: UInt) {
        super.init(name: name, age: age)
    }
    
    func training() {
        print("train football")
    }
    
}


class BaseFootballPlayer : BasePerson, FootballPlayer{
    
    var number:UInt
    var salary:UInt
    
    init(name: String, age: UInt, number:UInt, salary: UInt) {
        self.number = number
        self.salary = salary
        super.init(name: name, age: age)
    }
    
    init(name: String, age: UInt, number:UInt) {
        self.number = number
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    
    override init(name: String, age: UInt) {
        self.number = 0
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    // поднять зп
    func raiseSalary(quantity:UInt) -> UInt {
        return 0//salary = salary * quantity
    }
    
    func kickTheBall() {
        print("kicked the ball")
    }
    
    func move() {
        print("\(name): run", terminator:"")
    }
    
}

class Defender : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick back")
    }
    
    override func move() {
        super.move()
        print(" back")
    }
}

class Forward : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick forward")
    }
    
    
    override func move() {
        super.move()
        print(" forward")
    }
}


class FootballTeam : Team{
    
    var name: String
    
    var maximumPlayersCount:UInt = 11
    
    var players:[UInt:Player?] = [UInt:FootballPlayer?]()
    
    
    var coach:Coach
    
    init(name:String, coach:Coach) {
        self.name = name
        self.coach = coach
    }
    
    
    func playMatch(){
        players.forEach(){$1?.move()}
    }
    
}

// вынесли метод printPlayers() в extension
extension FootballTeam{
    
    func printPlayers(){
        
        // extension имеет доступ к переменной players из класса FootballTeam
        players.forEach(){ // по синтаксису видно, что xcode может определить, откуда переменная (Cmd+Click работает))
            if let player = $1{
                print($0, player.name)
            }
        }
    }
    
}

var footballCoach = FootballCoach(name: "mr. Trainer", age: 40)

var team = FootballTeam(name: "Dream Team", coach: footballCoach)

var defender = Defender(name: "Tom", age: 24, number: 2, salary: 1000)
var forward = Forward(name: "David", age: 20, number: 10, salary: 2000)

team.players[defender.number] = defender
team.players[forward.number] = forward

team.playMatch()

team.printPlayers()
*/


// MARK: __Protocol

/*
 
 дефолтная реализация для протоколов (используется в любом проекте, основанном на методологии POP)
 
 если в классе будет по новому определен метод из extension - будет использоваться метод класса
 
 */

/*

protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
}


protocol Player : Person{
    
    var number:UInt {get set}
    
    var salary:UInt {get set}
    
    mutating func raiseSalary(quantity:UInt) -> UInt
    
    func move()
    
}

// задает дефолтную реализацию для метода raiseSalary
extension Player{
    
    // поднять зп
    func raiseSalary(quantity:UInt) -> UInt {
        return calcSalary(quantity: quantity)
    }
    
    // можно добавлять методы, которых нет в протоколе
    func calcSalary(quantity: UInt) -> UInt {
        return salary * quantity * age // переменная age видна, т.к. протокол Player расширяет протокол Person
    }
    
}


protocol FootballPlayer : Player{
    
    func kickTheBall() // пнуть мяч
    
}


protocol Coach : Person{
    
    func training()  // тренировать
}


protocol Team{
    
    var maximumPlayersCount: UInt {get}
    
    var players:[UInt:Player?] {get set}
    
    var coach:Coach {get set}
    
    var name:String {get set}
    
    func printPlayers()
}


class BasePerson : Person { // базовый класс для всех людей
    
    var name = ""
    var age:UInt = 0
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
}


class FootballCoach : BasePerson, Coach { // футбольный тренер
    
    override init(name: String, age: UInt) {
        super.init(name: name, age: age)
    }
    
    func training() {
        print("train football")
    }
    
}


class BaseFootballPlayer : BasePerson, FootballPlayer{
    
    var number:UInt
    var salary:UInt
    
    init(name: String, age: UInt, number:UInt, salary: UInt) {
        self.number = number
        self.salary = salary
        super.init(name: name, age: age)
    }
    
    init(name: String, age: UInt, number:UInt) {
        self.number = number
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    
    override init(name: String, age: UInt) {
        self.number = 0
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    
    
    func kickTheBall() {
        print("kicked the ball")
    }
    
    func move() {
        print("\(name): run", terminator:"")
    }
    
}


class Defender : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick back")
    }
    
    override func move() {
        super.move()
        print(" back")
    }
}

class Forward : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick forward")
    }
    
    
    override func move() {
        super.move()
        print(" forward")
    }
}


class FootballTeam : Team{
    
    var name: String
    
    var maximumPlayersCount:UInt = 11
    
    var players:[UInt:Player?] = [UInt:FootballPlayer?]()
    
    
    var coach:Coach
    
    init(name:String, coach:Coach) {
        self.name = name
        self.coach = coach
    }
    
    
    func playMatch(){
        players.forEach(){$1?.move()}
    }
    
    // если в классе указан такой же метод, как в extension - будет использовать этот метод
    func printPlayers() {
        print("own impl")
    }
}



// дефолтная реализация для нужных методов из протокола Team
extension Team{ // не обязан реализовывать все методы - только нужные
    
    // этот метод теперь не обязаны реализовывать классы
    func printPlayers(){
        
        players.forEach(){ // по синтаксису видно, что xcode видит переменную players, т.к. она из протокола
            if let player = $1{
                print($0, player.name)
            }
        }
    }
    
}

var footballCoach = FootballCoach(name: "mr. Trainer", age: 40)

var team = FootballTeam(name: "Dream Team", coach: footballCoach)

var defender = Defender(name: "Tom", age: 24, number: 2, salary: 1000)
var forward = Forward(name: "David", age: 20, number: 10, salary: 2000)

team.players[defender.number] = defender
team.players[forward.number] = forward

team.players[2]??.raiseSalary(quantity: 10) // два ?? означают двойную распаковку (сначала элемент dict, затем сам Player)

team.playMatch()

team.printPlayers() // используется метод класса, а не extension
*/





// MARK: __Adopt

/*
 
 если тип полностью соответствует какому-либо протоколу, но сам протокол не был указан,
 то можно указать протокол через extension
 После этого можно использовать полиморфизм для этого типа.
 Особенно актуально, когда нет доступа к исходному коду типа
 
 */

/*


// пример из документации


protocol TextRepresentable {
    var textualDescription: String { get }
}

struct Hamster {
    var name: String
    var textualDescription: String {
        return "A hamster named \(name)"
    }
}


extension Hamster: TextRepresentable {} // если закомментировать эту строку - выйдет ошибка value of type 'Hamster' does not conform to specified type 'TextRepresentable'

var h:TextRepresentable = Hamster(name: "test")
h.textualDescription
*/


// MARK: - Cast

/*
 
 приведение типов для объектов коллекции
 
 (берем реализацию без generics)
 
 */

/*

protocol Person{
    
    var name:String {get set}
    var age:UInt {get}
    
}


protocol Player : Person{
    
    var number:UInt {get set}
    
    var salary:UInt {get set}
    
    mutating func raiseSalary(quantity:UInt) -> UInt
    
    func move()
    
}

extension Player{
    
    
    func raiseSalary(quantity:UInt) -> UInt {
        return calcSalary(quantity: quantity)
    }
    
    
    func calcSalary(quantity: UInt) -> UInt {
        return salary * quantity * age
    }
    
}


protocol FootballPlayer : Player{
    
    func kickTheBall()
    
}


protocol Coach : Person{
    
    func training()
}


protocol Team{
    
    var maximumPlayersCount: UInt {get}
    
    var players:[UInt:Player?] {get set}
    
    var coach:Coach {get set}
    
    var name:String {get set}
    
    var count:Int {get}
    
    func printPlayers()
    
    func showPlayersSalary(amount: UInt) -> [UInt: Player?]
    
}


extension Team{
    
    var count:Int {
        return players.filter { $1 != nil }.count // $0 - key, $1 - value
    }
    
    func showPlayersSalary(amount: UInt) -> [UInt: Player?] {
        return players.filter {
            $1!.salary >= amount
        }
    }
    
}


class BasePerson : Person {
    
    var name = ""
    var age:UInt = 0
    
    init(name: String, age: UInt) {
        self.name = name
        self.age = age
    }
    
}


class FootballCoach : BasePerson, Coach {
    
    override init(name: String, age: UInt) {
        super.init(name: name, age: age)
    }
    
    func training() {
        print("train football")
    }
    
}


class BaseFootballPlayer : BasePerson, FootballPlayer{
    
    var number:UInt
    var salary:UInt
    
    init(name: String, age: UInt, number:UInt, salary: UInt) {
        self.number = number
        self.salary = salary
        super.init(name: name, age: age)
    }
    
    init(name: String, age: UInt, number:UInt) {
        self.number = number
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    
    override init(name: String, age: UInt) {
        self.number = 0
        self.salary = 0
        super.init(name: name, age: age)
    }
    
    
    
    func kickTheBall() {
        print("kicked the ball")
    }
    
    func move() {
        print("\(name): run", terminator:"")
    }
    
}


class Defender : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick back")
    }
    
    override func move() {
        super.move()
        print(" back")
    }
    
    func defence(){
        print("\(name): defence")
    }
}

class Forward : BaseFootballPlayer{
    override init(name: String, age: UInt, number:UInt, salary: UInt) {
        super.init(name: name, age: age, number: number, salary: salary)
    }
    
    override func kickTheBall() {
        print("kick forward")
    }
    
    
    override func move() {
        super.move()
        print(" forward")
    }
    
    
}


class FootballTeam : Team{
    
    var name: String
    
    var maximumPlayersCount:UInt = 11
    
    var players:[UInt:Player?] = [UInt:FootballPlayer?]() // в эту коллекцию могут попадать все реализации протокола FootballPlayer
    
    
    var coach:Coach
    
    init(name:String, coach:Coach) {
        self.name = name
        self.coach = coach
    }
    
    
    func playMatch(){
        players.forEach(){
            
            // т.к. у типов есть свои специфичные методы - нужно сделать приведение
            
            if let defender = $1 as? Defender{ // сначала проверяем на тип - если это защитник
                defender.defence() // защищаемся
            }else if let forward = $1 as? Forward{ // если это нападающий
                forward.move() //  бежим
                forward.kickTheBall() // и пинаем мяч
            }
        }
    }
    
    func printPlayers() {
        print("own impl")
    }
}



extension Team{
    
    
    func printPlayers(){
        
        players.forEach(){
            if let player = $1{
                print($0, player.name)
            }
        }
    }
    
}

var footballCoach = FootballCoach(name: "mr. Trainer", age: 40)

var team = FootballTeam(name: "Dream Team", coach: footballCoach)

var defender = Defender(name: "Tom", age: 24, number: 2, salary: 1000)
var forward = Forward(name: "David", age: 20, number: 10, salary: 2000)

team.players[defender.number] = defender
team.players[forward.number] = forward

team.players[2]??.raiseSalary(quantity: 10)

team.playMatch()
*/



// MARK: - Enum


// MARK: __Base
/*
 
 работа с enum
 
 */

/*
// месяцы в году
// название enum - желательно в единственном числе (не Months)
enum Month {
    
    // нельзя хранить stored properties, но можно добавлять computed properties
    
    // все слова с меленькой буквы (в отличии от Java, где все буквы - прописные)
    case january
    case february
    case march
    case april
    case may
    case june
    case july
    case august
    case september
    case october
    case november
    case december
    
}




// объявление переменной

var april = Month.april
//var april:Month = .april
*/








// MARK: __Init


// можем задавать любые возможности для инициализации
/*


enum Month {
    
    init?(abbr: String) {
        switch abbr {
        case "янв", "jan":
            self = .january
        case "фев", "feb":
            self = .february
        // и так далее
        default:
            return nil
        }
    }
    
    case january
    case february
    case march
    case april
    case may
    case june
    case july
    case august
    case september
    case october
    case november
    case december
    
}

var month = Month(abbr: "янв")

*/




// MARK: __Methods


// можем задавать любые методы

/*

enum Month {
    
    init?(abbr: String) {
        switch abbr {
        case "янв", "jan":
            self = .january
        case "фев", "feb":
            self = .february
        // и так далее
        default:
            return nil
        }
    }
    
    case january
    case february
    case march
    case april
    case may
    case june
    case july
    case august
    case september
    case october
    case november
    case december
    
    
    // следующий месяц
    mutating func next() {
        
        switch self {
        case .january:
            self = .february
        case .february:
            self = .march
        case .march:
            self = .april
        case .april:
            self = .may
        case .may:
            self = .june
        case .june:
            self = .july
        case .july:
            self = .august
        case .august:
            self = .september
        case .september:
            self = .october
        case .october:
            self = .november
        case .november:
            self = .december
        case .december:
            self = .january
            
        }
        
    }
    
}

var month = Month(abbr: "янв")!

month.next()

month
*/



// MARK: __Associated Values

/*

// типы операций с товаром
enum Operation {
    
    case pay (amount:Int, cardNumber: String)
    case send (address:String, product:Product)
    case moneyback (product: Product)
    
}

struct Product{
    var name:String
}


var pay1 = Operation.pay(amount: 100, cardNumber: "12311")

var send1 = Operation.send(address: "1 street", product: Product(name: "book"))
*/




// MARK: __Raw Values

// raw value может быть только элементарным типом (Int, Double, Float, Character, String)
/*

enum Month1: Int { // тип raw value указываем как при объявлении переменной
    
    case january = 1
    case february = 2
    case march = 3
    case april = 4
    case may = 5
    case june = 6
    case july = 7
    case august = 8
    case september = 9
    case october = 10
    case november = 11
    case december = 12
}


// более сокращенный вариант
enum Month2: Int {
    
    // все цифры расставятся автоматически
    case january,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    october,
    november,
    december
}

enum Month3: Int {
    
    // если надо начать с 1 (или любого другого числа)
    case january = 1,
    february,
    march,
    april,
    may,
    june,
    july,
    august,
    september,
    october,
    november,
    december
}



enum Month4: String {
    
    case january // автоматически все значения будут как названия ("january")
    case february
    case march
    case april
    case may
    case june
    case july
    case august
    case september
    case october
    case november
    case december
    
}


Month1.february.rawValue
Month2.january.rawValue
Month3.april.rawValue

// получение элемента enum по rawValue
let month = Month1(rawValue: 2) // возвращает Optional


*/




// MARK: __Nested
/*
struct A{
    
    var aVar = 3
    
    class B{
        
        var bVar = "str"
        
        struct C{
            
            var cVar = true
            
            class D{
                
                var dVar = 1
                
                func print(){
                    // функция имеет доступ только к своим переменным
                    Swift.print(dVar) // Swift требуется, т.к. находимся во внутреннем типе
                }
            }
        }
    }
}




var d = A.B.C.D() // цепочка вызовов - создается только экземпляр D (внешние типы не создаются)


d.print()



enum CarType{
    
    case Sedan
    case Minivan
    
    enum CarModel{
        case Toyota
        case BMW
    }
    
}


CarType.CarModel.Toyota
*/



// MARK: __GenericsEnum

// enum могут использовать Generics для associated values

/*


enum Operation<Amount, CardNumber, Product, Address> {
    
    case pay (amount:Amount, cardNumber: CardNumber)
    case send (address:Address, product:Product)
    case moneyback (product: Product)
    
}

struct Product{
    var name:String
}


typealias oper = Operation<Int, String, Product, String>

var pay = oper.pay(amount: 2, cardNumber: "123123123")

var send = oper.send(address: "addr", product: Product(name: "test product"));

*/



// MARK: __Indirect

/*
 
 associated values из enum содержат ссылку на этот же enum (рекурсия)
 
 часто используется при построении дерева, графов и пр.
 
 indirect - указывает компилятору, что имеет место вложенность enum
 
 - правильное распределение памяти, не будет ошибки overflow
 - вместо того, чтобы создавать один большой объект enum - создаются ссылки от одного enum к другому (т.е. сам enum уже не хранит associated values, а просто ссылается на них)
 - таким образом вложенность может быть бесконечной (пока хватит памяти)
 
 
 */
/*


// пример построение дерева
indirect enum Tree {
    case Empty
    case Node(value: Int, left: Tree, right: Tree)
}

/*
 
 3
 / \
 1   2
 
 */
let tree1 = Tree.Node(value: 1, left: Tree.Empty, right: Tree.Empty)

let tree2 = Tree.Node(value: 2, left: Tree.Empty, right: Tree.Empty)

let tree3 = Tree.Node(value: 3, left: tree1, right: tree2)

*/


// MARK: - Error

/*
// пример кода из документации Apple (немного доработанный)
// https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/ErrorHandling.html


// содержит типы ошибок
enum VendingMachineError: Error { // Error помечает enum, что здесь хранятся типы собственных ошибок
    case invalidSelection // неверно сделали заказ (неверный выбор)
    case insufficientFunds(coinsNeeded: Int) // не хватает денег
    case outOfStock // не осталось продуктов на складе
}

// продукт для заказа
struct Item {
    var price: Int
    var count: Int
}


// машина для продажи продуктов
class VendingMachine {
    
    // набор продуктов, которые можно заказать (dictionary)
    var inventory = [
        "Candy Bar": Item(price: 12, count: 7),
        "Chips": Item(price: 10, count: 4),
        "Pretzels": Item(price: 7, count: 11)
    ]
    
    // сколько заплатили
    var coinsDeposited = 0
    
    // процесс заказа
    func vend(itemNamed name: String) throws {
        
        // есть ли такой продукт в наличии
        guard let item = inventory[name] else {
            throw VendingMachineError.invalidSelection // вручную выбрасываем исключение
        }
        
        // есть ли вообще товары на складе
        guard item.count > 0 else {
            throw VendingMachineError.outOfStock // нет на складе
        }
        
        // хватает ли денег на покупку
        guard item.price <= coinsDeposited else {
            throw VendingMachineError.insufficientFunds(coinsNeeded: item.price - coinsDeposited) // не хватает денег
        }
        
        // отнимаем из депозита сумму
        coinsDeposited -= item.price
        
        
        // уменьшаем количество соотв. товаров на 1
        var newItem = item
        newItem.count -= 1
        inventory[name] = newItem
        
        print("Dispensing \(name)")
    }
}

// кто что любит заказывать
let favoriteSnacks = [
    "Alice": "Chips",
    "Bob": "Licorice",
    "Eve": "Pretzels",
]



func buyFavoriteSnack(person: String, vendingMachine: VendingMachine) throws { // перебрасывает ошибку выше в место вызова функции
    
    let snackName = favoriteSnacks[person] ?? "Candy Bar" // если у человека нет любимого продукта - значит это будет "Candy Bar"
    try vendingMachine.vend(itemNamed: snackName) // заказать продукт
}



func test(name:String, coins:Int){
    
    // можно указыват в любом месте метода
    defer {
        // не может содержать break, return, throw
        print("clean1")
    }
    
    // порядок вызова блоков defer - снизу вверх как они объявлены
    defer {
        print("clean2")
    }
    
    
    let vendingMachine = VendingMachine()
    
    vendingMachine.coinsDeposited = coins // вносим деньги
    
    do {
        try buyFavoriteSnack(person: name, vendingMachine: vendingMachine)
    } catch VendingMachineError.invalidSelection {
        print("Invalid Selection.")
    } catch VendingMachineError.outOfStock {
        print("Out of Stock.")
    } catch VendingMachineError.insufficientFunds(let coinsNeeded) { // константа будет хранить сумму, которой не хватило
        print("Insufficient funds. Please insert an additional \(coinsNeeded) coins.")
    } catch { // если произошла другая ошибка
        print(error) // хранит ошибку
    }
    
}


func testOptional(name: String, coins: Int) throws {
    
    let vendingMachine = VendingMachine()
    
    vendingMachine.coinsDeposited = coins
    
    try buyFavoriteSnack(person: name, vendingMachine: vendingMachine)
}


test(name: "Alice", coins: 100) // success



// в случае ошибки - вернется nil (не сможем посмотреть тип ошибки)
try? testOptional(name: "Bob", coins: 1)


// если точно уверены, что не будет ошибки, используем ! (тогда не надо ничего обрабатывать)
var result = try! testOptional(name: "Eve", coins: 500)
print(result)


fatalError("критичная ошибка") // системные метод, можно вызывать в любом месте кода, чтобы остановить работу приложения (критичная ошибка)

*/


// MARK: - Generics

// MARK: __Func

/*
// основное назначение generics - универсализация типов


// самый частый пример для generics


// если бы не было generics
func sum(a: Int, b: Int) -> Int {
    return a + b
}

func sum(a: Double, b: Double) -> Double {
    return a + b
}



// решение с generics

func sumGenerics<T: Numeric>(a: T, b: T) -> T { // Numeric - можно передавать только числа
    return a + b
}

sumGenerics(a:1.0, b:35.1) // тип T задается при вызове

*/









// MARK: __ClassGenerics

//  пример из документации, измененный и доработанный

/*

// может принимать только Int
struct IntStack1 {
    var items = [Int]()
    
    mutating func push(_ item: Int) {
        items.append(item)
    }
    
    mutating func pop() -> Int {
        return items.removeLast()
    }
}


// может принимать любой тип
struct Stack2<Element> {
    
    var items = [Element]()
    
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}


struct Person{
    var name:String
}


// может принимать только Int
var stack1 = IntStack1()
stack1.push(2)


// может принимать любой объявленный тип
var stackOfStrings = Stack2<String>()
stackOfStrings.push("uno")
stackOfStrings.push("dos")
stackOfStrings.push("tres")
stackOfStrings.push("cuatro")


var stackOfPersons = Stack2<Person>()
stackOfPersons.push(Person(name:"Jim"))
stackOfPersons.push(Person(name:"David"))
stackOfPersons.push(Person(name:"Tim"))

stackOfPersons.items

stackOfPersons.pop() // убираем одного

stackOfPersons.items

*/





// MARK: __Extension

//  пример из документации, измененный и доработанный

/*


// может принимать любой тип
struct GenericStack<Element> {
    
    var items = [Element]()
    
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}


struct Person{
    var name:String
}


extension GenericStack { // видит тип Element из структуры
    var topItem: Element? {
        return items.isEmpty ? nil : items[items.count - 1]
    }
}


// может принимать любой объявленный тип
var stackOfStrings = GenericStack<String>()
stackOfStrings.push("uno")
stackOfStrings.push("dos")
stackOfStrings.push("tres")
stackOfStrings.push("cuatro")

stackOfStrings.topItem


var stackOfPersons = GenericStack<Person>()
stackOfPersons.push(Person(name:"Jim"))
stackOfPersons.push(Person(name:"David"))
stackOfPersons.push(Person(name:"Tim"))

stackOfPersons.items

stackOfPersons.pop() // убираем одного

stackOfPersons.items


*/





// MARK: __Type

/*
//  пример из документации, измененный и доработанный

// можем ограничить тип generics


// может принимать только реализации проктола Person
struct GenericStack<Element: Person> {
    
    var items = [Element]()
    
    mutating func push(_ item: Element) {
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}

protocol Person{
    var name:String {get set}
}

struct Man:Person{
    var name:String
}

struct Woman:Person{
    var name:String
    
}


extension GenericStack { // видит тип Element из структуры
    var topItem: Element? {
        return items.isEmpty ? nil : items[items.count - 1]
    }
}



var stack1 = GenericStack<Man>()
stack1.push(Man(name:"Jim"))
stack1.push(Man(name:"David"))
stack1.push(Man(name:"Tim"))

stack1.items


var stack2 = GenericStack<Woman>()

stack2.push(Woman(name:"Sara"))
stack2.push(Woman(name:"Anna"))
stack2.push(Woman(name:"Kate"))


stack2.items


// var stack3 = GenericStack<String>() // нельзя, т.к. это не Person

*/




// MARK: __Associated Type

// если нужно задать generic type для протокола
/*


struct GenericStack<Element>: Stack {
    
    //    typealias Item = Element // можно явно определить тип Item из протокола
    
    var items = [Element]()
    
    mutating func push(_ item: Element) { // если явно не опрередлили тип через typealias - комплятор сам "поймет" исходя из параметра функции
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}


protocol Stack{
    
    associatedtype Item
    
    mutating func push(_ item: Item)
    
    mutating func pop() -> Item
    
}

protocol Person{
    var name:String {get set}
}

struct Man:Person{
    var name:String
}

struct Woman:Person{
    var name:String
    
}


extension GenericStack {
    var topItem: Element? {
        return items.isEmpty ? nil : items[items.count - 1]
    }
}



var stack1 = GenericStack<Man>()
stack1.push(Man(name:"Jim"))
stack1.push(Man(name:"David"))
stack1.push(Man(name:"Tim"))

stack1.items


var stack2 = GenericStack<Woman>()

stack2.push(Woman(name:"Sara"))
stack2.push(Woman(name:"Anna"))
stack2.push(Woman(name:"Kate"))


stack2.items


var stack3 = GenericStack<String>()

stack3.push("Sara")
stack3.push("Anna")
stack3.push("Kate")


stack3.items
*/







// MARK: __Where

/*
// если нужно задать generic type для протокола


struct GenericStack<Element : Person>: Stack {// какой тип установим для Element - такой будет и для протокола
    
    //    typealias Item = Element // можно явно определить тип Item из протокола
    
    var items = [Element]()
    
    mutating func push(_ item: Element) { // если явно не определили тип через typealias - комплятор сам "поймет" исходя из параметра функции
        items.append(item)
    }
    
    mutating func pop() -> Element {
        return items.removeLast()
    }
}


protocol Stack{
    
    associatedtype Item : Person // только реализации протокола Person
    
    var items:[Item] {get set}
    
    mutating func push(_ item: Item)
    
    mutating func pop() -> Item
    
}

protocol Person{
    var name:String {get set}
}

struct Man:Person{
    var name:String
}

struct Woman:Person{
    var name:String
    
}


extension Stack {
    var topItem: Item? {
        return items.isEmpty ? nil : items[items.count - 1]
    }
}


extension Stack where Item == Man { // этот extension будет задействован только если тип Man
    func work() {
        print("working...")
    }
}



var stack1 = GenericStack<Man>()
stack1.push(Man(name:"Jim"))
stack1.push(Man(name:"David"))
stack1.push(Man(name:"Tim"))

stack1.work() // метод виден

var stack2 = GenericStack<Woman>()

stack2.push(Woman(name:"Sara"))
stack2.push(Woman(name:"Anna"))
stack2.push(Woman(name:"Kate"))

//stack2.work() // не виден этот метод, т.к. Woman

stack2.items



// нельзя, т.к. можно только Person
//var stack3 = GenericStack<String>()

*/
