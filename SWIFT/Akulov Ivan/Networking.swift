
// список кодов состояния http - https://ru.wikipedia.org/wiki/Список_кодов_состояния_HTTP
// Paw - https://paw.cloud/ - программа для генирации и онлайн просмотров запросов

import Foundation



//MARK: GET request

/*
@IBAction func getRequest (_ sender: Any) {
    guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {
        return
    }
    
    let session = URLSession.shared
    session.dataTask(with: url) { (data, response, error) in
        guard let response = response,
            let data = data else {return}
        print(response) // вывод ответа на запрос - показывает код 200 - УСПЕШНО
        print(data)     // в двоичном коде. Надо сериализовывать
        
        do  {
            let json = try JSONSerialization.jsonObject(with: data, options: []) // сериализация
            print(json)                                                         // рспечатка сериализованного JSON
        } catch {
            print(error)
        }
        
        }.resume()
}
 */


//MARK: POST request - запись, закачка
/*

@IBAction func postRequest (_ sender: Any) {
    guard let url = URL(string: "https://jsonplaceholder.typicode.com/posts") else {return}
    
    
    let userData = ["Course" : "Networking", "Lesson" : "GET and POST request"]
    var request = URLRequest(url: url)
    
    request.httpMethod = "POST" // выбираем параметр запроса - POST (что нибудь записать, перезаписать)
    
    guard let htttpBody = try? JSONSerialization.data(withJSONObject: userData, options: []) else {return}
    
    request.httpBody = htttpBody // вылаживаем данные в тело запроса
    request.addValue("application/json", forHTTPHeaderField: "Content-Type") // выбираем тип данных
    
    let session = URLSession.shared
    
    session.dataTask(with: request) { (data, response, error) in
        guard let response = response, let data = data else {return}
        print(response)  // в статусе код - 201, что значит созданно
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            print(json)
        } catch {
            print(error)
        }
        }.resume()
    
}
*/


// MARK: Декодирования данных с JSON в свой формат

/*
func fetchData () {
    let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_course"
    
    guard let url = URL(string: jsonUrlString) else {return}
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        
        guard let data = data else {return}
        
        do {
            let course = try JSONDecoder().decode(Course.self, from: data) //сохрание данных в модель Course (структура) - поля должны соответсвовать
            print(course.imageUrl)
        } catch let error {
            print("error serialization json", error)
        }
        
        }.resume()
}


struct Course: Decodable {
    let id: Int
    let name: String
    let link: String
    let imageUrl: String
    let number_of_lessons: Int
    let number_of_tests: Int
}
*/




/*
func fetchData () {
    //let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_course"
    
    // URL с массивом
    //let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_courses"
    
    // URL с описанием
    //let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_website_description"
    
    // URL с json с отсутствующими полями
    let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_missing_or_wrong_fields"
    
    guard let url = URL(string: jsonUrlString) else {return}
    
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        
        guard let data = data else {return}
        
        // запрос на джейсон с отсутвующми полями. Чтоб запрос прошел удачно, надо в сруктурах указать проперти как опциональные
        do {
            let webSiteDescription = try JSONDecoder().decode(WebsiteDescription.self, from: data)
            print("\(webSiteDescription.websiteName ?? "") \(webSiteDescription.websiteDescription ?? "") \( webSiteDescription.courses.first?.name ?? "")")
        } catch let error {
            print("error serialization json", error)
        }
        
        
        
        // структура WebsiteDescription
        //            do {
        //                let webSiteDescription = try JSONDecoder().decode(WebsiteDescription.self, from: data)
        //                print("\(webSiteDescription.websiteName) \(webSiteDescription.websiteDescription) \(String(describing: webSiteDescription.courses.first?.name))")
        //            } catch let error {
        //                print("error serialization json", error)
        //            }
        
        
        // если массив
        //            do {
        //                let courses = try JSONDecoder().decode([Course].self, from: data)
        //                print(courses)
        //            } catch let error {
        //                print("error serialization json", error)
        //            }
        
        // для одного обьекта в json
        //            do {
        //                let course = try JSONDecoder().decode(Course.self, from: data)
        //                print(course.imageUrl)
        //            } catch let error {
        //                print("error serialization json", error)
        //            }
        
        }.resume()
}


}





struct WebsiteDescription: Decodable {
    let websiteDescription: String?
    let websiteName: String?
    let courses: [Course]
}
struct Course: Decodable {
    let id: Int?
    let name: String?
    let link: String?
    let imageUrl: String?
    let number_of_lessons: Int?
    let number_of_tests: Int?
}
*/

//MARK: Перевод с Snake case в Camel case
/*
let jsonUrlString = "https://swiftbook.ru//wp-content/uploads/api/api_courses"

guard let url = URL(string: jsonUrlString) else {return}

URLSession.shared.dataTask(with: url) { (data, response, error) in
    
    guard let data = data else {return}
    do {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase             // будет автоматически переведены переменные с snakeCase в CamelCase
        self.courses = try decoder.decode([Course].self, from: data)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    } catch let error {
        print("error serialization json", error)
    }
    
    }.resume()
*/

//MARK: Upload Image

// загрузка на сайт https://api.imgur.com/oauth2/addclient
/*
static func uploapImage(url: String) {
    let image = UIImage(named: "audi_tt_rs_2017-wallpaper-2880x1800")!
    let httpHeaders = ["Authorization" : "Client-ID 0c7aea1debb0640"]
    guard let imageProperties = ImageProperties(with: image, forKey: "image") else {return}
    
    guard let url = URL(string: url) else {return}
    
    var request = URLRequest(url: url)
    request.httpMethod = "POST"
    request.allHTTPHeaderFields = httpHeaders
    request.httpBody = imageProperties.data
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
        if let response = response {
            print(response)
        }
        
        if let data = data {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }
        }.resume()
}
*/

// MARK: Загрузка в фоном режиме

// в настройках проекта -> Capabilities -> Background Modes -> ON

/*
class DataProvider: NSObject {
    
    private var downloatTask: URLSessionDownloadTask!
    var fileLocation: ((URL) -> ())?
    var onProgress: ((Double) -> ())?
    
    private lazy var bgSession: URLSession = {
        // поведение нашей сессии при загрузке и разгрузке данных. Для возможности бэеграунда вызываем background. Идентифаер пишем bundle нашего преложения
        let config = URLSessionConfiguration.background(withIdentifier: "DV.Alamofire-test")
        config.isDiscretionary = true // могут ли фоновые задачи быть запланированы по обеспечении системы. Рекомендовано устанавливать true. По умолчания false
        config.sessionSendsLaunchEvents = true //по завершению загрузки данных приложение загрузится в фоновом режиме
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    func startDownload() {
        
        if let url = URL(string: "https://speed.hetzner.de/100MB.bin") {
            downloatTask = bgSession.downloadTask(with: url) // создание url сешн
            downloatTask.earliestBeginDate = Date().addingTimeInterval(3) // загрузка начнется не ранее заданного времени - через три секунды
            downloatTask.countOfBytesClientExpectsToSend = 512 // наиболее вероятную верхную границу байтов которую клиент желает отправить - рекомендуется указывать
            downloatTask.countOfBytesClientExpectsToReceive = 100 * 1024 * 1024 // наиболее вероятную врехную границу числа байтов который клиент должен получить
            downloatTask.resume()
        }
    }
    
    func stopDownload() {
        downloatTask.cancel()
    }
}

extension DataProvider: URLSessionDelegate {
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            guard
                let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.bgSessionCommpletionHandler
                else {return}
            
            appDelegate.bgSessionCommpletionHandler = nil
            completionHandler()
        }
    }
}

extension DataProvider: URLSessionDownloadDelegate {
    
    // доступ к ссылке на заргруженный файл
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("Did finish downloading: \(location.absoluteString)")
        
        DispatchQueue.main.async {
            self.fileLocation?(location)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard totalBytesWritten != NSURLSessionTransferSizeUnknown else {return}
        let progress = Double(totalBytesWritten / totalBytesExpectedToWrite)
        print("Download progress: \(progress)")
        
        DispatchQueue.main.async {
            self.onProgress?(progress)
        }
    }
}
*/

/*
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var bgSessionCommpletionHandler: ( () -> () )?
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        bgSessionCommpletionHandler = completionHandler
    }
    
}
*/


// MARK: Создание background session - download

/*
import UIKit

class DataProvider: NSObject {
    
    private var downloatTask: URLSessionDownloadTask!
    var fileLocation: ((URL) -> ())?
    var onProgress: ((Double) -> ())?
    
    private lazy var bgSession: URLSession = {
        // поведение нашей сессии при загрузке и разгрузке данных. Для возможности бэеграунда вызываем background. Идентифаер пишем bundle нашего преложения
        let config = URLSessionConfiguration.background(withIdentifier: "DV.Alamofire-test")
        config.isDiscretionary = true // запуск задачи в оптимальное время - может дождаться когда включится вай фай. могут ли фоновые задачи быть запланированы по обеспечении системы. Рекомендовано устанавливать true. По умолчания false
        config.timeoutIntervalForResource = 300 // Время ожидания сети в секундах
        config.waitsForConnectivity = true // ожидания подключения к сети. по умолчании true - работает только если изначально не было подключеня в сети. но если произошел сбой в средине сессии, то этот параметр не поможет -
        config.sessionSendsLaunchEvents = true //по завершению загрузки данных приложение загрузится в фоновом режиме
        return URLSession(configuration: config, delegate: self, delegateQueue: nil)
    }()
    
    func startDownload() {
        
        if let url = URL(string: "https://speed.hetzner.de/100MB.bin") {
            downloatTask = bgSession.downloadTask(with: url) // создание url сешн
            downloatTask.earliestBeginDate = Date().addingTimeInterval(3) // загрузка начнется не ранее заданного времени - через три секунды
            downloatTask.countOfBytesClientExpectsToSend = 512 // наиболее вероятную верхную границу байтов которую клиент желает отправить - рекомендуется указывать
            downloatTask.countOfBytesClientExpectsToReceive = 100 * 1024 * 1024 // наиболее вероятную врехную границу числа байтов который клиент должен получить
            downloatTask.resume()
        }
    }
    
    func stopDownload() {
        downloatTask.cancel()
    }
}

extension DataProvider: URLSessionDelegate {
    
    func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        DispatchQueue.main.async {
            guard
                let appDelegate = UIApplication.shared.delegate as? AppDelegate,
                let completionHandler = appDelegate.bgSessionCommpletionHandler
                else {return}
            
            appDelegate.bgSessionCommpletionHandler = nil
            completionHandler()
        }
    }
}

extension DataProvider: URLSessionDownloadDelegate {
    
    // доступ к ссылке на заргруженный файл
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        print("Did finish downloading: \(location.absoluteString)")
        
        DispatchQueue.main.async {
            self.fileLocation?(location)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard totalBytesWritten != NSURLSessionTransferSizeUnknown else {return}
        let progress = Double(totalBytesWritten) / Double(totalBytesExpectedToWrite)
        print("Download progress: \(progress)")
        
        DispatchQueue.main.async {
            self.onProgress?(progress)
        }
    }
}


extension DataProvider: URLSessionTaskDelegate {
    
    // Восстановление соеденения
    func urlSession(_ session: URLSession, taskIsWaitingForConnectivity task: URLSessionTask) {
        // ожидания соединения
    }
}
*/
/*

import UIKit
import UserNotifications

enum Actions: String, CaseIterable {
    case downloadImage = "Download Image"
    case get = "GET"
    case post = "POST"
    case ourCourses = "Our Courses"
    case uploadImage = "Upload Image"
    case downloadFile = "Download File"
}

private let reuseIdentifier = "Cell"
private let url = "https://jsonplaceholder.typicode.com/posts"
private let uploadImage = "https://api.imgur.com/3/image"

class MainViewController: UICollectionViewController {
    
    //let actions = ["Download Image", "GET", "POST", "Our Courses", "Upload Image"]
    
    let actions = Actions.allCases
    private var alert: UIAlertController!
    private let dataProvider = DataProvider()
    private var filePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self
        title = "Controls"
        
        registerForNotification()
        
        dataProvider.fileLocation = {
            (location) in
            // Сохранить файл для дальнейшего испльзования
            print("Download finished: \(location.absoluteString)")
            self.filePath = location.absoluteString
            self.alert.dismiss(animated: false, completion: nil)
            self.posNotification()
        }
    }
    
    private func showAlert() {
        alert = UIAlertController(title: "Downloading...", message: "0%", preferredStyle: .alert)
        
        let hight = NSLayoutConstraint(item: alert.view,
                                       attribute: .height,
                                       relatedBy: .equal,
                                       toItem: nil,
                                       attribute: .notAnAttribute,
                                       multiplier: 0,
                                       constant: 170)
        alert.view.addConstraint(hight)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive) {
            (action) in
            self.dataProvider.stopDownload()
        }
        
        alert.addAction(cancelAction)
        present(alert, animated: true) {
            
            let size = CGSize(width: 40, height: 40)
            let point = CGPoint(x: self.alert.view.frame.width / 2 - size.width/2, y: self.alert.view.frame.height / 2 - size.height/2)
            let activityIndicator = UIActivityIndicatorView(frame: CGRect(origin: point, size: size))
            activityIndicator.color = .gray
            activityIndicator.startAnimating()
            
            let progressView = UIProgressView(frame: CGRect(x: 0, y: self.alert.view.frame.height - 44, width: self.alert.view.frame.width, height: 2))
            progressView.tintColor = .blue
            
            self.dataProvider.onProgress = {
                (progress) in
                progressView.progress = Float(progress)
                self.alert.message = String(Int(progress * 100)) + "%"
            }
            
            self.alert.view.addSubview(activityIndicator)
            self.alert.view.addSubview(progressView)
        }
    }
    
    
    UICollectionViewDataSource
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return actions.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! CollectionViewCell
        cell.label.text = actions[indexPath.row].rawValue
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let action = actions[indexPath.row]
        switch action {
        case .downloadImage:
            performSegue(withIdentifier: "ShowImage", sender: self)
        case .get:
            NetWorkManager.getRequest(url: url)
        case .post:
            NetWorkManager.postRequest(url: url)
        case .ourCourses:
            performSegue(withIdentifier: "OurCourses", sender: self)
        case .uploadImage:
            NetWorkManager.uploapImage(url: uploadImage)
        case .downloadFile:
            showAlert()
            dataProvider.startDownload()
            
        }
        
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width - 30, height: 100)
    }
}

extension MainViewController {
    private func registerForNotification() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { (_, _) in
            
        }
        
    }
    
    private func posNotification () {
        let content = UNMutableNotificationContent()
        content.title = "Download complete!"
        content.body = "Your background transfer has completed. File path: \(filePath!)"
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 3, repeats: false)
        let request = UNNotificationRequest(identifier: "TransferComplete", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
}
*/


// MARK: - Alamofire

/*

import Foundation
import Alamofire

class AlamofireNetworkRequest {
    
    static var onProgress: ((Double) -> ())?
    static var completed: ((String) -> ())?
    
    static func sendRequest(url: String, completion: @escaping (_ courses: [Course]) -> () ) {
        
        guard let url = URL(string: url) else {return}
        
        request(url, method: .get).validate().responseJSON { (response) in // если не постаить валидэйт, то result всегда будет success, исключая - если не будет подключения к интернету
            
            switch response.result {
            case .success(let value):
                
                var courses = [Course]()
                courses = Course.getArray(from: value)!
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
            
            /* // запрос на урл и ответ, проверка на то, что не было ошибки и вывод результат
             guard let statusCode = response.response?.statusCode else {return}
             
             print("StatusCode: ", statusCode)
             
             if (200..<300).contains(statusCode) {
             let value = response.result.value
             print("value: ", value ?? "nil")
             } else {
             let error = response.error
             print(error ?? "error")
             }
             */
        }
    }
    
    static func downloadImage (url: String, completion: @escaping (_ image: UIImage) -> ()  ) {
        
        
        
        request(url, method: .get).responseData { (responseData) in
            
            switch responseData.result {
            case .success(let data):
                guard let image = UIImage(data: data) else {return}
                DispatchQueue.main.async {
                    completion(image)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    //
    static func responseData (url: String) {
        request(url).responseData { (responseData) in
            
            switch responseData.result {
            case .success(let data):
                guard let string = String(data: data, encoding: .utf8) else { return }
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func responceString(url: String) {
        
        request(url).responseString { (responseString) in
            
            switch responseString.result {
            case .success(let string):
                print(string)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    static func response(url: String) {
        
        request(url).response { (response) in
            
            guard let data = response.data,
                let string = String(data: data, encoding: .utf8)
                else {return}
            print(string)
            
        }
    }
    
    static func downloadImageWithProgress (url: String, completion: @escaping (_ image: UIImage) -> ()) {
        
        guard let url = URL(string: url) else {  return }
        
        request(url).validate().downloadProgress { (progress) in
            
            print("total unit count:\n", progress.totalUnitCount)
            print("completedUnitCount:\n", progress.completedUnitCount)
            print("fraction completed:\n", progress.fractionCompleted)
            print("loclizedDescription:\n", progress.localizedDescription)
            print("----------------------------------")
            
            self.onProgress?(progress.fractionCompleted)
            self.completed?(progress.localizedDescription)
            }.response { (response) in
                
                guard let data = response.data, let image = UIImage(data: data) else { return }
                
                DispatchQueue.main.async {
                    completion(image)
                }
        }
    }
    
    static func postRequest(url: String, completion: @escaping ([Course]) -> () ) {
        
        guard let url = URL(string: url) else { return }
        
        let body: [String : Any] = [ "id" : 101,
                                     "name" : "Dmitriy",
                                     "link" : "www.clubtone.net",
                                     "imageUrl" : "www.imgeUrl" ]
        
        request(url, method: .put, parameters: body).responseJSON { (responseData) in
            
            guard let urlresp = responseData.request else {return}
            
            print(urlresp.url?.absoluteString ?? "")
            
            guard let statusCode = responseData.response?.statusCode else { return }
            
            
            print("status code ", statusCode)
            
            switch responseData.result {
                
            case .success(let data):
                
                print(data)
                guard  let json = data as? [String : Any],
                    let course = Course(json: json)
                    else {return}
                
                var courses = [Course]()
                courses.append(course)
                completion(courses)
                
            case .failure(let error):
                print(error)
            }
        }
    }
    
    
    static func uploadImage(url: String) {
        
        guard let url = URL(string: url) else { return }
        
        let image = UIImage(named: "audi_tt_rs_2017-wallpaper-2880x1800")!
        
        let data = image.pngData()!
        
        let httpHeaders = ["Authorization" : "Client-ID 0c7aea1debb0640"]
        
        
        upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(data, withName: "image")
        }, to: url,
           headers: httpHeaders) { (encodingCompletion) in
            switch encodingCompletion {
            case .success(request: let uploadRequest,
                          streamingFromDisk: let streamingFromDisk,
                          streamFileURL: let streamFileUrl):
                print(uploadRequest)
                print(streamingFromDisk)
                print(streamFileUrl ?? "strimingFileUrl is Nil")
                
                uploadRequest.validate().responseJSON(completionHandler: { (responseJSON) in
                    switch responseJSON.result {
                    case .success(let value):
                        print(value)
                    case .failure(let error):
                        print(error)
                    }
                })
            case .failure(let error):
                print(error)
            }
        }
        
    }
    
    
}

 */

// MARK: - Авторизация с помощью Facebook

// В настройках проекта -> Capabilities -> Keychain Sharing -> выбрать лицензию разработчика


/*
import UIKit
import FBSDKCoreKit

let primaryColor = UIColor(red: 130/255, green: 170/255, blue: 30/255, alpha: 1)
let secondaryColor = UIColor(red: 210/255, green: 110/255, blue: 155/255, alpha: 1)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    var bgSessionCommpletionHandler: ( () -> () )?
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        bgSessionCommpletionHandler = completionHandler
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        FBSDKApplicationDelegate.sharedInstance()?.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        let appID = FBSDKSettings.appID
        
        if url.scheme != nil && url.scheme!.hasPrefix("fb\(appID)") && url.host == "authorize" {
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        
        return false
    }
    
}
*/

/*
import UIKit
import FBSDKLoginKit

class LoginViewController: UIViewController {
    
    lazy var fbLoginButton: UIButton = {
        let loginButton = FBSDKLoginButton()
        loginButton.delegate = self
        return loginButton
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        
        setupViews()
        
        if FBSDKAccessToken.currentAccessTokenIsActive() {
            print("The user is logged in")
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    private func setupViews() {
        view.addSubview(fbLoginButton)
        
        fbLoginButton.translatesAutoresizingMaskIntoConstraints = false
        fbLoginButton.backgroundColor = .red
        NSLayoutConstraint(item: fbLoginButton, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1.0, constant: 30).isActive = true
        NSLayoutConstraint(item: fbLoginButton, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1.0, constant: -30).isActive = true
        NSLayoutConstraint(item: fbLoginButton, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true
        let heightConstraint = NSLayoutConstraint(item: fbLoginButton, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 50)
        view.addConstraint(heightConstraint)
        
    }
    
}


extension LoginViewController: FBSDKLoginButtonDelegate {
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        print("Successfully logget in with facebook...")
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        
        print("Did log out of facebook")
    }
    
    
}
*/


// MARK: - Кастомная кнопка Facebook


/*
lazy var customFBLoginButton: UIButton = {
    let loginButton = UIButton()
    loginButton.backgroundColor = UIColor(hexValue: "#3B5999", alpha: 1)
    loginButton.setTitle("Login with facebook", for: .normal)
    loginButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
    loginButton.setTitleColor(.white, for: .normal)
    loginButton.layer.cornerRadius = 4
    loginButton.clipsToBounds = true
    loginButton.addTarget(self, action: #selector(handleCustomFBLogin), for: .touchUpInside)
    return loginButton
}()

@objc private func handleCustomFBLogin() {
    
    FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self) { (result, error) in
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        guard let result = result else { return }
        
        if result.isCancelled {  return }
        else {
            self.openMainViewController()
        }
    }
}
*/


// MARK: - Firebase

// Вставлять pod 'Firebase/Auth' - там доступна одна библиотека с которой нужно будет работать
















