

/*
 
 Классы - Double Class - которые создаются в тестах для симуляции реальных обтьектов и классов. Бывают следующих видов:
 
 Dummy             - пустышка, маникен
 
 stub                  - заглушка
 
 spy                 - шпион
 
 mock            - макет
 */




//Ожидания - когда закончатся потоки

/*
func testGeocoderFetchesCorrectCoordinate () {
 
    let geocoderAnswer = expectation(description: "Geocoder answer") // создание ожидания
    
    let adressString = "Запорожье"
    let geocoder = CLGeocoder()
    geocoder.geocodeAddressString(adressString) { (placemarks, error) in
 
        guard
            let placemarks = placemarks,
            let placemark = placemarks.first,
            let location = placemark.location
            else {
                
                XCTFail()
                return
        }
        
        let latitude = location.coordinate.latitude
        let longitude = location.coordinate.longitude
        
        XCTAssertEqual(latitude, 47.8166667)
        XCTAssertEqual(longitude, 35.1833333)
        
        geocoderAnswer.fulfill()                    // дается отметка что ожидание можно заканивать
    }
 
    waitForExpectations(timeout: 6, handler: nil)       // ждет 6 секунд окончания ожидания. Если не дождаля то выходит ошибка
}
*/
